<?php
//広告
$url_get_ad = API_PATH.API_KNOW_GET_AD;
$post_data_get_ad['type'] = 0;
$a = "";
foreach ( $post_data_get_ad as $key => $val ){ $a.= "$key=" . urlencode( $val ). "&" ;}
$post_data_get_ad = substr($a,0,-1);
$res_get_ad = request_post($url_get_ad, $post_data_get_ad);
$res_get_ad = str_replace("	"," ",$res_get_ad);
$json_get_ad=json_decode($res_get_ad,TRUE);
$result = $json_get_ad['result'];
$msg = $json_get_ad['msg'];
if($result=="0"){
    $count_ad = 0;
}else{
    $count_ad = count($json_get_ad["data"]);
    if($json_get_ad["data"][0]['ad_img']==""&&$json_get_ad["data"][1]['ad_img']==""){
        $count_ad=0;
    }
}

//急上昇ワード
$url_search_index = API_PATH.API_KNOW_SEARCH_INDEX;
$post_data_search_index["p_num"]=1;
$o = "";
foreach ( $post_data_search_index as $k => $v ){$o.= "$k=" . urlencode( $v ). "&" ;}
$post_data_search_index = substr($o,0,-1);
$res_search_index = request_post($url_search_index, $post_data_search_index);
$search_index_json = json_decode($res_search_index,TRUE);

$result_search_index = $search_index_json['result'];
$msg = $search_index_json['msg'];
if($result_search_index!="0"){
    $count_search_index = count($search_index_json["data"]);
    $key1=$search_index_json["data"]["list"][0]["keyword"];
    $key2=$search_index_json["data"]["list"][1]["keyword"];
    $key3=$search_index_json["data"]["list"][2]["keyword"];
    $key4=$search_index_json["data"]["list"][3]["keyword"];
    $key5=$search_index_json["data"]["list"][4]["keyword"];
    $key6=$search_index_json["data"]["list"][5]["keyword"];
    $key7=$search_index_json["data"]["list"][6]["keyword"];
    $key8=$search_index_json["data"]["list"][7]["keyword"];
    $key9=$search_index_json["data"]["list"][8]["keyword"];
    $key10=$search_index_json["data"]["list"][9]["keyword"];
}
?>
<div id="side">
    <div style="display:block;">
        <ul style="line-height:40px;">
            <li style="float:right;">
                <a href="https://twitter.com/SYSZO" target="_blank" title="Twitter">
                    <span style="width:32px;height:32px;background-image:url(/images/twitter.png);display:inline-block;overflow:hidden;text-indent:-9999px;">Twitter</span>
                </a>
            </li>
            <li style="float:right;">
                <a href="https://www.facebook.com/syszo" target="_blank" title="Facebook">
                    <span style="width:32px;height:32px;background-image:url(/images/facebook.png);display:inline-block;overflow:hidden;text-indent:-9999px;">Facebook</span>
                </a>
            </li>
        </ul>
    </div>
    <!--/MdSocialLink01-->
    <?php if ($count_ad>0) { ?>
        <ul id="bnrArea">
            <?php for ($i = 0; $i < $count_ad; $i++){ ?>
                <li><a href="<?php echo $json_get_ad["data"][$i]['link_url'];?>" target="_blank"><img style="width:300px;height:250px;" src="<?php echo HOME_PAGE.$json_get_ad["data"][$i]['ad_img'];?>" alt=""/></a></li>
            <?php } ?>
            <!--<li>-->
            <!--<script type="text/javascript">-->
            <!--    imobile_pid = "32592";-->
            <!--    imobile_asid = "558441";-->
            <!--    imobile_width = 300;-->
            <!--    imobile_height = 250;-->
            <!--</script>-->
            <!--<script type="text/javascript" src="http://spdeliver.i-mobile.co.jp/script/ads.js?20101001"></script>-->
            <!--</li>-->
            <!--<li>-->
            <!--<script type="text/javascript">-->
            <!--imobile_pid = "32592";-->
            <!--imobile_asid = "565102";-->
            <!--imobile_width = 300;-->
            <!--imobile_height = 250;-->
            <!--</script>-->
            <!--<script type="text/javascript" src="http://spdeliver.i-mobile.co.jp/script/ads.js?20101001"></script>-->
            <!--</li>-->
            <!--<li>-->
            <!--<script type="text/javascript">-->
            <!--    imobile_pid = "32592";-->
            <!--    imobile_asid = "558442";-->
            <!--    imobile_width = 300;-->
            <!--    imobile_height = 72;-->
            <!--    imobile_option = {-->
            <!--         type: "infeed_inline",-->
            <!--    };-->
            <!--    imobile_infeed = {-->
            <!--        num: "1",-->
            <!--    };-->
            <!--</script>-->
            <!--<script type="text/javascript" src="http://spdeliver.i-mobile.co.jp/script/ads.js?20101001"></script>-->
            <!--</li>-->
        </ul>
    <?php } ?>
    <!--/#bnrArea-->
    <section id="app" style="margin-top: 20px;">
        <h3><img src="images/title_app.png" alt="SYSZOはアプリ版もご利用ください"/></h3>
        <div id="appInner">
            <h4><img src="images/app_syszo.png" alt="SYSZOアプリ版では質問機能以外につぶやきやフレンド登録、チャット機能など様々な機能があります。"/></h4>
            <p>SYSZOはApp StoreまたはGooglePlayよりダウンロードしてご利用ください。</p>
            <ul>
                <li><a href="https://itunes.apple.com/jp/app/syszo/id991513528?l=ja&ls=1&mt=8" target="_blank"><img src="images/btn_appstore.png" alt="App Store" /></a></li>
                <li><a href="https://play.google.com/store/apps/details?id=com.app.ug_inc&hl=ja" target="_blank"><img src="images/btn_googleplay.png" alt="Google play"/></a></li>
            </ul>
        </div>
    </section>
    <!--/#app-->

    <section id="keyWord">
        <h3><img src="images/title_word.png" alt="急上昇ワード"/></h3>
        <p class="update">更新<span><?php echo date('Y.m.d H:i',strtotime($search_index_json['data']['time']));?></span></p>
        <ul>
            <li class="no01"><a href="search.php?key=<?php echo mb_convert_encoding($key1, "utf-8", "auto");?>"><span>1</span><?php echo $key1;?></a></li>
            <li class="no02"><a href="search.php?key=<?php echo mb_convert_encoding($key2, "utf-8", "auto");?>"><span>2</span><?php echo $key2;?></a></li>
            <li class="no03"><a href="search.php?key=<?php echo mb_convert_encoding($key3, "utf-8", "auto");?>"><span>3</span><?php echo $key3;?></a></li>
            <li><a href="search.php?key=<?php echo mb_convert_encoding($key4, "utf-8", "auto");?>"><span>4</span><?php echo $key4;?></a></li>
            <li><a href="search.php?key=<?php echo mb_convert_encoding($key5, "utf-8", "auto");?>"><span>5</span><?php echo $key5;?></a></li>
            <li><a href="search.php?key=<?php echo mb_convert_encoding($key6, "utf-8", "auto");?>"><span>6</span><?php echo $key6;?></a></li>
            <li><a href="search.php?key=<?php echo mb_convert_encoding($key7, "utf-8", "auto");?>"><span>7</span><?php echo $key7;?></a></li>
            <li><a href="search.php?key=<?php echo mb_convert_encoding($key8, "utf-8", "auto");?>"><span>8</span><?php echo $key8;?></a></li>
            <li><a href="search.php?key=<?php echo mb_convert_encoding($key9, "utf-8", "auto");?>"><span>9</span><?php echo $key9;?></a></li>
            <li><a href="search.php?key=<?php echo mb_convert_encoding($key10, "utf-8", "auto");?>"><span>10</span><?php echo $key10;?></a></li>
        </ul>
    </section>
    <!--/#keyWord-->
</div>
<!--/#side-->