<?php
require 'include.php';
	$title="SYSZO - 情シス特化型メディア";
	$action=$_GET['action'];
	$send_flg="0";
	if($action=="get_pwd"){
		$chk_mail_msg="";

		$url = API_PATH.API_LOGIN_GET_PWD;

		$user_email=$_POST['user_email'];

		$post_data['user_email'] = $user_email;
		//$pattern = "/^([0-9A-Za-z\\-_\\.]+)@([0-9a-z]+\\.[a-z]{2,3}(\\.[a-z]{2})?)$/i";

		if($user_email!=""){
			//if (preg_match($pattern,$user_email)){
			if (validate_email($user_email,false)){
				$o = "";
				foreach ( $post_data as $k => $v ){ $o.= "$k=" . urlencode( $v ). "&" ;}
				$post_data = substr($o,0,-1);
				$res = request_post($url, $post_data);
				$obj = json_decode($res);

				$result = $obj->{'result'};
				$msg = $obj->{'msg'};
				if($result!="0"){
					$send_flg="1";
				}else{
					$send_flg="0";
					$chk_mail_msg="該当メールアドレスが存在しません。";
				}
			}else{
				$chk_mail_msg="正しいメールアドレスを入力してください。";
			}
		}else{
			$chk_mail_msg="メールアドレスを入力してください。";
		}
	}
?>
<?php include "head.php"; ?>
</head>
<body>
<?php include "header.php"; ?>
<div id="wrapper">
  <section id="mypage">
    <h2>パスワード再発行</h2>
    <div id="myPageInner">
<?php if($send_flg=="1"){ ?>
			<p id="pwText">パスワード再発行メールを送信しました。</p>
<?php }else{ ?>
			<p id="pwText">ご登録いただいているメールアドレスを入力の上、送信ボタンを押してください。<br>
			ご登録のメールアドレス宛に、再発行したパスワードを送信します。</p>
			<form action="?action=get_pwd" method="post">
				<dl>
					<?php if($result=="0"||$chk_mail_msg!=""){echo "<dt  style='color:red;'>$chk_mail_msg</dt>";}?>
					<dt>メールアドレス</dt>
					<dd>
						<input type="text" name="user_email" maxlength="100" size="40" value="<?php echo $user_email;?>" />
					</dd>
				</dl>
				<div id="submit" class="mt15">
					<input name="commit" type="submit" value="送信する" />
				</div>
			</form>

<?php } ?>
    </div>
  </section>
</div>
<!--/#wrapper-->
<?php include "footer.php"; ?>
<script>
$(function() {
    $('#button').click(function(){
    $(this).next('#questionArea').slideToggle();
	$("#button").toggleClass("active");
    });
});
</script>
</body>
</html>