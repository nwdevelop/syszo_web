(function($) {
  $.fn.layerBoard = function(option) {
		var elements = this;
		elements.each(function(){
			option = $.extend({
				delayTime: 200,	
				fadeTime : 500,	
				alpha : 0.5,
				limitMin : 1,
				easing: 'swing',
				limitCookie : 0
			}, option);
			var limitSec = option.limitMin * 60; //秒数に変換
			if ($.cookie('layerBoardTime') == null) {
				LayerBoardFunc ();
				var start = new Date();
				$.cookie('layerBoardTime', start.getTime(), { expires: option.limitCookie,path: '/' });
			} else {
				var now = new Date();
				secDiff = now.getTime() - $.cookie('layerBoardTime');
				secTime = Math.floor( secDiff / 1000);
				if (secTime >= limitSec) {
					LayerBoardFunc ();
					$.cookie('layerBoardTime', null, { expires:-1,path: '/' });
					var start = new Date();
					$.cookie('layerBoardTime', start.getTime(), { expires:option.limitCookie,path: '/' });
				}
			}
			function LayerBoardFunc () {
				$('.layer_board_bg', elements).show().animate({opacity: 0},0).delay(option.delayTime).animate({opacity: option.alpha},option.fadeTime,function(){
					$('.layer_board', elements).fadeIn(option.fadeTime);																																					
				})
			}
//			$('.layer_board_bg', elements).click(function() {
//				$('.layer_board', elements).fadeOut(option.fadeTime);
//				$(this).fadeOut(option.fadeTime);
//			});
			$('.btn_close').click(function() {
				$('.layer_board', elements).fadeOut(option.fadeTime);
				$('.layer_board_bg', elements).fadeOut(option.fadeTime);
				//window.location.href="/"; 
			});
			$('.layer_board_btn').click(function() {
				$('.layer_board_bg', elements).show().animate({opacity: 0},0).delay(option.delayTime).animate({opacity: option.alpha},option.fadeTime,function(){
					$('.layer_board', elements).fadeIn(option.fadeTime);																																					
				});
			});
		});
		return this;		
	};
})( jQuery );


