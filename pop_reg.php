<?php
require 'include.php';
if(isset($_GET['action'])){
	if($_GET['action']=="reg"){
		$url = API_PATH.API_LOGIN_REG;
		$user_nick=stripslashes(trim($_POST['user_nick']));
		$user_email=stripslashes(trim($_POST['user_email']));
		$user_pwd=stripslashes(trim($_POST['user_pwd']));

		if(empty($user_nick)){
			echo "ニックネームを入力してください。";die;
		}
		if(empty($user_email)){
			echo "メールアドレスを入力してください。";die;
		}
		if(empty($user_pwd)){
			echo "パスワードを入力してください。";die;
		}

		$post_data['user_nick'] = $user_nick;
		$post_data['user_email'] = $user_email;
		$post_data['user_pwd'] = $user_pwd;

		$o = "";
		foreach ( $post_data as $k => $v ){ $o.= "$k=" . urlencode( $v ). "&" ;}
		$post_data = substr($o,0,-1);
		$res = request_post($url, $post_data);
		$obj = json_decode($res);

		$result = $obj->{'result'};
		$msg = $obj->{'msg'};
		if($result!="0"){
			$_SESSION['user_id']='';
			$_SESSION['user_nick']='';
			setcookie('user_id','',time()-3600);
			setcookie('user_nick','',time()-3600);
			$_SESSION['user_id']=$obj->{'data'}->{'user_id'};
			$_SESSION['user_nick']=$user_nick;
			session_regenerate_id();
			echo 'success';
			die;
		}else{
			echo $msg;
			die;
		}
	}
}
?>