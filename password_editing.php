<?php
require 'include.php';
$title="SYSZO - 情シス特化型メディア";

$login_user_id=$_SESSION['user_id'];
if($login_user_id==""){$login_user_id=$_COOKIE['user_id'];}
$login_user_name=$_SESSION['user_nick'];
if($login_user_name==""){$login_user_name=$_COOKIE['user_nick'];}

if($login_user_id==""){header("Location:https://syszo.com/login.php");}

$action=$_GET['action'];

if ($action=="save"){
	$url_pwd_edit = API_PATH.API_MYSELF_PWD_EDIT;

	$old_pwd=$_POST['old_pwd'];
	$new_pwd=$_POST['new_pwd'];
	$new_pwd2=$_POST['new_pwd2'];

	if($new_pwd==$new_pwd2){
		if(strlen($new_pwd)<6||strlen($new_pwd)>16){
			$result = "2";
			$msg2="6〜16文字以内で入力してください。";
		}else{
			$post_data_save['user_id'] = $login_user_id;
			$post_data_save['old_pwd']=$old_pwd;
			$post_data_save['user_pwd']=$new_pwd;

			$o = "";
			foreach ( $post_data_save as $k => $v ){ $o.= "$k=" . urlencode( $v ). "&" ;}
			$post_data_save = substr($o,0,-1);
			$res_save = request_post($url_pwd_edit, $post_data_save);
			$obj_save = json_decode($res_save);

			$result = $obj_save->{'result'};
			$msg = $obj_save->{'msg'};
			if($result!="0"){
				header("Location:mypage.php");
			}
		}
	}
	else{
		$result = "2";
		$msg2="入力したパスワードは一致しません。";
	}
}
?>
<?php include "head.php"; ?>
<link rel="stylesheet" href="css/chosen.css">
</head>
<body>
<?php include "header.php"; ?>
<div id="wrapper">
  <section id="mypage">
    <h2>パスワード変更<span id="editing"><a href="mypage.php">戻る</a></span></h2>
    <div id="yourPage">
      <form action="?action=save" method="post">
				<?php if($result=="0"){echo "<p  style='color:red;'>$msg</p>";}?>
       <dl id="newPw">
        <dt>現在のパスワード</dt>
        <dd><input name="old_pwd" type="password" value="" size="40" /></dd>
      </dl>
      <dl>
				<?php if($result=="2"){echo "<p  style='color:red;'>$msg2</p>";}?>
        <dt>新しいパスワード</dt>
        <dd><input name="new_pwd" type="password" value="" size="40" /></br><span class="note">（6〜16文字以内）</span></dd>
      </dl>
      <dl>
        <dt>新しいパスワード（確認用）</dt>
        <dd><input name="new_pwd2" type="password" value="" size="40" /></br><span class="note">（6〜16文字以内）</span></dd>
      </dl>
        <div id="submit">
          <input name="commit" type="submit" value="変更" />
        </div>
      </form>
    </div>
  </section>
</div>
<!--/#wrapper-->
<?php include "footer.php"; ?>
<script>
$(function() {
    $('#button').click(function(){
    $(this).next('#questionArea').slideToggle();
	$("#button").toggleClass("active");
    });
});
</script>
</body>
</html>