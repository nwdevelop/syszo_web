<?php
    //include
	require '../util/include.php';
	$action = $_GET['action'];

	$MM=sprintf("%02d",$_GET['h']);
	$DD=sprintf("%02d",$_GET['i']);
	$insert_time_from=$_GET['g'].'-'.$MM.'-'.$DD.' 00:00:00';
	$insert_time_from=strtotime($insert_time_from);

	$MM_TO=sprintf("%02d",$_GET['l']);
	$DD_TO=sprintf("%02d",$_GET['m']);
	$insert_time_to=$_GET['k'].'-'.$MM_TO.'-'.$DD_TO.' 23:59:59';
	$insert_time_to=strtotime($insert_time_to);

	$s_user_nick=$_GET['a'];
	$s_user_email=$_GET['b'];
	$s_title=$_GET['c'];
	$s_content=$_GET['d'];
	$s_comments=$_GET['e'];
	$s_sts=$_GET['f'];
	$s_sort_item=$_GET['j'];
	$s_sort_type=$_GET['n'];

    if ($action=='export'){
		$link = db_conn();
        mysql_set_charset('utf8');

		//All
		$sqlall = "select DISTINCT ak.*,am.user_nick,am.user_email
				from app_know ak
				LEFT JOIN app_know_comments akc ON ak.id=akc.info_id
				LEFT JOIN app_member am ON  am.user_id=ak.user_id
				WHERE 1";
		//状況
		if($s_sts=='2') {
			$sqlall .= " and ak.urgent=2";
		}
		elseif($s_sts=='3') {
			$sqlall .= " and ak.urgent=1";
		}
		//ニックネーム
		if($s_user_nick!='') {
			$sqlall .= " and am.user_nick like '%$s_user_nick%'";
		}
		//メールアドレス
		if($s_user_email!='') {
			$sqlall .= " and am.user_email like '%$s_user_email%'";
		}
		//タイトル
		if($s_title!='') {
			$sqlall .= " and ak.title like '%$s_title%'";
		}
		//内容
		if($s_content!='') {
			$sqlall .= " and ak.content like '%$s_content%'";
		}
		//コメント
		if($s_comments!='') {
			$sqlall .= " and akc.content like '%$s_comments%'";
		}
		if($_GET['g']!='') {
			$sqlall .= " and ak.insert_time >= $insert_time_from ";
		}
		if($_GET['k']!='') {
			$sqlall .= " and ak.insert_time <= $insert_time_to ";
		}

		//ソート順タイプ
		if($s_sort_type=='1'){
			$sort_str='desc';
		}
		elseif($s_sort_type=='2'){
			$sort_str='asc';
		}else{
			$sort_str='desc';
		}
		//ソート順
		if($s_sort_item=='1'){
			$sort_key='id';
		}
		elseif($s_sort_item=='2'){
			$sort_key='comments_sum';
		}else{
			$sort_key='id';
		}

		$sql = sprintf("%s GROUP BY ak.id order by ak.%s %s ",$sqlall,$sort_key,$sort_str);

		$result = mysql_query($sqlall,$link) or die(mysql_error());

		$csv_h_1 = "ID";
		$csv_h_2 = "状況";
		$csv_h_3 = "投稿者";
		$csv_h_4 = "投稿時間";
		$csv_h_5 = "コメント数";
		$csv_h_6 = "投稿タイトル";
		$csv_h_7 = "投稿内容";
		$csv_h_8 = "イメージ";
		$csv_h_9 = "メールアドレス";

		$strhead="";

		$strhead= $strhead."\"".$csv_h_1."\",\"".$csv_h_2."\",\"".$csv_h_3."\",\"".$csv_h_4."\",\"".$csv_h_5."\",\"".$csv_h_6."\",\"".$csv_h_7."\",\"".$csv_h_8."\",\"".$csv_h_9."\"\n";

        $i=0;
        while($rs=mysql_fetch_object($result)){

			$csv_c_1=$rs->id;
			if ($rs->urgent=='2'){
				$csv_c_2='緊急';
			}else{
				$csv_c_2='通常';
			}
			$csv_c_3=$rs->user_nick;
			$csv_c_4=date("Y-m-d H:i:s",$rs->insert_time);
			$csv_c_5=$rs->comments_sum;
			$csv_c_6=$rs->title;
			$csv_c_7=str_replace("\r","<br-r>",$rs->content);
			$csv_c_7=str_replace("\n","<br-n>",$csv_c_7);
			$csv_c_8=$rs->url;
			$csv_c_9=$rs->user_email;

			$str = $str."\"".$csv_c_1."\",\"".$csv_c_2."\",\"".$csv_c_3."\",\"".$csv_c_4."\",\"".$csv_c_5."\",\"".$csv_c_6."\",\"".$csv_c_7."\",\"".$csv_c_7."\",\"".$csv_c_8."\"\n";

        }
		db_disConn($result, $link);

        //CSVファイル名
        $filename = 'export_bbs_info_'.date('YmdHis').'.csv';

        //$str = iconv('utf-8','SJIS',$str);
		$str = $strhead.$str;
		$str = "\xEF\xBB\xBF".$str ; //BOM
        //出力処理
        export_csv($filename,$str);
    }
?>