<?php

	//include
	require '../util/include.php';

	$home_page_name='シス蔵管理メニュー';
	$home_page_url=URL_PATH;
	$f_page_name='シス蔵管理メニュー';
	$f_page_url=URL_PATH;
	$page_name='お知らせ管理画面';

?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title><?php echo $page_name; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<link href="../css/common.css" type="text/css" rel="stylesheet">
</head>
<body>
<body>
<div id="header">
	<div id="header_content">
		<h1><a href="<?php echo $home_page_url; ?>">シス蔵管理画面</a></h1>
	</div>
</div>
<div id="nav">
	<div id="nav_content">
		<a href="<?php echo $home_page_url; ?>"><?php echo $home_page_name.' ＞ '; ?></a>
		<?php echo $page_name; ?>
	</div>
</div>
<div class="content">
	<div style='text-align:center;'>
		<div style='text-align:left;margin-top:120px;margin-bottom:20px'>
			<br/>
			<a href="m_notice_push.php">プッシュ通知管理画面へ</a><br/><br/>
			<a href="m_notice_mail.php">メール配信画面へ</a><br/><br/>
		</div>
	</div>
</div>

</body>
</html>
