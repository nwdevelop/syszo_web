<?php
header('Cache-control: private, must-revalidate');
	//ユーザー管理⇒会員登録情報
	//include
	require '../util/include.php';
//	header('Cache-control:no-cache');
	$home_page_name='シス蔵管理メニュー';
	$home_page_url=URL_PATH;
	$f_page_name='お知らせ管理画面';
	$f_page_url=URL_PATH.'m_notice.php';
	$l_page_name='メール配信対象選択画面';
	$l_page_url=URL_PATH.'m_notice_mail.php';
	$page_name='メール配信画面';

	$insert_time=strtotime(date('Y-m-d H:i:s',time()));

	$action = $_GET['action'];
	//$u_ids = $_GET['u_id'];
	$u_ids = $_POST['u_id'];
	//$all_flg = $_GET['all_flg'];
	$all_flg = $_POST['all_flg'];
	$mail_success_flg="0";

	if($u_ids==''&&$all_flg!='1'){
		//header("Location:m_notice_mail.php");
	}
	$u_ids=str_replace(",","','",$u_ids);
	$u_ids="'".$u_ids."'";

	//Update
	if ($action=='send'){

		$mail_title=$_POST['mail_title'];
		$mail_content_all=$_POST['mail_content'];
		$u_ids=$_POST['u_ids'];
		$u_ids=str_replace(",","','",$u_ids);
		$u_ids="'".$u_ids."'";
		if($u_ids==''&&$all_flg!='1'){
			header("Location:m_notice_mail.php");
		}
		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

		$sql = sprintf("select * from app_member WHERE user_id in (%s)",$u_ids);

		$result = mysql_query($sql,$db);
		while($rs=mysql_fetch_object($result))
		{
			$mail_content=str_replace('{@USER_NAME}',$rs->user_nick,$mail_content_all);
			$ret=send_noti_mail($rs->user_email,$mail_title,$mail_content);
			$sql_insert = sprintf("insert into app_mail_history  (user_id,user_email,user_nick,mail_title,mail_contents,insert_time) VALUES ('%s','%s','%s','%s','%s',%d)",$rs->user_id,$rs->user_email,$rs->user_nick,$mail_title,$mail_content,$insert_time);
			$result_insert = mysql_query($sql_insert,$db);
			$mail_success_flg="1";
		}
		mysql_close($db);
	}

	//Search
	if (($action=='search')||($action=='send')){

		$link = db_conn();
		mysql_set_charset('utf8');

		$page_size=10000;

		if( isset($_GET['page']) ){
		   $page = intval( $_GET['page'] );
		}
		else{
		   $page = 1;
		}
		$rowCnt = 0;

		//All
		$sqlall = "select am.*,aa.area_name,aj.name job_name,ai.industry_name,ascc.size_company_name,au.time_name,
					aq.qualified_name,asw.software_name,ah.hardware_name,aay.allyear_name,arm.reg_money_name,
					(select count(*) from app_fans af where am.user_id=af.uid_a) follow_cnt,
					(select count(*) from app_mail_history amh where am.user_id=amh.user_id) mail_cnt,
					(select count(*) from app_fans_b afb where am.user_id=afb.uid_b) follower_cnt 
					 from app_member am 
					 left join app_area aa on am.address=aa.area_id 
					 left join app_industry ai on am.industry=ai.industry_id 
					 left join app_jobs aj on am.job=aj.job_id 
					 left join app_size_company ascc on am.size_company=ascc.size_company_id 
					 left join app_use au on am.calendar=au.use_id 
					 left join app_software asw on am.software=asw.software_id 
					 left join app_hardware ah on am.hardware=ah.hardware_id 
					 left join app_qualified aq on am.qualified=aq.qualified_id 
					 left join app_allyear aay on am.allyear=aay.allyear_id 
					 left join app_reg_money arm on am.reg_money=arm.reg_money_id 
					 WHERE 1 ";

		//ニックネーム
		if($u_ids!=''&&$all_flg!='1') {
			$sqlall .= " and am.user_id in ($u_ids) ";
		}elseif($all_flg=='1') {
			$sqlall .= " and am.mail_flg <> 3 ";
		}

		$result = mysql_query($sqlall,$link) or die(mysql_error());

		if(!$result){
			$rowCnt = -1;
			db_disConn($result, $link);
		}
		$rowCntall=mysql_num_rows($result);

		$sql = sprintf("%s order by am.user_id desc limit %d,%d",$sqlall,($page-1)*$page_size,$page_size);

		$result = mysql_query($sql,$link);

		if(!$result){
			$rowCnt = -1;
			db_disConn($result, $link);
		}

		$rowCnt=mysql_num_rows($result);

		//paging
		if($rowCnt==0){
			$page_count = 0;
			db_disConn($result, $link);
		}
		else{
			if( $rowCntall<$page_size ){ $page_count = 1; }
			if( $rowCntall%$page_size ){
				$page_count = (int)($rowCntall / $page_size) + 1;
			}else{
				$page_count = $rowCntall / $page_size;
			}
		}
		$page_string = '';
		if (($page == 1)||($page_count == 1)){
		   $page_string .= 'トップページ|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		else{
			$page_string .= '<a href=?action=search&page=1'.
							'>トップページ</a>|<a href=?action=search&page='.($page-1).
							'>前頁</a>|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		if( ($page == $page_count) || ($page_count == 0) ){
		   $page_string .= '次頁|最終ページ';
		}
		else{
			$page_string .= '<a href=?action=search&page='.($page+1).
							'>次頁</a>|<a href=?action=search&page='.$page_count.
							'>最終ページ</a>';
		}
	}
?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title><?php echo $page_name; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<link href="../css/common.css" type="text/css" rel="stylesheet">
<script src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<?php
	if($mail_success_flg=="1"){
		echo"
		<script language='javascript' type='text/javascript'>
			alert('メールを配信しました!');
			window.location.href='m_notice_mail.php';
		</script>
		";
	}
?>
</head>
<body>
<div id="header">
	<div id="header_content">
		<h1><a href="<?php echo $home_page_url; ?>">シス蔵管理画面</a></h1>
	</div>
</div>
<div id="nav">
	<div id="nav_content">
		<a href="<?php echo $home_page_url; ?>"><?php echo $home_page_name.' ＞ '; ?></a>
		<a href="<?php echo $f_page_url; ?>"><?php echo $f_page_name.' ＞ '; ?></a>
		<a href="<?php echo $l_page_url; ?>"><?php echo $l_page_name.' ＞ '; ?></a>
		<?php echo $page_name; ?>
	</div>
</div>
<div class='content'>
	<div style='float:left;margin-top:120px;margin-bottom:20px'>
		<form action='?action=send' method='post' name='form1'onkeydown='if(event.keyCode==13){return false;}'>
			<div style='float:left; text-align:left;width:180px;height:20px;' >
				タイトル:
			</div>
			<div style='float:left; text-align:left; width:296px;height:20px;' >
				<input type='text' name='mail_title' id='mail_title' style='width:296px;height:20px;' value='<?php echo $mail_title;?>' tabindex='1'/>
			</div>
			<div style='clear:both;'></div><br/>
			<div style='float:left; text-align:left;width:180px;height:20px;' >
				メール本文:
			</div>
			<div style='float:left; text-align:left;width:180px;'>
				<textarea name='mail_content' id='mail_content' style='width:98%;min-width:546px;height:200px;font-family: inherit;'tabindex='2'><?php echo $mail_content;?></textarea>
			</div>
			<div style='clear:both;'></div><br/>
			<?php
				if ($all_flg=='1'){
					echo "
					<div style='float:left; text-align:left;width:180px;height:20px;' >
						送信対象:
					</div>
					<div style='float:left; text-align:left;width:500px;'>
						全員（受け取らないユーザーを除く）
					</div>
					<div style='clear:both;'></div><br/>
					";
				}
			?>
			<div style='float:left; text-align:left;margin-left:5px;' >
				<input type="button" class="btn_search" value="戻る" onclick="back_url('m_notice_mail.php')" tabindex='4'/>
			</div>
			<div style='float:left; text-align:left;;margin-left:20px;' >
				<input type="button" class="btn_search" value="送信" tabindex='3'onclick="sendSubmit()"/>
			</div>
			<div style='clear:both;'></div><br/>
			<?php
				if ($all_flg!='1'){
					echo "
					<div style='float:left; text-align:left;width:500px;height:20px;' >
						送信対象一覧（受け取らないユーザーも送信される）:
					</div>
					";
				}
			?>
			<div style='clear:both;'></div><br/>
		<?php
			if ($rowCnt>0&&$all_flg!='1'){
				echo "
					<table width='100%' cellspacing='1' cellpadding='2'>
						<tr bgcolor='#DBE6F5'>
						  <td><span style='float:left; text-align:center'><font color=#666666>$page_string</font></span></td>
						</tr>
					</table>";
				echo "
					<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
						<tr bgcolor='#DBE6F5'>
							<th width='255px'>ニックネーム</th>
							<th width='255px'>メールアドレス</th>
							<th width='100px'>フォロー数</th>
							<th width='100px'>フォロワー数</th>
							<th width='80px'>性別</th>
							<th width='120px'>現在地</th>
							<th width='200px'>業種</th>
							<th width='200px'>会社規模</th>
							<th width='200px'>役職・役割</th>
							<th width='200px'>予算規模</th>
							<th width='200px'>情シス歴</th>
							<th width='200px'>ソフトウェア</th>
							<th width='200px'>ハードウェア</th>
							<th width='200px'>保有資格</th>
							<th width='200px'>年間予算</th>
							<th width='200px'>申込時間</th>
							<th width='200px'>最終ログイン</th>
							<th width='200px'>最終書込</th>
							<th width='200px'>ユーザーID</th>
						</tr>
					</table>
				";
				$i=1;
				 while($rs=mysql_fetch_object($result))
				{
				  echo "
					   <table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
							<tr align='left' bgcolor='#EEF2F4'>
						";
				    echo "
							<td width='255px'align='center'>".$rs->user_nick."</td>
							<td width='255px'align='center'>".$rs->user_email."</td>
				";
				  if ($rs->follow_cnt==0){
					  echo "
							<td width='100px'align='center'>$rs->follow_cnt</td>
					  ";
				  }else{
					  echo "
							<td width='100px'align='center'><a href='javascript:void(0)'onclick='searchFollow($rs->user_id)' >".$rs->follow_cnt."</a></td>
					  ";
				  }
				  if ($rs->follower_cnt==0){
					  echo "
							<td width='100px'align='center'>$rs->follower_cnt</td>
					  ";
				  }else{
					  echo "
							<td width='100px'align='center'><a href='javascript:void(0)'onclick='searchFollower($rs->user_id)' >".$rs->follower_cnt."</a></td>
					  ";
				  }
				  //性別
				  if ($rs->user_sex==1){
					  echo "
							<td width='80px' align='center'>1:男性</td>
						";
				  }
				  elseif ($rs->user_sex==2){
					  echo "
							<td width='80px' align='center'>2:女性</td>
						";
				  }
				   else{
					  echo "
							<td width='80px'align='center'></td>
						";
				  }
				  echo "
							<td width='120px'align='center'>".$rs->area_name."</td>
							<td width='200px'align='center'>".$rs->industry_name."</td>
							<td width='200px'align='center'>".$rs->size_company_name."</td>
							<td width='200px'align='center'>".$rs->job_name."</td>
							<td width='200px'align='center'>".$rs->allyear_name."</td>
							<td width='200px'align='center'>".$rs->time_name."</td>
							<td width='200px'align='center'>".$rs->software_name."</td>
							<td width='200px'align='center'>".$rs->hardware_name."</td>
							<td width='200px'align='center'>".$rs->qualified_name."</td>
							<td width='200px'align='center'>".$rs->reg_money_name."</td>
				";
				  echo "
							<td width='200px'align='center'>".date("Y-m-d H:i:s",$rs->insert_time)."</td>
							<td width='200px'align='center'>".date("Y-m-d H:i:s",$rs->last_login)."</td>
							<td width='200px'align='center'>".date("Y-m-d H:i:s",$rs->last_input)."</td>
							<td width='200px'align='center'>".$rs->user_id."</td>
				";
				 
				  echo"
							<input type='hidden' name='u_ids' id='u_ids' value=".str_replace("'","",$u_ids)." />
							</tr>
					  </table>
					";
					$i++;
				}
				echo "
					<table width='100%' cellspacing='1' cellpadding='2'>
						<tr bgcolor='#DBE6F5'>
						  <td><span style='float:left; text-align:center'><font color=#666666>$page_string</font></span></td>
						</tr>
					</table>";
				mysql_close($link);
			}
		?>
		<div style='clear:both;'></div><br/>
			<?php
				if ($all_flg!='1'){
					echo "
						<div style='float:left; text-align:left;margin-left:5px;' >
							<input type='button' class='btn_search' value='戻る' onclick=\"back_url('m_notice_mail.php')\" />
						</div>
						<div style='float:left; text-align:left;;margin-left:20px;' >
							<input type='button' class='btn_search' value='送信' onclick=\"sendSubmit()\"/>
						</div>
					";
				}
			?>

		</form>
	<script language="javascript" type="text/javascript">
		function show(msg,id) {
			document.getElementById(id).value=msg;
		}
		function initSearch(){
			document.form1.action='?action=search';
			document.form1.submit();
		}
		function sendSubmit(){
			var i_title=$.trim(document.form1.mail_title.value);
			var i_content=$.trim(document.form1.mail_content.value);
			if(i_title==""){
				alert("タイトルを入力してください。");
				document.form1.mail_title.focus();
				return false;
			}
			if(i_content==""){
				alert("メール本文を入力してください。");
				document.form1.mail_content.focus();
				return false;
			}
			var ret=confirm('本当に送信してもよろしいですか？');if(ret){
				document.form1.action='?action=send';
				document.form1.submit();
			}else{
				return false;
			}

		}
	</script>
	</div>
</div>
</body>
</html>