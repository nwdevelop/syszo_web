<?php
    //include
	require '../util/include.php';
	$action = $_GET['action'];

	$MM=sprintf("%02d",$_GET['h']);
	$DD=sprintf("%02d",$_GET['i']);
	$insert_time_from=$_GET['g'].'-'.$MM.'-'.$DD.' 00:00:00';
	$insert_time_from=strtotime($insert_time_from);

	$MM_TO=sprintf("%02d",$_GET['l']);
	$DD_TO=sprintf("%02d",$_GET['m']);
	$insert_time_to=$_GET['k'].'-'.$MM_TO.'-'.$DD_TO.' 23:59:59';
	$insert_time_to=strtotime($insert_time_to);

	$s_user_nick=$_GET['a'];
	$s_user_email=$_GET['b'];
	$s_address_id=$_GET['c'];
	$s_user_sex=$_GET['d'];
	$s_user_id=$_GET['e'];
	$s_sort_item=$_GET['j'];
	$s_sort_type=$_GET['n'];

    if ($action=='export'){
		$link = db_conn();
        mysql_set_charset('utf8');

		//All
		$sqlall = "select am.*,aa.area_name,aj.name job_name,ai.industry_name,ascc.size_company_name,au.time_name,
					aq.qualified_name,asw.software_name,ah.hardware_name,aay.allyear_name,arm.reg_money_name,
					(select count(*) from app_fans af where am.user_id=af.uid_a) follow_cnt,
					(select count(*) from app_fans_b afb where am.user_id=afb.uid_b) follower_cnt
					 from app_member am
					 left join app_area aa on am.address=aa.area_id
					 left join app_industry ai on am.industry=ai.industry_id
					 left join app_jobs aj on am.job=aj.job_id
					 left join app_size_company ascc on am.size_company=ascc.size_company_id
					 left join app_use au on am.calendar=au.use_id
					 left join app_software asw on am.software=asw.software_id
					 left join app_hardware ah on am.hardware=ah.hardware_id
					 left join app_qualified aq on am.qualified=aq.qualified_id
					 left join app_allyear aay on am.allyear=aay.allyear_id
					 left join app_reg_money arm on am.reg_money=arm.reg_money_id
					 WHERE 1";
		//ニックネーム
		if($s_user_nick!='') {
			$sqlall .= " and am.user_nick like '%$s_user_nick%'";
		}
		//メールアドレス
		if($s_user_email!='') {
			$sqlall .= " and am.user_email like '%$s_user_email%'";
		}
		//性別
		if($s_user_sex!='') {
			$sqlall .= " and am.user_sex = $s_user_sex";
		}
		//居住都道府県
		if($s_address_id!='') {
			$sqlall .= " and am.address = $s_address_id";
		}
		//ユーザーID
		if($user_id!='') {
			$sqlall .= " and am.user_id = $user_id";
		}
		if($_GET['g']!='') {
			$sqlall .= " and am.insert_time >= $insert_time_from ";
		}
		if($_GET['k']!='') {
			$sqlall .= " and am.insert_time <= $insert_time_to ";
		}

		//ソート順タイプ
		if($s_sort_type=='1'){
			$sort_str='desc';
		}
		elseif($s_sort_type=='2'){
			$sort_str='asc';
		}else{
			$sort_str='desc';
		}
		//ソート順
		if($s_sort_item=='1'){
			$sort_key='user_id';
		}
		elseif($s_sort_item=='2'){
			$sort_key='follow_cnt';
		}
		elseif($s_sort_item=='3'){
			$sort_key='follow_cnt';
		}else{
			$sort_key='user_id';
		}

		$sql = sprintf("%s order by am.%s %s ",$sqlall,$sort_key,$sort_str);

		$result = mysql_query($sqlall,$link) or die(mysql_error());

		$csv_h_1 = "ユーザーID";
		$csv_h_2 = "状態";
		$csv_h_3 = "デバイス";
		$csv_h_4 = "ニックネーム";
		$csv_h_5 = "メールアドレス";
		$csv_h_6 = "フォロー数";
		$csv_h_7 = "フォロワー数";
		$csv_h_8 = "性別";
		$csv_h_9 = "現在地";
		$csv_h_10 = "業種";
		$csv_h_11 = "会社規模";
		$csv_h_12 = "役職・役割";
		$csv_h_13 = "予算規模";
		$csv_h_14 = "情シス歴";
		$csv_h_15 = "ソフトウェア";
		$csv_h_16 = "ハードウェア";
		$csv_h_17 = "保有資格";
		$csv_h_18 = "年間予算";
		$csv_h_19 = "申込時間";
		$csv_h_20 = "最終ログイン";
		$csv_h_21 = "最終書込";

		$strhead="";

		$strhead= $strhead."\"".$csv_h_1."\",\"".$csv_h_2."\",\"".$csv_h_3."\",\"".$csv_h_4."\",\"".$csv_h_5."\",\"".$csv_h_6."\",\"".$csv_h_7."\",\"".$csv_h_8."\",\"".$csv_h_9."\",\"".$csv_h_10."\",\"".$csv_h_11."\",\"".$csv_h_12."\",\"".$csv_h_13."\",\"".$csv_h_14."\",\"".$csv_h_15."\",\"".$csv_h_16."\",\"".$csv_h_17."\",\"".$csv_h_18."\",\"".$csv_h_19."\",\"".$csv_h_20."\",\"".$csv_h_21."\"\n";

        $i=0;
        while($rs=mysql_fetch_object($result)){

			$csv_c_1=$rs->user_id;
			if ($rs->status=='0'){
				$csv_c_2="通常";
			}else{
				$csv_c_2="退会";
			}
			if ($rs->device=='1'){
				$csv_c_3="Android";
			}elseif ($rs->device=='2'){
				$csv_c_3="iOS";
			}elseif ($rs->device=='0'){
				$csv_c_3="Web";
			}else{
				$csv_c_3="Unknown";
			}
			$csv_c_4=$rs->user_nick;
			$csv_c_5=$rs->user_email;
			$csv_c_6=$rs->follow_cnt;
			$csv_c_7=$rs->follower_cnt;
			//性別
			if ($rs->user_sex==1){
				$csv_c_8="1:男性";
			}elseif ($rs->user_sex==2){
				$csv_c_8="2:女性";
			}else{
				$csv_c_8="";
			}
			$csv_c_9=$rs->area_name;
			$csv_c_10=$rs->industry_name;
			$csv_c_11=$rs->size_company_name;
			$csv_c_12=$rs->job_name;
			$csv_c_13=$rs->allyear_name;
			$csv_c_14=$rs->time_name;
			$csv_c_15=$rs->software_name;
			$csv_c_16=$rs->hardware_name;
			$csv_c_17=$rs->qualified_name;
			$csv_c_18=$rs->reg_money_name;
			$csv_c_19=date("Y-m-d H:i:s",$rs->insert_time);
			$csv_c_20=date("Y-m-d H:i:s",$rs->last_login);
			$csv_c_21=date("Y-m-d H:i:s",$rs->last_input);

			$str= $str."\"".$csv_c_1."\",\"".$csv_c_2."\",\"".$csv_c_3."\",\"".$csv_c_4."\",\"".$csv_c_5."\",\"".$csv_c_6."\",\"".$csv_c_7."\",\"".$csv_c_8."\",\"".$csv_c_9."\",\"".$csv_c_10."\",\"".$csv_c_11."\",\"".$csv_c_12."\",\"".$csv_c_13."\",\"".$csv_c_14."\",\"".$csv_c_15."\",\"".$csv_c_16."\",\"".$csv_c_17."\",\"".$csv_c_18."\",\"".$csv_c_19."\",\"".$csv_c_20."\",\"".$csv_c_21."\"\n";

        }
		db_disConn($result, $link);

        //CSVファイル名
        $filename = 'export_user_info_'.date('YmdHis').'.csv';

        //$str = iconv('utf-8','SJIS',$str);
		$str = $strhead.$str;
		$str = "\xEF\xBB\xBF".$str ; //BOM
        //出力処理
        export_csv($filename,$str);
    }
?>