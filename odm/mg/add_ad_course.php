<?php
	//include
	require '../util/include.php';

	$comp_id=$_POST['add_comp_id'];
	$ad_id=$_POST['add_ad_id'];
	$course_name=$_POST['add_course_name'];
	$from_date=$_POST['add_from_date'];
	$to_date=$_POST['add_to_date'];
	$price=$_POST['add_price'];
	$ret=0;

	$existChk = searchAdCourseInfo($comp_id,$ad_id,$course_name,$from_date,$to_date);

	if ($existChk=='1'){
		echo '該当広告コース情報が既に存在しました。';
	}else{
		$ret = addAdCourseInfo($comp_id,$ad_id,$course_name,$from_date,$to_date,$price);
		if($ret!='0'){
			echo 'システムエラー、登録を失敗しました。';
		}else{
			echo '広告コース情報を登録しました。';
		}
	}
?>