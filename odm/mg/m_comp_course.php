<?php
//ユーザー管理⇒会員登録情報
//include
require '../util/include.php';
header('Cache-control: private, must-revalidate');
$home_page_name='シス蔵管理メニュー';
$home_page_url=URL_PATH;
$f_page_name='シス蔵管理メニュー';
$f_page_url=URL_PATH.'m_mem.php';
$page_name='公式アカウント管理画面';
$systime=date('Y-m-d H:i:s',time());
$action = $_GET['action'];
$get_comp_id = $_GET['get_comp_id'];

//Update
if ($action=='update'){
	$comp_id = $_GET['id'];
	$up_course_name = $_GET['up_course_name'];
	$up_from = $_GET['up_from'];
	$up_to = $_GET['up_to'];
	$up_price = $_GET['up_price'];

	$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
	if(!$db){
		die("connot connect:" . mysql_error());
	}
	$dns = mysql_select_db(DB_NAME,$db);
	if(!$dns){
		die("connot use db:" . mysql_error());
	}
	mysql_set_charset('utf8');
	$sql = sprintf("UPDATE app_mg_comp_course SET course_name ='%s',from_date ='%d',to_date ='%d',price ='%s',update_time ='%d' WHERE comp_id = %d",$up_course_name,strtotime($up_from),strtotime($up_to),$up_price,strtotime($systime),$comp_id);
	$result = mysql_query($sql,$db);
	mysql_close($db);
}
//Delete
if ($action=='delete'){
	$comp_id = $_GET['comp_id'];

	$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
	if(!$db){
		die("connot connect:" . mysql_error());
	}

	$dns = mysql_select_db(DB_NAME,$db);

	if(!$dns){
		die("connot use db:" . mysql_error());
	}

	mysql_set_charset('utf8');

	$sql = sprintf("delete from app_mg_comp_course WHERE comp_id = %d",$comp_id);
	$result = mysql_query($sql,$db);

	mysql_close($db);

}
//updateflg
if ($action=='updateflg'){
	$comp_id = $_GET['comp_id'];
	$flg = $_GET['flg'];

	$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
	if(!$db){
		die("connot connect:" . mysql_error());
	}

	$dns = mysql_select_db(DB_NAME,$db);

	if(!$dns){
		die("connot use db:" . mysql_error());
	}

	mysql_set_charset('utf8');

	$sql = sprintf("UPDATE app_mg_comp_course SET on_off_flg=%d WHERE comp_id = %d",$flg,$comp_id);
	$result = mysql_query($sql,$db);

	mysql_close($db);

}
//Search
if (($action=='search')||($action=='update')||($action=='delete')||($action=='updateflg')){

	$link = db_conn();
	mysql_set_charset('utf8');

	$page_size=100;

	if( isset($_GET['page']) ){
		$page = intval( $_GET['page'] );
	}
	else{
		$page = 1;
	}
	$rowCnt = 0;
	//会社名・部署名:
	$s_comp_id = $_POST['s_comp_id'];
	if($_GET['s_comp_id']!='') {
		$s_comp_id=$_GET['s_comp_id'];
	}
	if($s_comp_id==""){
		$s_comp_id=$get_comp_id;
	}
	//コース名
	$s_course_name = $_POST['s_course_name'];
	if($_GET['s_course_name']!='') {
		$s_course_name=$_GET['s_course_name'];
	}
	//All
	$sqlall = "select a.*,b.comp_name,b.dep_name from app_mg_comp_course a LEFT JOIN app_mg_comp b ON a.comp_id=b.comp_id WHERE 1 ";

	//会社ID
	if($s_comp_id!='') {
		$sqlall .= " and a.comp_id =".$s_comp_id;
	}
	//コース名
	if($s_course_name!='') {
		$sqlall .= " and a.course_name like '%".$s_course_name."%'";
	}

	$result = mysql_query($sqlall,$link) or die(mysql_error());

	if(!$result){
		$rowCnt = -1;
		db_disConn($result, $link);
	}
	$rowCntall=mysql_num_rows($result);

	$sql = sprintf("%s order by a.comp_id,a.insert_time limit %d,%d",$sqlall,($page-1)*$page_size,$page_size);

	$result = mysql_query($sql,$link);

	if(!$result){
		$rowCnt = -1;
		db_disConn($result, $link);
	}

	$rowCnt=mysql_num_rows($result);

	//paging
	if($rowCnt==0){
		$page_count = 0;
		db_disConn($result, $link);
	}
	else{
		if( $rowCntall<$page_size ){ $page_count = 1; }
		if( $rowCntall%$page_size ){
			$page_count = (int)($rowCntall / $page_size) + 1;
		}else{
			$page_count = $rowCntall / $page_size;
		}
	}
	$page_string = '';
	if (($page == 1)||($page_count == 1)){
		$page_string .= 'トップページ|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁(<b>'.$rowCntall.'</b>件)|';
	}
	else{
		$page_string .= '<a href=?action=search&page=1'.
				'&s_comp_id='.$s_comp_id.
				'&s_course_name='.$s_course_name.
				'>トップページ</a>|<a href=?action=search&page='.($page-1).
				'&s_comp_id='.$s_comp_id.
				'&s_course_name='.$s_course_name.
				'>前頁</a>|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁(<b>'.$rowCntall.'</b>件)|';
	}
	if( ($page == $page_count) || ($page_count == 0) ){
		$page_string .= '次頁|最終ページ';
	}
	else{
		$page_string .= '<a href=?action=search&page='.($page+1).
				'&s_comp_id='.$s_comp_id.
				'&s_course_name='.$s_course_name.
				'>次頁</a>|<a href=?action=search&page='.$page_count.
				'&s_comp_id='.$s_comp_id.
				'&s_course_name='.$s_course_name.
				'>最終ページ</a>';
	}
}
?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
	<title><?php echo $page_name; ?></title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" >
	<meta http-equiv="content-style-type" content="text/css">
	<meta http-equiv="content-script-type" content="text/javascript">
	<link href="../css/common.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="../js/common.js"></script>
	<script charset="utf-8" src="../js/jquery.js" type="text/javascript"></script>
	<script src="../js/jquery.blockUI.js" type="text/javascript"></script>
</head>
<body>
<div id="header">
	<div id="header_content">
		<h1><a href="<?php echo $home_page_url; ?>">公式アカウント管理画面</a></h1>
	</div>
</div>
<div id="nav">
	<div id="nav_content">
		<a href="<?php echo $home_page_url; ?>"><?php echo $home_page_name.' ＞ '; ?></a>
		<?php echo $page_name; ?>
	</div>
</div>
<div class='content'>
	<div style='float:left;margin-top:120px;margin-bottom:20px'>
		<form action='?action=search' method='post' name='form1'>
			<div style='float:left; text-align:left;width:180px;height:20px;' >
				会社ID:
			</div>
			<div style='float:left; text-align:left; width:296px;height:20px;' >
				<input type='text' name='s_comp_id' id='s_comp_id' style='width:296px;height:20px;' value='<?php echo $s_comp_id;?>'/>
			</div>
			<div style='clear:both;'></div><br/>
			<div style='float:left; text-align:left;width:180px;height:20px;' >
				コース名:
			</div>
			<div style='float:left; text-align:left; width:296px;height:20px;' >
				<input type='text' name='s_course_name' id='s_course_name' style='width:296px;height:20px;' value='<?php echo $s_course_name;?>'/>
			</div>
			<div style='clear:both;'></div><br/>
			<div style='float:left; text-align:left;' >
				<input type="submit" class="btn_search" value="検索" />
			</div>
			<div style='float:left; text-align:left;margin-left:40px;' >
				<input type="button" class="btn_search" value="コース作成" onclick="addCompCourse()" href="javascript:void(0)""/>
			</div>
			<div style='clear:both; margin-bottom:20px'></div>
			<?php
			if ($rowCnt>0){
				echo "<div>検索件数：$rowCntall 件</div><input type='button' class='btn_search' value='CSV出力' style='width:120px;' onclick='exportTo()' href='javascript:void(0)'/>";
				echo "
					<table width='100%' cellspacing='1' cellpadding='2'>
						<tr bgcolor='#DBE6F5'>
						  <td><span style='float:left; text-align:center'><font color=#666666>$page_string</font></span></td>
						</tr>
					</table>";
				echo "
					<table id='firm_table' width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
					<thead>
						<tr bgcolor='#DBE6F5'>
							<th width='180px'>操作</th>
							<th width='60px'>状態</th>
							<th width='100px'>会社ID</th>
							<th width='255px'>コース名</th>
							<th width='255px'>利用開始日</th>
							<th width='255px'>利用終了日</th>
							<th width='100px'>金額</th>
							<th width='255px'>会社名</th>
							<th width='255px'>部署名</th>
						</tr>
					</thead>
						<tbody>
				";
				$i=1;
				while($rs=mysql_fetch_object($result))
				{
					echo "
							<tr align='left' bgcolor='#EEF2F4'>
								<td width='180px'align='center'>
									<input type='button' class='btn2' value='更新' onclick=\"updateChange("."'up_course_name".$i."',"."'up_from".$i."',"."'up_to".$i."',"."'up_price".$i."',".$rs->comp_id.",".$page.")\" >
									";
									if($rs->on_off_flg=='1'){//無効
										//有効に
										echo"<input type='button' class='btn2' value='有効' onclick=\"updateOnOffFlgChange('".$rs->comp_id."',0,".$page.")\" >";
									}else{//有効
										//無効に
										echo"<input type='button' class='btn2' value='無効' onclick=\"updateOnOffFlgChange('".$rs->comp_id."',1,".$page.")\" >";
									}
					echo "
									<input type='button' class='btn3' value='削除' onclick=\"var ret=confirm('該当情報を削除します。よろしいですか？');if(ret)deleteInfo('".$rs->comp_id."',".$page.")\">
								</td>
					";
					if($rs->on_off_flg=='1'){//無効
						echo"<td width='60px'align='center'>無効</td>";
					}else{//有効
						echo"<td width='60px'align='center'>有効</td>";
					}
					echo "
							<td width='100px'align='center'>".$rs->comp_id."</td>
							<td width='255px'><input style='width:98%;' type='text' name='up_course_name".$i."' id='up_course_name".$i."' value=".$rs->course_name."></td>
							<td width='255px'align='center'><input  style='width:98%;' type='date' name='up_from".$i."' id='up_from".$i."' value=".date("Y-m-d H:i:s",$rs->from_date)."></td>
							<td width='255px'align='center'><input  style='width:98%;' type='date' name='up_to".$i."' id='up_to".$i."' value=".date("Y-m-d H:i:s",$rs->to_date)."></td>
							<td width='100px'><input type='number'style='width:98%;' type='text' name='up_price".$i."' id='up_price".$i."' value=".$rs->price."></td>
							<td width='255px'>".$rs->comp_name."</td>
							<td width='255px'>".$rs->dep_name."</td>
				";

					echo"
							</tr>
					";
					$i++;
				}
				echo "
					</tbody>
					</table>
					<table width='100%' cellspacing='1' cellpadding='2'>
						<tr bgcolor='#DBE6F5'>
						  <td><span style='float:left; text-align:center'><font color=#666666>$page_string</font></span></td>
						</tr>
					</table>";
				mysql_close($link);
			}else{
				if ($action=='search'){
					echo "<div>検索件数：0件</div>";
				}
			}
			?>
		</form>
		<script src="../js/tableExporter.min.js" type="text/javascript"></script>
		<script language="javascript" type="text/javascript">
			function exportTo() {
				$('#firm_table').tableExport({
					filename: 'export_comp_course_info_%DD%-%MM%-%YY%',
					format: 'csv',
					head_delimiter:',',
					column_delimiter:',',
					cols: '2,3,4,5,6,7,8,9'
				});
			}
			function deleteInfo(comp_id,page) {
				var pageurl="?action=delete&comp_id="+comp_id+"&page="+page;
				window.location.href=pageurl;
			}

			function updateOnOffFlgChange(comp_id,flg,page) {
				var pageurl="?action=updateflg&comp_id="+comp_id+"&flg="+flg+"&page="+page;
				window.location.href=pageurl;
			}
			function updateChange(up_course_name,up_from,up_to,up_price,comp_id,page) {
				var up_course_name=document.getElementById(up_course_name).value;
				var up_from=document.getElementById(up_from).value;
				var up_to=document.getElementById(up_to).value;
				var up_price=document.getElementById(up_price).value;
				var pageurl="?action=update&id="+comp_id+"&up_course_name="+encodeURIComponent(up_course_name)+"&up_from="+encodeURIComponent(up_from)+"&up_to="+encodeURIComponent(up_to)+"&up_price="+encodeURIComponent(up_price)+"&page="+page;
				window.location.href=pageurl;
			}
			function show(msg,id) {
				document.getElementById(id).value=msg;
			}
			function initSearch(){
				document.form1.action='?action=search';
				document.form1.submit();
			}
			function comp_course_list(comp_id){
				var pageurl="m_comp_course.php?action=search&comp_id="+comp_id;
				window.location.href=pageurl;
			}
		</script>
	</div>
	<div id="addCompCourseForm" style="text-align: center; display: none;">
		<form onsubmit="return false;" style="margin-bottom:0px;" method="post" action="add_comp_course.php" name="addCompCourseForm">
			<table border="0" style="font-size:12px; text-align:center; margin:30px auto;">
				<tbody>
				<tr>
					<td colspan="3" class="pirobox_up"><div class="piro_close" style="visibility: visible; display: block;"onclick="jQuery.unblockUI();initSearch();"></div></td>
				</tr>
				<tr>
					<td align="left" colspan="2">
						<div style="font-size:20px; text-align:left; font-weight:bold; color:#900;">
							公式アカウント新規作成
						</div>
						<div style='clear:both;'></div><br/>
					</td>
				</tr>
				<tr>
					<td align="left" colspan="2">
						<div style="color:#FC8B05; font-weight:bold;font-size:16px;width:150px;">*会社ID：</div>
						<input id="add_comp_id" type="text" class="nomaltext" name="add_comp_id" value=""/>
						<div style='clear:both;'></div><br/>
					</td>
				</tr>
				<tr>
					<td align="left" colspan="2">
						<div style="color:#FC8B05; font-weight:bold;font-size:16px;width:150px;">*コース名：</div>
						<input id="add_course_name" type="text" class="nomaltext" name="add_course_name" value=""/>
						<div style='clear:both;'></div><br/>
					</td>
				</tr>
				<tr>
					<td align="left" colspan="2">
						<div style="color:#FC8B05; font-weight:bold;font-size:16px;width:150px;">*利用開始日：</div>
						<input id="add_from_date" type="date" class="nomaltext add_from_date" name="add_from_date" value=""/>
						<div style='clear:both;'></div><br/>
					</td>
				</tr>
				<tr>
					<td align="left" colspan="2">
						<div style="color:#FC8B05; font-weight:bold;font-size:16px;width:150px;">*利用終了日：</div>
						<input id="add_to_date" type="date" class="nomaltext add_to_date" name="add_to_date" value=""/>
						<div style='clear:both;'></div><br/>
					</td>
				</tr>
				<tr>
					<td align="left" colspan="2">
						<div style="color:#FC8B05; font-weight:bold;font-size:16px;width:150px;">*金額：</div>
						<input type='number' id="add_price" type="text" class="nomaltext" name="add_price" value=""/>
						<div style='clear:both;'></div><br/>
					</td>
				</tr>
				<input type="file" id="input_chat_attachment" name="file" style="display: none;" accept="image/*" />
				<tr>
					<td align="center" colspan="2">
						<input type="button" style="color:white;font-size:16px; line-height:16px;background-color:orange;width:100px;height:30px;font-weight: bold;"onclick="var ret=confirm('公式情報を追加します。よろしいですか？');if(ret)addCompCourseInfoSubmit()" value="確認"/>
					</td>
				</tr>
				</tbody>
			</table>
		</form>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>