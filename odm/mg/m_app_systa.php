<?php
	//include
	require '../util/include.php';
	header('Cache-control: private, must-revalidate');
	$home_page_name='シス蔵管理メニュー';
	$home_page_url=URL_PATH;
	$f_page_name='アプリ管理メニュー';
	$f_page_url=URL_PATH.'m_app.php';
	$page_name='シスッター_投稿管理画面';

	$action = $_GET['action'];

	//Delete
	if ($action=='delete'){
		$c_id = $_GET['c_id'];

		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

		$sql = sprintf("delete from app_brid WHERE id = %d",$c_id);
		$result = mysql_query($sql,$db);

		mysql_close($db);

	}
	//Search
	if (($action=='search')||($action=='delete')){

		$link = db_conn();
		mysql_set_charset('utf8');

		$page_size=100;

		if( isset($_GET['page']) ){
		   $page = intval( $_GET['page'] );
		}
		else{
		   $page = 1;
		}
		$rowCnt = 0;

		$s_user_nick = $_POST['s_user_nick'];
		if($_GET['s_user_nick']!='') {
			$s_user_nick=$_GET['s_user_nick'];
		}
		$s_user_email = $_POST['s_user_email'];
		if($_GET['s_user_email']!='') {
			$s_user_email=$_GET['s_user_email'];
		}
		$s_content = $_POST['s_content'];
		if($_GET['s_content']!='') {
			$s_content=$_GET['s_content'];
		}
		$s_comments = $_POST['s_comments'];
		if($_GET['s_comments']!='') {
			$s_comments=$_GET['s_comments'];
		}
		$YYYY = $_POST['YYYY'];
		if($_GET['YYYY']!='') {
			$YYYY=$_GET['YYYY'];
		}
		$MM = $_POST['MM'];
		if($_GET['MM']!='') {
			$MM=$_GET['MM'];
		}
		$DD = $_POST['DD'];
		if($_GET['DD']!='') {
			$DD=$_GET['DD'];
		}
		$YYYY_TO = $_POST['YYYY_TO'];
		if($_GET['YYYY_TO']!='') {
			$YYYY_TO=$_GET['YYYY_TO'];
		}
		$MM_TO = $_POST['MM_TO'];
		if($_GET['MM_TO']!='') {
			$MM_TO=$_GET['MM_TO'];
		}
		$DD_TO = $_POST['DD_TO'];
		if($_GET['DD_TO']!='') {
			$DD_TO=$_GET['DD_TO'];
		}
		$sort_item = $_POST['sort_item'];
		if($_GET['sort_item']!='') {
			$sort_item=$_GET['sort_item'];
		}
		$sort_type = $_POST['sort_type'];
		if($_GET['sort_type']!='') {
			$sort_type=$_GET['sort_type'];
		}

		//All
		$sqlall = "select DISTINCT ak.*,am.user_nick,am.user_email
				from app_brid ak 
				LEFT JOIN app_brid_comments akc ON ak.id=akc.bird_id
				LEFT JOIN app_member am ON  am.user_id=ak.user_id 
				WHERE 1";

		//ニックネーム
		if($s_user_nick!='') {
			$sqlall .= " and am.user_nick like '%$s_user_nick%'";
		}
		//メールアドレス
		if($s_user_email!='') {
			$sqlall .= " and am.user_email like '%$s_user_email%'";
		}
		//内容
		if($s_content!='') {
			$sqlall .= " and ak.content like '%$s_content%'";
		}
		//コメント
		if($s_comments!='') {
			$sqlall .= " and akc.content like '%$s_comments%'";
		}
		//登録年月日From
		$MM=sprintf("%02d",$MM);
		$DD=sprintf("%02d",$DD);
		$insert_time_from=$YYYY.'-'.$MM.'-'.$DD.' 00:00:00';
		$insert_time_from=strtotime($insert_time_from);

		if($YYYY!='') {
			$sqlall .= " and ak.insert_time >= $insert_time_from ";
		}
		//登録年月日To
		$MM_TO=sprintf("%02d",$MM_TO);
		$DD_TO=sprintf("%02d",$DD_TO);
		$insert_time_to=$YYYY_TO.'-'.$MM_TO.'-'.$DD_TO.' 23:59:59';
		$insert_time_to=strtotime($insert_time_to);
		if($YYYY_TO!='') {
			$sqlall .= " and ak.insert_time <= $insert_time_to ";
		}
		$result = mysql_query($sqlall,$link) or die(mysql_error());

		if(!$result){
			$rowCnt = -1;
			db_disConn($result, $link);
		}
		$rowCntall=mysql_num_rows($result);

				//ソート順タイプ
		if($sort_type=='1'){
			$sort_str='desc';
		}
		elseif($sort_type=='2'){
			$sort_str='asc';
		}else{
			$sort_str='desc';
		}
		//ソート順
		if($sort_item=='1'){
			$sort_key='id';
		}
		elseif($sort_item=='2'){
			$sort_key='comments_sum';
		}else{
			$sort_key='id';
		}

		$sql = sprintf("%s GROUP BY ak.id order by ak.%s %s limit %d,%d",$sqlall,$sort_key,$sort_str,($page-1)*$page_size,$page_size);
		$result = mysql_query($sql,$link);

		if(!$result){
			$rowCnt = -1;
			db_disConn($result, $link);
		}

		$rowCnt=mysql_num_rows($result);

		//paging
		if($rowCnt==0){
			$page_count = 0;
			db_disConn($result, $link);
		}
		else{
			if( $rowCntall<$page_size ){ $page_count = 1; }
			if( $rowCntall%$page_size ){
				$page_count = (int)($rowCntall / $page_size) + 1;
			}else{
				$page_count = $rowCntall / $page_size;
			}
		}
		$page_string = '';
		if (($page == 1)||($page_count == 1)){
		   $page_string .= 'トップページ|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		else{
			$page_string .= '<a href=?action=search&page=1'.
							'&s_user_nick='.$s_user_nick.
							'&s_user_email='.$s_user_email.
							'&s_content='.$s_content.
							'&s_comments='.$s_comments.
							'&YYYY='.$YYYY.
							'&MM='.$MM.
							'&DD='.$DD.
							'&YYYY_TO='.$YYYY_TO.
							'&MM_TO='.$MM_TO.
							'&DD_TO='.$DD_TO.
							'&sort_item='.$sort_item.
							'&sort_type='.$sort_type.
							'>トップページ</a>|<a href=?action=search&page='.($page-1).
							'&s_user_nick='.$s_user_nick.
							'&s_user_email='.$s_user_email.
							'&s_title='.$s_title.
							'&s_content='.$s_content.
							'&s_comments='.$s_comments.
							'&YYYY='.$YYYY.
							'&MM='.$MM.
							'&DD='.$DD.
							'&YYYY_TO='.$YYYY_TO.
							'&MM_TO='.$MM_TO.
							'&DD_TO='.$DD_TO.
							'&sort_item='.$sort_item.
							'&sort_type='.$sort_type.
							'>前頁</a>|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		if( ($page == $page_count) || ($page_count == 0) ){
		   $page_string .= '次頁|最終ページ';
		}
		else{
			$page_string .= '<a href=?action=search&page='.($page+1).
							'&s_user_nick='.$s_user_nick.
							'&s_user_email='.$s_user_email.
							'&s_title='.$s_title.
							'&s_content='.$s_content.
							'&s_comments='.$s_comments.
							'&YYYY='.$YYYY.
							'&MM='.$MM.
							'&DD='.$DD.
							'&YYYY_TO='.$YYYY_TO.
							'&MM_TO='.$MM_TO.
							'&DD_TO='.$DD_TO.
							'&sort_item='.$sort_item.
							'&sort_type='.$sort_type.
							'>次頁</a>|<a href=?action=search&page='.$page_count.
							'&s_user_nick='.$s_user_nick.
							'&s_user_email='.$s_user_email.
							'&s_title='.$s_title.
							'&s_content='.$s_content.
							'&s_comments='.$s_comments.
							'&YYYY='.$YYYY.
							'&MM='.$MM.
							'&DD='.$DD.
							'&YYYY_TO='.$YYYY_TO.
							'&MM_TO='.$MM_TO.
							'&DD_TO='.$DD_TO.
							'&sort_item='.$sort_item.
							'&sort_type='.$sort_type.
							'>最終ページ</a>';
		}
	}



?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title><?php echo $page_name; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<link href="../css/common.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="../js/year.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script charset="utf-8" src="../js/jquery.js" type="text/javascript"></script>
<script src="../js/jquery.blockUI.js" type="text/javascript"></script>
</head>
<body>
<div id="header">
	<div id="header_content">
		<h1><a href="<?php echo $home_page_url; ?>">シス蔵管理画面</a></h1>
	</div>
</div>
<div id="nav">
	<div id="nav_content">
		<a href="<?php echo $home_page_url; ?>"><?php echo $home_page_name.' ＞ '; ?></a>
		<a href="<?php echo $f_page_url; ?>"><?php echo $f_page_name.' ＞ '; ?></a>
		<?php echo $page_name; ?>
	</div>
</div>
<div class='content'>
	<div style='float:left;margin-top:120px;margin-bottom:20px'>
		<form action='?action=search' method='post' name='form1'>
			<div style='float:left; text-align:left;margin:2px; width:180px;height:20px;' >
				投稿者ニックネーム:
			</div>
			<div style='float:left; text-align:left;margin:2px; width:300px;height:20px;' >
				<input type='text' name='s_user_nick' id='s_user_nick' style='width:300px;height:20px;' value='<?php echo $s_user_nick;?>'/>
			</div>
			<div style='float:left; text-align:left;margin:2px; width:180px;height:20px;margin-left:60px;' >
				投稿者メールアドレス:
			</div>
			<div style='float:left; text-align:left;margin:2px; width:300px;height:20px;' >
				<input type='text' name='s_user_email' id='s_user_email' style='width:300px;height:20px;' value='<?php echo $s_user_email;?>'/>
			</div>
			<div style='clear:both;'></div><br/>
			<div style='float:left; text-align:left;margin:2px; width:180px;height:20px;' >
				投稿内容キーワード:
			</div>
			<div style='float:left; text-align:left;margin:2px; width:300px;height:20px;' >
				<input type='text' name='s_content' id='s_content' style='width:300px;height:20px;' value='<?php echo $s_content;?>'/>
			</div>
			<div style='float:left; text-align:left;margin:2px; width:180px;height:20px;margin-left:60px;' >
				コメントキーワード:
			</div>
			<div style='float:left; text-align:left;margin:2px; width:300px;height:20px;' >
				<input type='text' name='s_comments' id='s_comments' style='width:300px;height:20px;' value='<?php echo $s_comments;?>'/>
			</div>
			<div style='clear:both;'></div><br/>
			<div style='float:left; text-align:left; width:180px;height:20px;' >
				投稿年月日From:
			</div>
			<div style='float:left; text-align:left;height:20px;' >
				<select name='YYYY' id='YYYY' onchange="YYYYDD(this.value)">
					<option value="">年を選択</option>
				</select>年
				<select name='MM' id='MM' onchange="MMDD(this.value)">
					<option value="">月を選択</option>
				</select>月
				<select name='DD' id='DD'>
					<option value="">日を選択</option>
				</select>日
			</div>
			<input type="hidden" name="s_year_from" id="s_year_from" value="<?php echo $YYYY;?>" />
			<input type="hidden" name="s_month_from" id="s_month_from" value="<?php echo $MM;?>" />
			<input type="hidden" name="s_day_from" id="s_day_from" value="<?php echo $DD;?>" />
			<div style='float:left; text-align:left; width:180px;height:20px;margin-left:60px;' >
				ソート順:
			</div>
			<div style='float:left; text-align:left; width:400px;height:25px;' >
				<input type="radio" name="sort_item" id="sort_item"value="1" <?php if ($sort_key=='id'||$sort_key=='') {echo 'checked=checked';}?>/>投稿日付
				<input type="radio" name="sort_item" id="sort_item"value="2" <?php if ($sort_key=='comments_sum') {echo 'checked=checked';}?>/>コメント数
			</div>
			<input type="hidden" name="sort_item_checked" id="sort_item_checked" value="<?php echo $sort_key;?>" />
			<div style='clear:both;'></div><br/>
			<div style='float:left; text-align:left; width:180px;height:20px;' >
				投稿年月日To:
			</div>
			<div style='float:left; text-align:left;height:20px;' >
				<select name='YYYY_TO' id='YYYY_TO' onchange="YYYYDD(this.value)">
					<option value="">年を選択</option>
				</select>年
				<select name='MM_TO' id='MM_TO' onchange="MMDDTO(this.value)">
					<option value="">月を選択</option>
				</select>月
				<select name='DD_TO' id='DD_TO'>
					<option value="">日を選択</option>
				</select>日
			</div>
			<input type="hidden" name="s_year_to" id="s_year_to" value="<?php echo $YYYY_TO;?>" />
			<input type="hidden" name="s_month_to" id="s_month_to" value="<?php echo $MM_TO;?>" />
			<input type="hidden" name="s_day_to" id="s_day_to" value="<?php echo $DD_TO;?>" />
			<div style='float:left; text-align:left; width:180px;height:20px;margin-left:60px;' >
				ソート順タイプ:
			</div>
			<div style='float:left; text-align:left; width:296px;height:25px;' >
				<input type="radio" name="sort_type" id="sort_type"value="1" <?php if ($sort_str=='desc'||$sort_str=='') {echo 'checked=checked';}?>/>降順
				<input type="radio" name="sort_type" id="sort_type"value="2" <?php if ($sort_str=='asc') {echo 'checked=checked';}?>/>昇順
			</div>
			<input type="hidden" name="sort_type_checked" id="sort_type_checked" value="<?php echo $sort_str;?>" />
			<div style='float:left; text-align:left;margin-left:60px;' >
				<input type="submit" class="btn_search" value="検索" />
			</div>
			<div style='clear:both; margin-bottom:20px'></div>
		<?php
			if ($rowCnt>0){
				echo "<div>検索件数：$rowCntall 件</div><input type='button' class='btn_search' value='CSV出力' style='width:120px;' onclick='exportCsv()' href='javascript:void(0)'/>";
				echo "
					<table width='100%' cellspacing='1' cellpadding='2'>
						<tr bgcolor='#DBE6F5'>
						  <td><span style='float:left; text-align:center'><font color=#666666>$page_string</font></span></td>
						</tr>
					</table>";
				echo "
					<table id='firm_table' width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
					<thead>
						<tr bgcolor='#DBE6F5'>
							<th width='60px'>操作</th>
							<th width='150px'>投稿者</th>
							<th width='200px'>投稿時間</th>
							<th width='100px'>コメント数</th>
							<th width='550px'>投稿内容</th>
							<th width='162px'>イメージ</th>
							<th width='200px'>メールアドレス</th>
							<th width='120px'>ID</th>
						</tr>
					</thead>
						<tbody>
				";
				$i=1;
				 while($rs=mysql_fetch_object($result))
				{
					echo "
							<tr align='left' bgcolor='#EEF2F4'>
								<td width='60px'align='center'>
									<input type='button' class='btn3' value='削除' onclick=\"var ret=confirm('該当投稿を削除します。よろしいですか？');if(ret)deleteInfo('".$rs->id."',".$page.")\">
								</td>
					";
					echo "
								<td width='150px'align='center'>".$rs->user_nick."</td>
								<td width='200px'align='center'>".date("Y-m-d H:i:s",$rs->insert_time)."</td>
					";
					if ($rs->comments_sum==0){
						  echo "
								<td width='100px'align='center'>$rs->comments_sum</td>
						  ";
					}else{
						  echo "
								<td width='100px'align='center'><a href='javascript:void(0)'onclick='searchSystaComments($rs->id)' >".$rs->comments_sum."</a></td>
						  ";
					}	
					echo "
								<td width='550px'>".$rs->content."</td>
					";
					if ($rs->pic!=''){
						  echo "
								<td width='162px'align='center' style='padding-top:5px;'><img src=".$rs->pic." width='160px' height='113px'></td>
						  ";
					}else{
						  echo "
								<td width='162px'align='center' style='padding-top:5px;'>&nbsp;</td>
						  ";
					}
					echo "
								<td width='200px'align='center'>".$rs->user_email."</td>
								<td width='120px'align='center'>".$rs->id."</td>							
					";
					echo "
							</tr>
					";
					$i++;
				}
				echo "
					</tbody>
				</table>
				<table width='100%' cellspacing='1' cellpadding='2'>
					<tr bgcolor='#DBE6F5'>
					  <td><span style='float:left; text-align:center'><font color=#666666>$page_string</font></span></td>
					</tr>
				</table>";
				mysql_close($link);
			}else{
				if ($action=='search'){
					echo "<div>検索件数：0件</div>";
				}
			}
		?>
		</form>
		<script src="../js/tableExporter.min.js" type="text/javascript"></script>
	<script language="javascript" type="text/javascript">
		function exportTo() {
			$('#firm_table').tableExport({
				filename: 'export_systa_info_%DD%-%MM%-%YY%',
				format: 'xls',
				head_delimiter:',',
				column_delimiter:',',
				cols: '2,3,4,5,7,8'
			});
		}
		function exportCsv(){
			var s_user_nick=document.getElementById("s_user_nick").value;//a
			var s_user_email=document.getElementById("s_user_email").value;//b
			var s_content=document.getElementById("s_content").value;//d
			var s_comments=document.getElementById("s_comments").value;//e
			var s_YYYY=document.getElementById("s_year_from").value;//g
			var s_MM=document.getElementById("s_month_from").value;//h
			var s_DD=document.getElementById("s_day_from").value;//i
			var s_sort_item=document.getElementById("sort_item_checked").value;//j
			var s_YYYY_TO=document.getElementById("s_year_to").value;//k
			var s_MM_TO=document.getElementById("s_month_to").value;//l
			var s_DD_TO=document.getElementById("s_day_to").value;//m
			var s_sort_type=document.getElementById("sort_type_checked").value;//n

			var pageurl="m_export_csv_systa.php?action=export&a="+s_user_nick+"&b="+s_user_email+"&d="+s_content+"&e="+s_comments+"&g="+s_YYYY+"&h="+s_MM+"&i="+s_DD+"&j="+s_sort_item+"&k="+s_YYYY_TO+"&l="+s_MM_TO+"&m="+s_DD_TO+"&n="+s_sort_type;

			window.location.href=pageurl;
		}
		function deleteInfo(c_id,page) {
			  var pageurl="?action=delete&c_id="+c_id+"&page="+page;
			  window.location.href=pageurl;
		}
		function show(msg,id) {
			document.getElementById(id).value=msg;
		}
		function initSearch(){
			document.form1.action='?action=search';
			document.form1.submit();
		}
	</script>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>