<?php
	//include
	require '../util/include.php';

	$home_page_name='シス蔵管理メニュー';
	$home_page_url=URL_PATH;
	$f_page_name='運営管理メニュー';
	$f_page_url=URL_PATH.'m_op.php';
	$page_name='会社情報CSV一括取込画面';

	$action = $_GET['action'];
	$msg_flg = 0;
	$msg="";

	//会社取込処理
	if ($action == 'import') {
        $filename = $_FILES['file']['tmp_name'];
        $i_area_id=$_POST['i_area_id'];
        if (empty ($filename)) {
            $msg_flg = 1;
            $msg = 'エラー：会社情報のCSVファイルを選択してください。';
        }
        else{
            $handle = fopen($filename, 'r');
            $result = input_csv($handle);
            $len_result = count($result);
            if($len_result==0){
                $msg_flg = 1;
                $msg = 'エラー：インポート対象レコードがありません。';
            }
            else{
                $db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                if(!$db){
                    die("connot connect:" . mysql_error());
                }

                $dns = mysql_select_db(DB_NAME,$db);

                if(!$dns){
                    die("connot use db:" . mysql_error());
                }

                mysql_set_charset('utf8');

                for ($i = 0; $i < $len_result; $i++) {
                    $area_id = $result[$i][0];     //地域名
                    $company_name = $result[$i][1];    //会社名
                    $company_name=addslashes($company_name);
                    if ($area_id!=""&&$i_area_id==$area_id&&$company_name!=""){

                        $sql = sprintf("insert into app_company (company_name,type,insert_time,city_id) VALUES ('%s',2,UNIX_TIMESTAMP(),%d)",$company_name,$area_id);
                        $result_u = mysql_query($sql,$db);

                        if(!$result_u){
                            $msg_flg = 1;
                            $msg = 'エラー：第'.($i+1).'行目以降、会社情報の取込は失敗。';
                            break;
                        }else{
                            $msg = '会社情報取込処理を正常に終了しました。';
                        }
                    }
                    else{
                        $msg_flg = 1;
                        $msg = 'エラー：第'.($i+1).'行目以降、会社情報の取込は失敗。';
                        break;
                    }
                }
                mysql_close($db);
            }
            fclose($handle);
        }
    }
?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title><?php echo $page_name; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<link href="../css/common.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="../js/common.js"></script>
<script charset="utf-8" src="../js/jquery.js" type="text/javascript"></script>
<script src="../js/jquery.blockUI.js" type="text/javascript"></script>
</head>
<body>
<div id="header">
	<div id="header_content">
		<h1><a href="<?php echo $home_page_url; ?>">シス蔵管理画面</a></h1>
	</div>
</div>
<div id="nav">
	<div id="nav_content">
		<a href="<?php echo $home_page_url; ?>"><?php echo $home_page_name.' ＞ '; ?></a>
		<a href="<?php echo $f_page_url; ?>"><?php echo $f_page_name.' ＞ '; ?></a>
		<?php echo $page_name; ?>
	</div>
</div>
<div class='content'>
    <div style='float:left;margin-top:120px;margin-bottom:20px'>
        <form action='?action=import' method='post' enctype='multipart/form-data'>
            <div style="color:#FC8B05; font-weight:bold;font-size:16px;width:100px;margin-left:20px;">
                対象地域名:
            </div>
            <select name='i_area_id' id='i_area_id' style='margin-left:20px;float:left;'>
                <option value='' ></option>
                <option value='1'>北海道</option>
                <option value='2'>青森県</option>
                <option value='3'>秋田県</option>
                <option value='4'>岩手県</option>
                <option value='5'>山形県</option>
                <option value='6'>宮城県</option>
                <option value='7'>福島県</option>
                <option value='8'>新潟県</option>
                <option value='9'>富山県</option>
                <option value='10'>石川県</option>
                <option value='11'>群馬県</option>
                <option value='12'>栃木県</option>
                <option value='13'>長野県</option>
                <option value='15'>埼玉県</option>
                <option value='16'>茨城県</option>
                <option value='17'>東京都</option>
                <option value='18'>千葉県</option>
                <option value='19'>神奈川県</option>
                <option value='20'>静岡県</option>
                <option value='21'>山梨県</option>
                <option value='22'>愛知県</option>
                <option value='14'>岐阜県</option>
                <option value='23'>福井県</option>
                <option value='24'>滋賀県</option>
                <option value='25'>三重県</option>
                <option value='26'>京都府</option>
                <option value='27'>奈良県</option>
                <option value='28'>兵庫県</option>
                <option value='29'>大阪府</option>
                <option value='30'>和歌山県</option>
                <option value='31'>島根県</option>
                <option value='32'>鳥取県</option>
                <option value='33'>岡山県</option>
                <option value='34'>広島県</option>
                <option value='35'>山口県</option>
                <option value='36'>香川県</option>
                <option value='37'>愛媛県</option>
                <option value='38'>徳島県</option>
                <option value='39'>高知県</option>
                <option value='40'>大分県</option>
                <option value='41'>福岡県</option>
                <option value='42'>佐賀県</option>
                <option value='43'>熊本県</option>
                <option value='44'>長崎県</option>
                <option value='45'>宮崎県</option>
                <option value='46'>鹿児島県</option>
                <option value='47'>沖縄県</option>
            </select>
            <div style='clear:both;'></div>
            <div style="color:#FC8B05; font-weight:bold;font-size:16px;width:200px;margin-left:20px;margin-top:20px;">
                会社情報のCSVファイル：
            </div>
            <div style='clear:both;'></div>
            <div style='float:left; text-align:left;margin-left:20px;' >
                <input type="file" name="file"/>
            </div>
            <div style='clear:both;'>
            <input type="submit" class="btn" value="取込" style='float:left;margin-left:20px;margin-top:20px;'/>
            <div style='clear:both;'>
        </form>
        <?php
        if (($action == 'import')){
            if ($msg_flg==0){
                echo "
						<div style='float:left; text-align:left;margin:20px;' >
							取込結果：<br/><br/>
							<font color='#33CC00'>$msg</font>
						</div>
						";
            }
            else{
                echo "
						<div style='float:left; text-align:left;margin:20px;;' >
							取込結果：<br/><br/>
							<font color='#FF0000'>$msg</font>
						</div>
						";
            }
        }
        ?>
    </div>
    <div style='clear:both;'></div>
</div>
</body>
</html>