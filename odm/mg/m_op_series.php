<?php
	//運営管理⇒現場の掃き溜めに会社・現場（都道府県）追加管理画面
	//include
	require '../util/include.php';

	$home_page_name='シス蔵管理メニュー';
	$home_page_url=URL_PATH;
	$f_page_name='運営管理メニュー';
	$f_page_url=URL_PATH.'m_op.php';
	$page_name='俺のシリーズ分類管理画面';

	$action = $_GET['action'];

	$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
	if(!$db){
		die("connot connect:" . mysql_error());
	}

	$dns = mysql_select_db(DB_NAME,$db);

	if(!$dns){
		die("connot use db:" . mysql_error());
	}

	mysql_set_charset('utf8');

	$sqlall = "select * from app_my_series WHERE 1";

	$result_co = mysql_query($sqlall,$db);

	mysql_close($db);

	//Delete
	if ($action=='delete'){
		$c_id = $_GET['c_id'];

		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

		$sql = sprintf("delete from app_my_series WHERE company_id = %d",$c_id);
		$result = mysql_query($sql,$db);

		mysql_close($db);

	}
	//Update
	if ($action=='update'){
		$c_id = $_GET['c_id'];
		$up_series_name = $_GET['name'];
		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

		$sql = sprintf("UPDATE app_my_series SET category_name ='%s' WHERE category_id = %d",$up_series_name,$c_id);
		$result = mysql_query($sql,$db);

		mysql_close($db);

	}
	//permission
	if ($action=='permission'){
		$c_id = $_GET['c_id'];

		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

		$sql = sprintf("UPDATE app_my_series SET status =1 WHERE category_id = %d",$c_id);
		$result = mysql_query($sql,$db);

		mysql_close($db);

	}
	//unPermission
	if ($action=='unpermission'){
		$c_id = $_GET['c_id'];

		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

		$sql = sprintf("UPDATE app_my_series SET status =0 WHERE category_id = %d",$c_id);
		$result = mysql_query($sql,$db);

		mysql_close($db);

	}
	//Search
	if (($action=='search')||($action=='delete')||($action=='update')||($action=='permission')||($action=='unpermission')){

		$link = db_conn();
		mysql_set_charset('utf8');

		$page_size=10;

		if( isset($_GET['page']) ){
		   $page = intval( $_GET['page'] );
		}
		else{
		   $page = 1;
		}
		$rowCnt = 0;

		$category_id = $_POST['series_name'];
		$status = $_POST['status'];

		//All
		$sqlall = "select * from app_my_series WHERE 1";

		if($category_id!='') {
			$sqlall .= " and category_id = $category_id";
		}
		if($status!='') {
			$sqlall .= " and status =$status";
		}

		$result = mysql_query($sqlall,$link) or die(mysql_error());

		if(!$result){
			$rowCnt = -1;
			db_disConn($result, $link);
		}
		$rowCntall=mysql_num_rows($result);

		//Select current all
		$sql = sprintf("%s order by category_id desc  limit %d,%d",$sqlall,($page-1)*$page_size,$page_size);

		$result = mysql_query($sql,$link);

		if(!$result){
			$rowCnt = -1;
			db_disConn($result, $link);
		}

		$rowCnt=mysql_num_rows($result);

		//paging
		if($rowCnt==0){
			$page_count = 0;
			db_disConn($result, $link);
		}
		else{
			if( $rowCntall<$page_size ){ $page_count = 1; }
			if( $rowCntall%$page_size ){
				$page_count = (int)($rowCntall / $page_size) + 1;
			}else{
				$page_count = $rowCntall / $page_size;
			}
		}
		$page_string = '';
		if (($page == 1)||($page_count == 1)){
		   $page_string .= 'トップページ|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		else{
		   $page_string .= '<a href=?action=search&page=1>トップページ</a>|<a href=?action=search&page='.($page-1).'>前頁</a>|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		if( ($page == $page_count) || ($page_count == 0) ){
		   $page_string .= '次頁|最終ページ';
		}
		else{
		   $page_string .= '<a href=?action=search&page='.($page+1).'>次頁</a>|<a href=?action=search&page='.$page_count.'>最終ページ</a>';
		}
	}



?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title><?php echo $page_name; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<link href="../css/common.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="../js/common.js"></script>
<script charset="utf-8" src="../js/jquery.js" type="text/javascript"></script>
<script src="../js/jquery.blockUI.js" type="text/javascript"></script>
</head>
<body>
<div id="header">
	<div id="header_content">
		<h1><a href="<?php echo $home_page_url; ?>">シス蔵管理画面</a></h1>
	</div>
</div>
<div id="nav">
	<div id="nav_content">
		<a href="<?php echo $home_page_url; ?>"><?php echo $home_page_name.' ＞ '; ?></a>
		<a href="<?php echo $f_page_url; ?>"><?php echo $f_page_name.' ＞ '; ?></a>
		<?php echo $page_name; ?>
	</div>
</div>
<div class='content'>
	<div style='float:left;margin-top:120px;margin-bottom:20px'>
		<form action='?action=search' method='post' name='form1'>
			<div style='float:left; text-align:left;margin:2px; width:180px;height:20px;' >
				俺のシリーズ分類名:
			</div>
			<div style='float:left; text-align:left;margin:2px; width:300px;height:20px;' >
				<select name="series_name" style='margin-left:2px;' />
				<option value=""></option>
				<?php
		        while($arr_series_row=mysql_fetch_array($result_co)){
		        	$arr_series_name[] = $arr_series_row;
		        ?>
		        	<option value="<?=$arr_series_row[category_id]?>"><?=$arr_series_row[category_name]?></option>
		        <?php
		        }
        		?>
        		</select>
			</div>
			<div style='clear:both;'></div><br/>
			<div style='float:left; text-align:left;margin:2px; width:180px;height:20px;' >
				承認状況:
			</div>
			<select name='status' id='status' style='float:left; margin-left:2px;'>
						<option value='' <?php if ($status=='') {echo 'selected';}?>></option>
				        <option value='0' <?php if ($status=='0') {echo 'selected';}?>>未承認</option>
                        <option value='1' <?php if ($status=='1') {echo 'selected';}?>>承認済</option>
			</select>
			<div style='float:left; text-align:left;margin-left:60px;' >
				<input type="submit" class="btn_search" value="検索" />
			</div>
			<div style='float:left; text-align:left;margin-left:40px;' >
				<input type="button" class="btn_search" value="分類追加" onclick="addseries()" href="javascript:void(0)""/>
			</div>
			<div style='clear:both; margin-bottom:20px'></div>
		<?php
			if ($rowCnt>0){
				echo "
					<table width='100%' cellspacing='1' cellpadding='2'>
						<tr bgcolor='#DBE6F5'>
						  <td><span style='float:left; text-align:center'><font color=#666666>$page_string</font></span></td>
						</tr>
					</table>";
				echo "
					<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
						<tr bgcolor='#DBE6F5'>
							<th width='120px'>操作</th>
							<th width='60px'>ID</th>
							<th width='550px'>分類名</th>
							<th width='200px'>記入時間</th>
						</tr>
					</table>
				";
				$i=1;
				 while($rs=mysql_fetch_object($result))
				{
				 if($rs->status=='1') {
					  echo "
						   <table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
								<tr align='left' bgcolor='#EEF2F4'>
									<td width='120px'align='center' >
										<input type='button' class='btn2' value='取下' style='color:red;' onclick=\"var ret=confirm('該当分類情報を取下します。よろしいですか？');if(ret)unPermissionInfo('".$rs->category_id."',".$page.")\">
										<input type='button' class='btn2' value='更新' onclick=\"updateChange("."'up_series_name".$i."',".$rs->category_id.",".$page.")\">
									</td>
						";
				  }
				  else{
						echo "
						   <table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
								<tr align='left' bgcolor='#EEF2F4'>
									<td width='120px'align='center' >
										<input type='button' class='btn2' value='承認' onclick=\"var ret=confirm('該当分類情報を承認します。よろしいですか？');if(ret)permissionInfo('".$rs->category_id."',".$page.")\">
										<input type='button' class='btn2' value='更新' onclick=\"updateChange("."'up_series_name".$i."',".$rs->category_id.",".$page.")\">
									</td>
						";
				  }
				  echo "
								<td width='60px'align='center'>".$rs->category_id."</td>
								<td width='550px'><input type='text' name='up_series_name".$i."' id='up_series_name".$i."' value=".$rs->category_name." style='width:546px;' ></input></td>
								<td width='200px'align='center'>".date("Y-m-d H:i:s",$rs->insert_time)."</td>
					";
				  echo "
							</tr>
					  </table>
					  </td>
					  </tr>
					</table>
					";
					$i++;
				}
				echo "
					<table width='100%' cellspacing='1' cellpadding='2'>
						<tr bgcolor='#DBE6F5'>
						  <td><span style='float:left; text-align:center'><font color=#666666>$page_string</font></span></td>
						</tr>
					</table>";
				mysql_close($link);
			}else{
				if ($action=='search'){
					echo "検索結果がありません。";
				}
			}
		?>
		</form>
	<script language="javascript" type="text/javascript">
		function deleteInfo(c_id,page) {
			  var pageurl="?action=delete&c_id="+c_id+"&page="+page;
			  window.location.href=pageurl;
		}
		function show(msg,id) {
			document.getElementById(id).value=msg;
		}
		function initSearch(){
			document.form1.action='?action=search';
			document.form1.submit();
		}
		function updateChange(up_series_name,c_id,page) {
			var series_name=document.getElementById(up_series_name).value;
			var pageurl="?action=update&c_id="+c_id+"&name="+series_name+"&page="+page;
			window.location.href=pageurl;
		}
		function permissionInfo(c_id,page) {
			var pageurl="?action=permission&c_id="+c_id+"&page="+page;
			window.location.href=pageurl;
		}
		function unPermissionInfo(c_id,page) {
			var pageurl="?action=unpermission&c_id="+c_id+"&page="+page;
			window.location.href=pageurl;
		}
	</script>
	</div>
	<div class="clearboth"></div>
</div>
<div id="addseriesForm" style="text-align: center; display: none;">
    <form onsubmit="return false;" style="margin-bottom:0px;" method="post" action="add_series_info.php" name="addseriesForm">
        <table border="0" style="font-size:12px; text-align:center; margin:30px auto;">
            <tbody>
            	<tr>
            		<td colspan="3" class="pirobox_up"><div class="piro_close" style="visibility: visible; display: block;"onclick="jQuery.unblockUI();initSearch();"></div></td>
            	</tr>
                <tr>
                    <td align="left" colspan="2">
                        <div style="font-size:20px; text-align:left; font-weight:bold; color:#900;">
                            俺のシリーズ分類情報追加
                        </div>
                        <div style='clear:both;'></div><br/>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
	                        <div style="color:#FC8B05; font-weight:bold;font-size:16px;width:100px;">分類名：</div>
	                        	<input id="add_series_name" type="text" class="nomaltext" name="add_series_name" value="俺の"></input>
                       		<div style='clear:both;'></div><br/>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <input type="button" style="color:white;font-size:16px; line-height:16px;background-color:orange;width:100px;height:30px;font-weight: bold;"onclick="var ret=confirm('分類情報を追加します。よろしいですか？');if(ret)addseriesSubmit()" value="確認"></input>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
</body>
</html>