<?php
	//include
	require '../util/include.php';
	header('Cache-control: private, must-revalidate');
	$home_page_name='シス蔵管理メニュー';
	$home_page_url=URL_PATH;
	$f_page_name='アプリ管理メニュー';
	$f_page_url=URL_PATH.'m_app.php';
	$page_name='知恵袋_投稿管理画面';

	$action = $_GET['action'];

	//Delete
	if ($action=='delete'){
		$c_id = $_GET['c_id'];

		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

		$sql = sprintf("delete from app_know WHERE id = %d",$c_id);
		$result = mysql_query($sql,$db);

		mysql_close($db);

	}
	//Update
	if ($action=='update'){
		$id = $_GET['id'];
		//$update_contents = str_replace("；；；","<br>",urldecode($_GET['contents']));
		$update_contents = urldecode($_GET['contents']);
		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

		$sql = sprintf("UPDATE app_know SET content ='%s' WHERE id = %d",$update_contents,$id);
		$result = mysql_query($sql,$db);

		mysql_close($db);

	}
	//Search
	if (($action=='search')||($action=='delete')||($action=='update')){

		$link = db_conn();
		mysql_set_charset('utf8');

		$page_size=100;

		if( isset($_GET['page']) ){
		   $page = intval( $_GET['page'] );
		}
		else{
		   $page = 1;
		}
		$rowCnt = 0;

		$s_user_nick = $_POST['s_user_nick'];
		if($_GET['s_user_nick']!='') {
			$s_user_nick=$_GET['s_user_nick'];
		}
		$s_user_email = $_POST['s_user_email'];
		if($_GET['s_user_email']!='') {
			$s_user_email=$_GET['s_user_email'];
		}
		$s_title = $_POST['s_title'];
		if($_GET['s_title']!='') {
			$s_title=$_GET['s_title'];
		}
		$s_content = $_POST['s_content'];
		if($_GET['s_content']!='') {
			$s_content=$_GET['s_content'];
		}
		$s_comments = $_POST['s_comments'];
		if($_GET['s_comments']!='') {
			$s_comments=$_GET['s_comments'];
		}

		$YYYY = $_POST['YYYY'];
		if($_GET['YYYY']!='') {
			$YYYY=$_GET['YYYY'];
		}
		$MM = $_POST['MM'];
		if($_GET['MM']!='') {
			$MM=$_GET['MM'];
		}
		$DD = $_POST['DD'];
		if($_GET['DD']!='') {
			$DD=$_GET['DD'];
		}
		$YYYY_TO = $_POST['YYYY_TO'];
		if($_GET['YYYY_TO']!='') {
			$YYYY_TO=$_GET['YYYY_TO'];
		}
		$MM_TO = $_POST['MM_TO'];
		if($_GET['MM_TO']!='') {
			$MM_TO=$_GET['MM_TO'];
		}
		$DD_TO = $_POST['DD_TO'];
		if($_GET['DD_TO']!='') {
			$DD_TO=$_GET['DD_TO'];
		}

		$sort_item = $_POST['sort_item'];
		if($_GET['sort_item']!='') {
			$sort_item=$_GET['sort_item'];
		}
		$sort_type = $_POST['sort_type'];
		if($_GET['sort_type']!='') {
			$sort_type=$_GET['sort_type'];
		}
		$s_sts = $_POST['s_sts'];
		if($_GET['s_sts']!='') {
			$s_sts=$_GET['s_sts'];
		}
		//All
		$sqlall = "select DISTINCT ak.*,am.user_nick,am.user_email
				from app_know ak 
				LEFT JOIN app_know_comments akc ON ak.id=akc.info_id
				LEFT JOIN app_member am ON  am.user_id=ak.user_id 
				WHERE 1";
		//状況
		if($s_sts=='2') {
			$sqlall .= " and ak.urgent=2";
		}
		elseif($s_sts=='3') {
			$sqlall .= " and ak.urgent=1";
		}
		//ニックネーム
		if($s_user_nick!='') {
			$sqlall .= " and am.user_nick like '%$s_user_nick%'";
		}
		//メールアドレス
		if($s_user_email!='') {
			$sqlall .= " and am.user_email like '%$s_user_email%'";
		}
		//タイトル
		if($s_title!='') {
			$sqlall .= " and ak.title like '%$s_title%'";
		}
		//内容
		if($s_content!='') {
			$sqlall .= " and ak.content like '%$s_content%'";
		}
		//コメント
		if($s_comments!='') {
			$sqlall .= " and akc.content like '%$s_comments%'";
		}
		//登録年月日From
		$MM=sprintf("%02d",$MM);
		$DD=sprintf("%02d",$DD);
		$insert_time_from=$YYYY.'-'.$MM.'-'.$DD.' 00:00:00';
		$insert_time_from=strtotime($insert_time_from);

		if($YYYY!='') {
			$sqlall .= " and ak.insert_time >= $insert_time_from ";
		}
		//登録年月日To
		$MM_TO=sprintf("%02d",$MM_TO);
		$DD_TO=sprintf("%02d",$DD_TO);
		$insert_time_to=$YYYY_TO.'-'.$MM_TO.'-'.$DD_TO.' 23:59:59';
		$insert_time_to=strtotime($insert_time_to);
		if($YYYY_TO!='') {
			$sqlall .= " and ak.insert_time <= $insert_time_to ";
		}

		$result = mysql_query($sqlall,$link) or die(mysql_error());

		if(!$result){
			$rowCnt = -1;
			db_disConn($result, $link);
		}
		$rowCntall=mysql_num_rows($result);

				//ソート順タイプ
		if($sort_type=='1'){
			$sort_str='desc';
		}
		elseif($sort_type=='2'){
			$sort_str='asc';
		}else{
			$sort_str='desc';
		}
		//ソート順
		if($sort_item=='1'){
			$sort_key='id';
		}
		elseif($sort_item=='2'){
			$sort_key='comments_sum';
		}else{
			$sort_key='id';
		}

		$sql = sprintf("%s GROUP BY ak.id order by ak.%s %s limit %d,%d",$sqlall,$sort_key,$sort_str,($page-1)*$page_size,$page_size);
		$sqlcnt = sprintf("%s GROUP BY ak.id ",$sqlall);
		$result = mysql_query($sql,$link);
		$resultcnt = mysql_query($sqlcnt,$link);
		$rowCntall_group=mysql_num_rows($resultcnt);

		if(!$result){
			$rowCnt = -1;
			db_disConn($result, $link);
		}
		if(!$resultcnt){
			$rowCnt = -1;
			db_disConn($resultcnt, $link);
		}

		$rowCnt=mysql_num_rows($result);

		//paging
		if($rowCnt==0){
			$page_count = 0;
			db_disConn($result, $link);
		}
		else{
			if( $rowCntall_group<$page_size ){ $page_count = 1; }
			if( $rowCntall_group%$page_size ){
				$page_count = (int)($rowCntall_group / $page_size) + 1;
			}else{
				$page_count = $rowCntall_group / $page_size;
			}
		}
		$page_string = '';
		if (($page == 1)||($page_count == 1)){
		   $page_string .= 'トップページ|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		else{
			$page_string .= '<a href=?action=search&page=1'.
							'&s_user_nick='.$s_user_nick.
							'&s_user_email='.$s_user_email.
							'&s_title='.$s_title.
							'&s_content='.$s_content.
							'&s_comments='.$s_comments.
							'&YYYY='.$YYYY.
							'&MM='.$MM.
							'&DD='.$DD.
							'&YYYY_TO='.$YYYY_TO.
							'&MM_TO='.$MM_TO.
							'&DD_TO='.$DD_TO.
							'&sort_item='.$sort_item.
							'&sort_type='.$sort_type.
							'&s_sts='.$s_sts.
							'>トップページ</a>|<a href=?action=search&page='.($page-1).
							'&s_user_nick='.$s_user_nick.
							'&s_user_email='.$s_user_email.
							'&s_title='.$s_title.
							'&s_content='.$s_content.
							'&s_comments='.$s_comments.
							'&YYYY='.$YYYY.
							'&MM='.$MM.
							'&DD='.$DD.
							'&YYYY_TO='.$YYYY_TO.
							'&MM_TO='.$MM_TO.
							'&DD_TO='.$DD_TO.
							'&sort_item='.$sort_item.
							'&sort_type='.$sort_type.
							'&s_sts='.$s_sts.
							'>前頁</a>|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		if( ($page == $page_count) || ($page_count == 0) ){
		   $page_string .= '次頁|最終ページ';
		}
		else{
			$page_string .= '<a href=?action=search&page='.($page+1).
							'&s_user_nick='.$s_user_nick.
							'&s_user_email='.$s_user_email.
							'&s_title='.$s_title.
							'&s_content='.$s_content.
							'&s_comments='.$s_comments.
							'&YYYY='.$YYYY.
							'&MM='.$MM.
							'&DD='.$DD.
							'&YYYY_TO='.$YYYY_TO.
							'&MM_TO='.$MM_TO.
							'&DD_TO='.$DD_TO.
							'&sort_item='.$sort_item.
							'&sort_type='.$sort_type.
							'&s_sts='.$s_sts.
							'>次頁</a>|<a href=?action=search&page='.$page_count.
							'&s_user_nick='.$s_user_nick.
							'&s_user_email='.$s_user_email.
							'&s_title='.$s_title.
							'&s_content='.$s_content.
							'&s_comments='.$s_comments.
							'&YYYY='.$YYYY.
							'&MM='.$MM.
							'&DD='.$DD.
							'&YYYY_TO='.$YYYY_TO.
							'&MM_TO='.$MM_TO.
							'&DD_TO='.$DD_TO.
							'&sort_item='.$sort_item.
							'&sort_type='.$sort_type.
							'&s_sts='.$s_sts.
							'>最終ページ</a>';
		}
	}



?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title><?php echo $page_name; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<link href="../css/common.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="../js/year.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script charset="utf-8" src="../js/jquery.js" type="text/javascript"></script>
<script src="../js/jquery.blockUI.js" type="text/javascript"></script>
</head>
<body>
<div id="header">
	<div id="header_content">
		<h1><a href="<?php echo $home_page_url; ?>">シス蔵管理画面</a></h1>
	</div>
</div>
<div id="nav">
	<div id="nav_content">
		<a href="<?php echo $home_page_url; ?>"><?php echo $home_page_name.' ＞ '; ?></a>
		<a href="<?php echo $f_page_url; ?>"><?php echo $f_page_name.' ＞ '; ?></a>
		<?php echo $page_name; ?>
	</div>
</div>
<div class='content'>
	<div style='float:left;margin-top:120px;margin-bottom:20px'>
		<form action='?action=search' method='post' name='form1'>
			<div style='float:left; text-align:left;margin:2px; width:180px;height:20px;' >
				投稿者ニックネーム:
			</div>
			<div style='float:left; text-align:left;margin:2px; width:300px;height:20px;' >
				<input type='text' name='s_user_nick' id='s_user_nick' style='width:300px;height:20px;' value='<?php echo $s_user_nick;?>'/>
			</div>
			<div style='float:left; text-align:left;margin:2px; width:180px;height:20px;margin-left:60px;' >
				投稿者メールアドレス:
			</div>
			<div style='float:left; text-align:left;margin:2px; width:300px;height:20px;' >
				<input type='text' name='s_user_email' id='s_user_email' style='width:300px;height:20px;' value='<?php echo $s_user_email;?>'/>
			</div>
			<div style='clear:both;'></div><br/>
			<div style='float:left; text-align:left;margin:2px; width:180px;height:20px;' >
				投稿タイトル:
			</div>
			<div style='float:left; text-align:left;margin:2px; width:300px;height:20px;' >
				<input type='text' name='s_title' id='s_title' style='width:300px;height:20px;' value='<?php echo $s_title;?>'/>
			</div>
			<div style='float:left; text-align:left;margin:2px; width:180px;height:20px;margin-left:60px;' >
				投稿内容キーワード:
			</div>
			<div style='float:left; text-align:left;margin:2px; width:300px;height:20px;' >
				<input type='text' name='s_content' id='s_content' style='width:300px;height:20px;' value='<?php echo $s_content;?>'/>
			</div>
			<div style='clear:both;'></div><br/>
			<div style='float:left; text-align:left;margin:2px; width:180px;height:20px;' >
				コメントキーワード:
			</div>
			<div style='float:left; text-align:left;margin:2px; width:300px;height:20px;' >
				<input type='text' name='s_comments' id='s_comments' style='width:300px;height:20px;' value='<?php echo $s_comments;?>'/>
			</div>
			<div style='float:left; text-align:left;margin:2px; width:180px;height:20px;margin-left:60px;' >
				状況:
			</div>
			<div style='float:left; text-align:left; width:400px;height:25px;' >
				<input type="radio" name="s_sts" id="s_sts" value="1" <?php if ($s_sts=='1'||$s_sts=='') {echo 'checked=checked';}?>/>両方とも
				<input type="radio" name="s_sts" id="s_sts" value="2" <?php if ($s_sts=='2') {echo 'checked=checked';}?>/>緊急のみ
				<input type="radio" name="s_sts" id="s_sts" value="3" <?php if ($s_sts=='3') {echo 'checked=checked';}?>/>通常のみ
			</div>
			<input type="hidden" name="s_sts_checked" id="s_sts_checked" value="<?php echo $s_sts;?>" />
			<div style='clear:both;'></div><br/>
			<div style='float:left; text-align:left; width:180px;height:20px;' >
				投稿年月日From:
			</div>
			<div style='float:left; text-align:left;height:20px;' >
				<select name='YYYY' id='YYYY' onchange="YYYYDD(this.value)">
					<option value="">年を選択</option>
				</select>年
				<select name='MM' id='MM' onchange="MMDD(this.value)">
					<option value="">月を選択</option>
				</select>月
				<select name='DD' id='DD'>
					<option value="">日を選択</option>
				</select>日
			</div>
			<input type="hidden" name="s_year_from" id="s_year_from" value="<?php echo $YYYY;?>" />
			<input type="hidden" name="s_month_from" id="s_month_from" value="<?php echo $MM;?>" />
			<input type="hidden" name="s_day_from" id="s_day_from" value="<?php echo $DD;?>" />
			<div style='float:left; text-align:left; width:180px;height:20px;margin-left:60px;' >
				ソート順:
			</div>
			<div style='float:left; text-align:left; width:400px;height:25px;' >
				<input type="radio" name="sort_item" id="sort_item" value="1" <?php if ($sort_key=='id'||$sort_key=='') {echo 'checked=checked';}?>/>投稿日付
				<input type="radio" name="sort_item" id="sort_item" value="2" <?php if ($sort_key=='comments_sum') {echo 'checked=checked';}?>/>コメント数
			</div>
			<input type="hidden" name="sort_item_checked" id="sort_item_checked" value="<?php echo $sort_key;?>" />
			<div style='clear:both;'></div><br/>
			<div style='float:left; text-align:left; width:180px;height:20px;' >
				投稿年月日To:
			</div>
			<div style='float:left; text-align:left;height:20px;' >
				<select name='YYYY_TO' id='YYYY_TO' onchange="YYYYDD(this.value)">
					<option value="">年を選択</option>
				</select>年
				<select name='MM_TO' id='MM_TO' onchange="MMDDTO(this.value)">
					<option value="">月を選択</option>
				</select>月
				<select name='DD_TO' id='DD_TO' >
					<option value="">日を選択</option>
				</select>日
			</div>
			<input type="hidden" name="s_year_to" id="s_year_to" value="<?php echo $YYYY_TO;?>" />
			<input type="hidden" name="s_month_to" id="s_month_to" value="<?php echo $MM_TO;?>" />
			<input type="hidden" name="s_day_to" id="s_day_to" value="<?php echo $DD_TO;?>" />
			<div style='float:left; text-align:left; width:180px;height:20px;margin-left:60px;' >
				ソート順タイプ:
			</div>
			<div style='float:left; text-align:left; width:296px;height:25px;' >
				<input type="radio" name="sort_type" id="sort_type" value="1" <?php if ($sort_str=='desc'||$sort_str=='') {echo 'checked=checked';}?>/>降順
				<input type="radio" name="sort_type" id="sort_type" value="2" <?php if ($sort_str=='asc') {echo 'checked=checked';}?>/>昇順
			</div>
			<input type="hidden" name="sort_type_checked" id="sort_type_checked" value="<?php echo $sort_str;?>" />
			<div style='float:left; text-align:left;margin-left:60px;' >
				<input type="submit" class="btn_search" value="検索" />
			</div>
			<div style='clear:both; margin-bottom:20px'></div>
		<?php
			if ($rowCnt>0){
				echo "<div>検索件数：$rowCntall 件</div><input type='button' class='btn_search' value='CSV出力' style='width:120px;' onclick='exportCsv()' href='javascript:void(0)'/>";
				echo "
					<table width='100%' cellspacing='1' cellpadding='2'>
						<tr bgcolor='#DBE6F5'>
						  <td><span style='float:left; text-align:center'><font color=#666666>$page_string</font></span></td>
						</tr>
					</table>";
				echo "
					<table id='firm_table' width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
					<thead>
						<tr bgcolor='#DBE6F5'>
							<th width='120px'>操作</th>
							<th width='150px'>投稿者</th>
							<th width='200px'>投稿時間</th>
							<th width='100px'>コメント数</th>
							<th width='200px'>投稿タイトル</th>
							<th width='550px'>投稿内容</th>
							<th width='162px'>イメージ</th>
							<th width='200px'>メールアドレス</th>
							<th width='120px'>ID</th>
							<th width='60px'>状況</th>
						</tr>
					</thead>
						<tbody>
				";
				$i=1;
				 while($rs=mysql_fetch_object($result))
				{
					if ($rs->urgent=='2'){
						  $bg_color='#BE233A';
						  $td_str='緊急';
					}else{
						  $bg_color='#EEF2F4';
						  $td_str='通常';
					} 
					echo "
							<tr align='left' bgcolor='$bg_color'>
								<td width='120px'align='center'>
									<input type='button' class='btn2' value='更新' onclick=\"updateChange("."'up_content".$i."',".$rs->id.",".$page.")\">
									<input type='button' class='btn3' value='削除' onclick=\"var ret=confirm('該当投稿を削除します。よろしいですか？');if(ret)deleteInfo('".$rs->id."',".$page.")\">
								</td>
					";
					echo "
								<td width='150px'align='center'>".$rs->user_nick."</td>
								<td width='200px'align='center'>".date("Y-m-d H:i:s",$rs->insert_time)."</td>
					";
					if ($rs->comments_sum==0){
						  echo "
								<td width='100px'align='center'>$rs->comments_sum</td>
						  ";
					}else{
						  echo "
								<td width='100px'align='center'><a href='javascript:void(0)'onclick='searchBBSComments($rs->id)' >".$rs->comments_sum."</a></td>
						  ";
					}	
					echo "
								<td width='200px'>".$rs->title."</td>
								<td width='550px'><textarea name='up_content".$i."' id='up_content".$i."' style='width:546px;height:200px;font-family: inherit;' >".$rs->content."</textarea></td>
					";
					if ($rs->url!=''){
						  echo "
								<td width='162px'align='center' style='padding-top:5px;'><img src=".$rs->url." width='160px' height='113px'></td>
						  ";
					}else{
						  echo "
								<td width='162px'align='center' style='padding-top:5px;'>&nbsp;</td>
						  ";
					}
					echo "
								<td width='200px'align='center'>".$rs->user_email."</td>
								<td width='120px'align='center'>".$rs->id."</td>	
								<td width='60px'align='center'>".$td_str."</td>								
					";
					echo "
							</tr>
					";
					$i++;
				}
				echo "
					</tbody>
				</table>
				<table width='100%' cellspacing='1' cellpadding='2'>
					<tr bgcolor='#DBE6F5'>
					  <td><span style='float:left; text-align:center'><font color=#666666>$page_string</font></span></td>
					</tr>
				</table>";
				mysql_close($link);
			}else{
				if ($action=='search'){
					echo "<div>検索件数：0件</div>";
				}
			}
		?>
		</form>
	<script src="../js/tableExporter.min.js" type="text/javascript"></script>
	<script language="javascript" type="text/javascript">
		function exportTo() {
			$('#firm_table').tableExport({
				filename: 'export_bbs_info_%DD%-%MM%-%YY%',
				format: 'xls',
				head_delimiter:',',
				column_delimiter:',',
				cols: '2,3,4,5,6,8,9,10'
			});
		}
		function exportCsv(){
			var s_user_nick=document.getElementById("s_user_nick").value;//a
			var s_user_email=document.getElementById("s_user_email").value;//b
			var s_title=document.getElementById("s_title").value;//c
			var s_content=document.getElementById("s_content").value;//d
			var s_comments=document.getElementById("s_comments").value;//e
			var s_sts=document.getElementById("s_sts_checked").value;//f
			var s_YYYY=document.getElementById("s_year_from").value;//g
			var s_MM=document.getElementById("s_month_from").value;//h
			var s_DD=document.getElementById("s_day_from").value;//i
			var s_sort_item=document.getElementById("sort_item_checked").value;//j
			var s_YYYY_TO=document.getElementById("s_year_to").value;//k
			var s_MM_TO=document.getElementById("s_month_to").value;//l
			var s_DD_TO=document.getElementById("s_day_to").value;//m
			var s_sort_type=document.getElementById("sort_type_checked").value;//n

			var pageurl="m_export_csv_bbs.php?action=export&a="+s_user_nick+"&b="+s_user_email+"&c="+s_title+"&d="+s_content+"&e="+s_comments+"&f="+s_sts+"&g="+s_YYYY+"&h="+s_MM+"&i="+s_DD+"&j="+s_sort_item+"&k="+s_YYYY_TO+"&l="+s_MM_TO+"&m="+s_DD_TO+"&n="+s_sort_type;

			window.location.href=pageurl;
		}
		function deleteInfo(c_id,page) {
			  var pageurl="?action=delete&c_id="+c_id+"&page="+page;
			  window.location.href=pageurl;
		}
		function show(msg,id) {
			document.getElementById(id).value=msg;
		}
		function initSearch(){
			document.form1.action='?action=search';
			document.form1.submit();
		}
		function updateChange(up_comment,c_id,page) {
			var comment=document.getElementById(up_comment).value;
			//var pageurl="?action=update&id="+c_id+"&contents="+encodeURIComponent(comment.replace(/\n/g,"；；；"))+"&page="+page;
			var pageurl="?action=update&id="+c_id+"&contents="+encodeURIComponent(comment)+"&page="+page;
			window.location.href=pageurl;
		}
	</script>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>