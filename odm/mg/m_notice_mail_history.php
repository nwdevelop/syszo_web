<?php
header('Cache-control: private, must-revalidate');
	//ユーザー管理⇒会員登録情報
	//include
	require '../util/include.php';
	$home_page_name='シス蔵管理メニュー';
	$home_page_url=URL_PATH;
	$f_page_name='お知らせ管理画面';
	$f_page_url=URL_PATH.'m_notice.php';
	$l_page_name='メール配信画面';
	$l_page_url=URL_PATH.'m_notice_mail.php';
	$page_name='メール送信履歴画面';
	

	$action = $_GET['action'];
	$user_id = $_GET['u'];

	//Search
	if (($action=='search')){

		$link = db_conn();
		mysql_set_charset('utf8');

		$page_size=100;

		if( isset($_GET['page']) ){
		   $page = intval( $_GET['page'] );
		}
		else{
		   $page = 1;
		}
		$rowCnt = 0;

		//All
		$sqlall = "select *from app_mail_history am WHERE 1";

		//ユーザーID
		if($user_id!='') {
			$sqlall .= " and user_id = $user_id";
		}

		$result = mysql_query($sqlall,$link) or die(mysql_error());

		if(!$result){
			$rowCnt = -1;
			db_disConn($result, $link);
		}
		$rowCntall=mysql_num_rows($result);


		$sql = sprintf("%s order by id desc limit %d,%d",$sqlall,($page-1)*$page_size,$page_size);

		$result = mysql_query($sql,$link);

		if(!$result){
			$rowCnt = -1;
			db_disConn($result, $link);
		}

		$rowCnt=mysql_num_rows($result);

		if($rowCnt>$page_size){
			$all_row=$page_size;
		}else{
			$all_row=$rowCnt;
		}

		//paging
		if($rowCnt==0){
			$page_count = 0;
			db_disConn($result, $link);
		}
		else{
			if( $rowCntall<$page_size ){ $page_count = 1; }
			if( $rowCntall%$page_size ){
				$page_count = (int)($rowCntall / $page_size) + 1;
			}else{
				$page_count = $rowCntall / $page_size;
			}
		}
		$page_string = '';
		if (($page == 1)||($page_count == 1)){
		   $page_string .= 'トップページ|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		else{
			$page_string .= '<a href=?action=search&page=1'.
							'&user_id='.$user_id.
							'>トップページ</a>|<a href=?action=search&page='.($page-1).
							'&user_id='.$user_id.
							'>前頁</a>|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		if( ($page == $page_count) || ($page_count == 0) ){
		   $page_string .= '次頁|最終ページ';
		}
		else{
			$page_string .= '<a href=?action=search&page='.($page+1).
							'&user_id='.$user_id.
							'>次頁</a>|<a href=?action=search&page='.$page_count.
							'&user_id='.$user_id.
							'>最終ページ</a>';
		}
	}



?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title><?php echo $page_name; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<meta http-equiv="Expires" CONTENT="0">
<meta http-equiv="Cache-Control" CONTENT="no-cache">
<meta http-equiv="Pragma" CONTENT="no-cache">
<link href="../css/common.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="../js/common.js"></script>
</head>
<body>
<div id="header">
	<div id="header_content">
		<h1><a href="<?php echo $home_page_url; ?>">シス蔵管理画面</a></h1>
	</div>
</div>
<div id="nav">
	<div id="nav_content">
		<a href="<?php echo $home_page_url; ?>"><?php echo $home_page_name.' ＞ '; ?></a>
		<a href="<?php echo $f_page_url; ?>"><?php echo $f_page_name.' ＞ '; ?></a>
		<a href="<?php echo $l_page_url; ?>"><?php echo $l_page_name.' ＞ '; ?></a>
		<?php echo $page_name; ?>
	</div>
</div>
<div class='content'>
	<div style='float:left;margin-top:120px;margin-bottom:20px'>
		<form action='?action=search' method='post' name='form1'>
		<div style='float:left; text-align:left;margin-left:5px;' >
			<input type="button" class="btn_search" value="戻る" onclick="back()" />
		</div>
		<div style='clear:both; margin-bottom:20px'></div>
		<?php
			if ($rowCnt>0){
				echo "
					<table width='100%' cellspacing='1' cellpadding='2'>
						<tr bgcolor='#DBE6F5'>
						  <td><span style='float:left; text-align:center'><font color=#666666>$page_string</font></span></td>
						</tr>
					</table>";
				echo "
					<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
						<tr bgcolor='#DBE6F5'>
							<th width='200px'>宛先</th>
							<th width='200px'>ユーザー名</th>
							<th width='300px'>メールタイトル</th>
							<th width='500px'>メール本文</th>
							<th width='100px'>送信日時</th>
						</tr>
					</table>
				";
				$i=1;
				 while($rs=mysql_fetch_object($result))
				{
				  echo "
					   <table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
							<tr align='left' bgcolor='#EEF2F4'>
								<td width='200px'align='center'>".$rs->user_email."</td>
								<td width='200px'align='center'>".$rs->user_nick."</td>
								<td width='300px'align='center'>".$rs->mail_title."</td>
								<td width='500px'align='center'><textarea style='width:98%;min-width:546px;height:200px;font-family: inherit;' disabled='disabled'>".$rs->mail_contents."</textarea></td>
								<td width='100px'align='center'>".date("Y-m-d H:i:s",$rs->insert_time)."</td>
					";
				  echo"
							</tr>
					  </table>
					";
					$i++;
				}
				echo "
					<table width='100%' cellspacing='1' cellpadding='2'>
						<tr bgcolor='#DBE6F5'>
						  <td><span style='float:left; text-align:center'><font color=#666666>$page_string</font></span></td>
						</tr>
					</table>";
				mysql_close($link);
			}else{
				if ($action=='search'){
					echo "検索結果がありません。";
				}
			}
		?>
		</form>
	<script language="javascript" type="text/javascript">
		function show(msg,id) {
			document.getElementById(id).value=msg;
		}
		function initSearch(){
			document.form1.action='?action=search';
			document.form1.submit();
		}
	</script>
	</div>
</div>
</body>
</html>