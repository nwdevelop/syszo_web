<?php
//ユーザー管理⇒会員登録情報
//include
require '../util/include.php';
header('Cache-control: private, must-revalidate');
$home_page_name='シス蔵管理メニュー';
$home_page_url=URL_PATH;
$f_page_name='シス蔵管理メニュー';
$f_page_url=URL_PATH.'m_mem.php';
$systime=date('Y-m-d H:i:s',time());
$page_name='広告管理';

$action = $_GET['action'];

//Update
if ($action=='update'){
	$ad_id = $_GET['ad_id'];
	$up_ad_name = $_GET['up_ad_name'];
	$up_ad_type = $_GET['up_ad_type'];
	$up_ad_size = $_GET['up_ad_size'];
	$up_link_url = $_GET['up_link_url'];

	$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
	if(!$db){
		die("connot connect:" . mysql_error());
	}
	$dns = mysql_select_db(DB_NAME,$db);
	if(!$dns){
		die("connot use db:" . mysql_error());
	}
	mysql_set_charset('utf8');
	$sql = sprintf("UPDATE app_mg_ad SET ad_name ='%s',ad_type =%d,ad_size ='%s',link_url ='%s' WHERE ad_id= %d ",$up_ad_name,$up_ad_type,$up_ad_size,$up_link_url,$ad_id);

	$result = mysql_query($sql,$db);
	mysql_close($db);
}
//Delete
if ($action=='delete'){
	$ad_id = $_GET['ad_id'];

	$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
	if(!$db){
		die("connot connect:" . mysql_error());
	}

	$dns = mysql_select_db(DB_NAME,$db);

	if(!$dns){
		die("connot use db:" . mysql_error());
	}

	mysql_set_charset('utf8');

	$sql = sprintf("delete from app_mg_ad WHERE ad_id = %d",$ad_id);
	$result = mysql_query($sql,$db);

	mysql_close($db);

}
//Search
if (($action=='search')||$action=='update'||($action=='delete')){

	$link = db_conn();
	mysql_set_charset('utf8');

	$page_size=100;

	if( isset($_GET['page']) ){
		$page = intval( $_GET['page'] );
	}
	else{
		$page = 1;
	}
	$rowCnt = 0;
	//広告ID:
	$s_ad_id = $_POST['s_ad_id'];
	if($_GET['s_ad_id']!='') {
		$s_ad_id=$_GET['s_ad_id'];
	}
	//広告名:
	$s_ad_name = $_POST['s_ad_name'];
	if($_GET['s_ad_name']!='') {
		$s_ad_name=$_GET['s_ad_name'];
	}

	//All
	$sqlall = "select * from app_mg_ad WHERE 1";

	//広告名
	if($s_ad_name!='') {
		$sqlall .= " and ad_name like '%$s_ad_name%'";
	}
	//広告ID
	if($s_ad_id!='') {
		$sqlall .= " and ad_id = $s_ad_id";
	}

	$result = mysql_query($sqlall,$link) or die(mysql_error());

	if(!$result){
		$rowCnt = -1;
		db_disConn($result, $link);
	}
	$rowCntall=mysql_num_rows($result);

	$sql = sprintf("%s order by ad_id limit %d,%d",$sqlall,($page-1)*$page_size,$page_size);

	$result = mysql_query($sql,$link);

	if(!$result){
		$rowCnt = -1;
		db_disConn($result, $link);
	}

	$rowCnt=mysql_num_rows($result);

	//paging
	if($rowCnt==0){
		$page_count = 0;
		db_disConn($result, $link);
	}
	else{
		if( $rowCntall<$page_size ){ $page_count = 1; }
		if( $rowCntall%$page_size ){
			$page_count = (int)($rowCntall / $page_size) + 1;
		}else{
			$page_count = $rowCntall / $page_size;
		}
	}
	$page_string = '';
	if (($page == 1)||($page_count == 1)){
		$page_string .= 'トップページ|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁(<b>'.$rowCntall.'</b>件)|';
	}
	else{
		$page_string .= '<a href=?action=search&page=1'.
				'&s_ad_id='.$s_ad_id.
				'&s_ad_name='.$s_ad_name.
				'>トップページ</a>|<a href=?action=search&page='.($page-1).
				'&s_ad_id='.$s_ad_id.
				'&s_ad_name='.$s_ad_name.
				'>前頁</a>|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁(<b>'.$rowCntall.'</b>件)|';
	}
	if( ($page == $page_count) || ($page_count == 0) ){
		$page_string .= '次頁|最終ページ';
	}
	else{
		$page_string .= '<a href=?action=search&page='.($page+1).
				'&s_ad_id='.$s_ad_id.
				'&s_ad_name='.$s_ad_name.
				'>次頁</a>|<a href=?action=search&page='.$page_count.
				'&s_ad_id='.$s_ad_id.
				'&s_ad_name='.$s_ad_name.
				'>最終ページ</a>';
	}
}
?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
	<title><?php echo $page_name; ?></title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" >
	<meta http-equiv="content-style-type" content="text/css">
	<meta http-equiv="content-script-type" content="text/javascript">
	<link href="../css/common.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="../js/common.js"></script>
	<script src="../js/jquery.js" type="text/javascript"></script>
	<script src="../js/jquery.blockUI.js" type="text/javascript"></script>
</head>
<body>
<div id="header">
	<div id="header_content">
		<h1><a href="<?php echo $home_page_url; ?>">広告管理</a></h1>
	</div>
</div>
<div id="nav">
	<div id="nav_content">
		<a href="<?php echo $home_page_url; ?>"><?php echo $home_page_name.' ＞ '; ?></a>
		<?php echo $page_name; ?>
	</div>
</div>
<div class='content'>
	<div style='float:left;margin-top:120px;margin-bottom:20px'>
		<form action='?action=search' method='post' name='form1'>
			<div style='float:left; text-align:left;width:180px;height:20px;' >
				広告ID:
			</div>
			<div style='float:left; text-align:left; width:296px;height:20px;' >
				<input type='text' name='s_ad_id' id='s_ad_id' style='width:296px;height:20px;' value='<?php echo $s_ad_id;?>'/>
			</div>
			<div style='clear:both;'></div><br/>
			<div style='float:left; text-align:left; width:180px;height:20px;' >
				広告名:
			</div>
			<div style='float:left; text-align:left; width:296px;height:20px;' >
				<input type='text' name='s_ad_name' id='s_ad_name' style='width:296px;height:20px;' value='<?php echo $s_ad_name;?>'/>
			</div>
			<div style='clear:both;'></div><br/>
			<div style='float:left; text-align:left;' >
				<input type="submit" class="btn_search" value="検索" />
			</div>
			<div style='float:left; text-align:left;margin-left:40px;' >
				<input type="button" class="btn_search" value="新規作成" onclick="addAd()" href="javascript:void(0)""/>
			</div>
			<div style='clear:both; margin-bottom:20px'></div>
			<?php
			if ($rowCnt>0){
				echo "<div>検索件数：$rowCntall 件</div><input type='button' class='btn_search' value='CSV出力' style='width:120px;' onclick='exportTo()' href='javascript:void(0)'/>";
				echo "
					<table width='100%' cellspacing='1' cellpadding='2'>
						<tr bgcolor='#DBE6F5'>
						  <td><span style='float:left; text-align:center'><font color=#666666>$page_string</font></span></td>
						</tr>
					</table>";
				echo "
					<table id='firm_table' width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
					<thead>
						<tr bgcolor='#DBE6F5'>
							<th width='60px'>操作</th>
							<th width='100px'>広告ID</th>
							<th width='255px'>広告名</th>
							<th width='100px'>広告タイプ</th>
							<th width='320px'>広告内容</th>
							<th width='255px'>広告サイズ</th>
							<th width='255px'>リンク先</th>
						</tr>
					</thead>
						<tbody>
				";
				$i=1;
				while($rs=mysql_fetch_object($result))
				{
					if($rs->ad_img){
						$image=$rs->ad_img;
						$image_url0=substr($image,0,1);
						$image_url1=substr($image,1,1);
						$image_url2=substr($image,2,1);
						$image_url3=substr($image,3);
						$image_url4=$image_url0."/".$image_url1."/".$image_url2."/".$image_url3;
						$image_url_info=IMG_URL_PATH."/ad/".$image_url4;
						$img_show_url=$image_url_info;
					}else{
						$img_show_url = '';
					}
					echo "
							<tr align='left' bgcolor='#EEF2F4'>
								<td width='60px'align='center'>
									<input type='button' class='btn2' value='更新' onclick=\"updateChange("."'up_ad_name".$i."',"."'up_ad_type".$i."',"."'up_ad_size".$i."',"."'up_link_url".$i."',".$rs->ad_id.",".$page.")\" >
									<input type='button' class='btn3' value='削除' onclick=\"var ret=confirm('該当広告を削除します。よろしいですか？');if(ret)deleteInfo('".$rs->ad_id."',".$page.")\">
								</td>
					";
					echo "
							<td width='100px'align='center'>".$rs->ad_id."</td>
							<td width='255px'><input style='width:98%;' type='text' name='up_ad_name".$i."' id='up_ad_name".$i."' value=".$rs->ad_name."></td>
					";
					if($rs->ad_type=='0'){
						echo "<td width='100px'align='center'>
									<select name='up_ad_type".$i."'id='up_ad_type".$i."'>
										<option value='0' selected>0:Web</option>
										<option value='1'>1:アプリ</option>
									</select>
								</td>";
						echo "<td width='320px'><img src='$img_show_url' style='width:300px;height:250px;'></td>";
					}else{
						echo "<td width='100px'align='center'>
									<select name='up_ad_type".$i."'id='up_ad_type".$i."'>
										<option value='0'>0:Web</option>
										<option value='1' selected>1:アプリ</option>
									</select></td>";
						echo "<td width='320px'><img src='$img_show_url' style='width:320px;height:50px;'></td>";
					}
					echo"
							<td width='255px'><input style='width:98%;' type='text' name='up_ad_size".$i."' id='up_ad_size".$i."' value=".$rs->ad_size."></td>
							<td width='255px'><input style='width:98%;' type='text' name='up_link_url".$i."' id='up_link_url".$i."' value=".$rs->link_url."></td>
				";
					echo"
							</tr>
					";
					$i++;
				}
				echo "
					</tbody>
					</table>
					<table width='100%' cellspacing='1' cellpadding='2'>
						<tr bgcolor='#DBE6F5'>
						  <td><span style='float:left; text-align:center'><font color=#666666>$page_string</font></span></td>
						</tr>
					</table>";
				mysql_close($link);
			}else{
				if ($action=='search'){
					echo "<div>検索件数：0件</div>";
				}
			}
			?>
		</form>
		<script src="../js/tableExporter.min.js" type="text/javascript"></script>
		<script language="javascript" type="text/javascript">
			function exportTo() {
				$('#firm_table').tableExport({
					filename: 'export_ad_info_%DD%-%MM%-%YY%',
					format: 'csv',
					head_delimiter:',',
					column_delimiter:',',
					cols: '2,3,4,6,7'
				});
			}
			function deleteInfo(ad_id,page) {
				var pageurl="?action=delete&ad_id="+ad_id+"&page="+page;
				window.location.href=pageurl;
			}
			function show(msg,id) {
				document.getElementById(id).value=msg;
			}
			function initSearch(){
				document.form1.action='?action=search';
				document.form1.submit();
			}
			function updateChange(up_ad_name,up_ad_type,up_ad_size,up_link_url,ad_id,page) {
				var up_ad_name=document.getElementById(up_ad_name).value;
				var up_ad_type=document.getElementById(up_ad_type).value;
				var up_ad_size=document.getElementById(up_ad_size).value;
				var up_link_url=document.getElementById(up_link_url).value;
				var pageurl="?action=update&ad_id="+ad_id+"&up_ad_name="+encodeURIComponent(up_ad_name)+"&up_ad_type="+encodeURIComponent(up_ad_type)+"&up_ad_size="+encodeURIComponent(up_ad_size)+"&up_link_url="+encodeURIComponent(up_link_url)+"&page="+page;
				window.location.href=pageurl;
			}
		</script>
	</div>
	<div id="addAdForm" style="text-align: center; display: none;">
		<form onsubmit="return false;" style="margin-bottom:0px;" method="post" action="add_ad.php" name="addAdForm">
			<table border="0" style="font-size:12px; text-align:center; margin:30px auto;">
				<tbody>
				<tr>
					<td colspan="3" class="pirobox_up"><div class="piro_close" style="visibility: visible; display: block;"onclick="jQuery.unblockUI();initSearch();"></div></td>
				</tr>
				<tr>
					<td align="left" colspan="2">
						<input type="button" style="color:white;font-size:16px; line-height:16px;background-color:orange;width:100px;height:30px;font-weight: bold;"onclick="var ret=confirm('広告情報を追加します。よろしいですか？');if(ret)addAdInfoSubmit()" value="確認"/>
					</td>
					<div style='clear:both;'></div>
				</tr>
				<tr>
					<td align="left" colspan="2">
						<div style="color:#FC8B05; font-weight:bold;font-size:16px;width:150px;">*広告名：</div>
						<input id="add_ad_name" type="text" class="nomaltext" name="add_ad_name" value=""/>
						<div style='clear:both;'></div>
					</td>
				</tr>
				<tr>
					<td align="left" colspan="2">
						<div style="color:#FC8B05; font-weight:bold;font-size:16px;width:150px;">*広告タイプ：</div>
						<input id="radio-m" type="radio" name="add_ad_type" value="0"><label for="radio-m">Web</label>
						<input id="radio-f" type="radio" name="add_ad_type" value="1"><label for="radio-f">アプリ</label>
						<div style='clear:both;'></div>
					</td>
				</tr>
				<tr>
					<td align="left" colspan="2">
						<div style="color:#FC8B05; font-weight:bold;font-size:16px;width:150px;">広告サイズ：</div>
						<input id="add_ad_size" type="text" class="nomaltext" name="add_ad_size" value=""/>
						<div style='clear:both;'></div>
					</td>
				</tr>
				<tr>
					<td align="left" colspan="2">
						<div style="color:#FC8B05; font-weight:bold;font-size:16px;width:150px;">*広告リンク先：</div>
						<input id="add_ad_url" type="text" class="nomaltext" name="add_ad_url" value=""/>
						<div style='clear:both;'></div>
					</td>
				</tr>
				<tr>
					<td align="left" colspan="2">
						<div style="color:#FC8B05; font-weight:bold;font-size:16px;width:100%;">*広告イメージ(2MBまでのJPEG,PNG,GIF画像)：</div>
						<input type="file" onchange="img_upload('ad_img','add_ad_img_url','ad_img_show')" id="ad_img" name="file" accept="image/*" />
						<div style='clear:both;'></div><br/>
						<img id="ad_img_show" src=""style='max-width:300px;max-height:250px;'/>
						<input type="hidden" id="add_ad_img_url" value="" />
						<div style='clear:both;'></div>
					</td>
				</tr>
				<tr>
					<td align="left" colspan="2">
						<input type="button" style="color:white;font-size:16px; line-height:16px;background-color:orange;width:100px;height:30px;font-weight: bold;"onclick="var ret=confirm('広告情報を追加します。よろしいですか？');if(ret)addAdInfoSubmit()" value="確認"/>
					</td>
				</tr>
				</tbody>
			</table>
		</form>
	</div>
	<div class="clearboth"></div>
</div>
<!--<script src="../js/upload/jquery-2.2.1.min.js"></script>-->
<script src="../js/upload/ajaxfileupload.js"></script>
<script>
	function img_upload(id,idd,show_id){
		document.domain = 't.syszo.com';
		$.ajaxFileUpload( {
			url : 'https://t.syszo.com/api-t/index.php/upload_image_web',
			secureuri : true,
			fileElementId : id,
			data : {name:id},
			dataType: "jsonp",
			jsonp: "callback",
			jsonpCallback:"flightHandler",
			success : function(data, status)
			{
				var jsonobj=eval('('+data+')');
				$("#"+ idd).val(jsonobj.data.hurl);
				document.getElementById(show_id).src = (jsonobj.data.url);
			},
			error : function(data, status, e)
			{
				var msg = jsonobj.msg;
				if(typeof msg =='string'){
					alert(msg);
				}
			}
		});
	}
</script>
</body>
</html>