<?php
	//include
	require '../util/include.php';

	$category_name=$_POST['add_category_name'];

	$existChk = searchCategoryInfo($category_name);

	if ($existChk=='1'){
		echo '該当カテゴリ情報が既に存在しました。';
	}else{
		$ret = addCategoryInfo($category_name);
		if($ret!='0'){
			echo 'システムエラー、登録を失敗しました。';
		}else{
			echo 'カテゴリ情報を登録しました。';
		}
	}
?>