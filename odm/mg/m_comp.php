<?php
//ユーザー管理⇒会員登録情報
//include
require '../util/include.php';
header('Cache-control: private, must-revalidate');
$home_page_name='シス蔵管理メニュー';
$home_page_url=URL_PATH;
$f_page_name='シス蔵管理メニュー';
$f_page_url=URL_PATH.'m_mem.php';
$page_name='企業管理画面';

$action = $_GET['action'];

//Update
if ($action=='update'){
	$comp_id = $_GET['id'];
	$user_id = $_GET['user_id'];
	$up_comp_name = $_GET['up_comp_name'];
	$up_dep_name = $_GET['up_dep_name'];
	$up_mail = $_GET['up_mail'];
	$up_man_name = $_GET['up_man_name'];
	$up_addr = $_GET['up_addr'];
	$up_tel = $_GET['up_tel'];
	$up_memo = urldecode($_GET['up_memo']);

	$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
	if(!$db){
		die("connot connect:" . mysql_error());
	}
	$dns = mysql_select_db(DB_NAME,$db);
	if(!$dns){
		die("connot use db:" . mysql_error());
	}
	mysql_set_charset('utf8');
	$sql = sprintf("UPDATE app_mg_comp SET comp_name ='%s',dep_name ='%s',mail ='%s',man_name ='%s',addr ='%s',tel ='%s',memo ='%s',update_time='%d' WHERE comp_id = %d",$up_comp_name,$up_dep_name,$up_mail,$up_man_name,$up_addr,$up_tel,$up_memo,time(),$comp_id);
	$sql2 = sprintf("UPDATE app_member SET user_email ='%s' WHERE user_id = %d",$up_mail,$user_id);
	$result = mysql_query($sql,$db);
	$result2 = mysql_query($sql2,$db);
	mysql_close($db);
}
//Delete
if ($action=='delete'){
	$comp_id = $_GET['comp_id'];
	$user_id = $_GET['user_id'];

	$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
	if(!$db){
		die("connot connect:" . mysql_error());
	}

	$dns = mysql_select_db(DB_NAME,$db);

	if(!$dns){
		die("connot use db:" . mysql_error());
	}

	mysql_set_charset('utf8');

	$sql = sprintf("delete from app_mg_comp WHERE comp_id = %d",$comp_id);
	$result = mysql_query($sql,$db);
	mysql_close($db);

}
//Search
if (($action=='search')||($action=='update')||($action=='delete')){

	$link = db_conn();
	mysql_set_charset('utf8');

	$page_size=100;

	if( isset($_GET['page']) ){
		$page = intval( $_GET['page'] );
	}
	else{
		$page = 1;
	}
	$rowCnt = 0;
	//会社名・部署名:
	$comp_name = $_POST['comp_name'];
	if($_GET['comp_name']!='') {
		$comp_name=$_GET['comp_name'];
	}
	//メールアドレス:
	$mail = $_POST['mail'];
	if($_GET['mail']!='') {
		$mail=$_GET['mail'];
	}
	//担当者
	$man_name = $_POST['man_name'];
	if($_GET['man_name']!='') {
		$man_name=$_GET['man_name'];
	}
	//Tel
	$tel = $_POST['tel'];
	if($_GET['tel']!='') {
		$tel=$_GET['tel'];
	}

	//All
	$sqlall = "select * from app_mg_comp WHERE 1";

	//ニックネーム
	if($comp_name!='') {
		$sqlall .= " and (comp_name like '%$comp_name%' or dep_name like '%$dep_name%')";
	}
	//メールアドレス
	if($mail!='') {
		$sqlall .= " and mail like '%$mail%'";
	}
	//担当者
	if($man_name!='') {
		$sqlall .= " and man_name like '%$man_name%'";
	}
	//tel
	if($tel!='') {
		$sqlall .= " and tel like '%$tel%'";
	}

	$result = mysql_query($sqlall,$link) or die(mysql_error());

	if(!$result){
		$rowCnt = -1;
		db_disConn($result, $link);
	}
	$rowCntall=mysql_num_rows($result);

	$sql = sprintf("%s order by comp_id limit %d,%d",$sqlall,($page-1)*$page_size,$page_size);

	$result = mysql_query($sql,$link);

	if(!$result){
		$rowCnt = -1;
		db_disConn($result, $link);
	}

	$rowCnt=mysql_num_rows($result);

	//paging
	if($rowCnt==0){
		$page_count = 0;
		db_disConn($result, $link);
	}
	else{
		if( $rowCntall<$page_size ){ $page_count = 1; }
		if( $rowCntall%$page_size ){
			$page_count = (int)($rowCntall / $page_size) + 1;
		}else{
			$page_count = $rowCntall / $page_size;
		}
	}
	$page_string = '';
	if (($page == 1)||($page_count == 1)){
		$page_string .= 'トップページ|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁(<b>'.$rowCntall.'</b>件)|';
	}
	else{
		$page_string .= '<a href=?action=search&page=1'.
				'&comp_name='.$comp_name.
				'&mail='.$mail.
				'&man_name='.$man_name.
				'&tel='.$tel.
				'>トップページ</a>|<a href=?action=search&page='.($page-1).
				'&comp_name='.$comp_name.
				'&mail='.$mail.
				'&man_name='.$man_name.
				'&tel='.$tel.
				'>前頁</a>|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁(<b>'.$rowCntall.'</b>件)|';
	}
	if( ($page == $page_count) || ($page_count == 0) ){
		$page_string .= '次頁|最終ページ';
	}
	else{
		$page_string .= '<a href=?action=search&page='.($page+1).
				'&comp_name='.$comp_name.
				'&mail='.$mail.
				'&man_name='.$man_name.
				'&tel='.$tel.
				'>次頁</a>|<a href=?action=search&page='.$page_count.
				'&comp_name='.$comp_name.
				'&mail='.$mail.
				'&man_name='.$man_name.
				'&tel='.$tel.
				'>最終ページ</a>';
	}
}



?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
	<title><?php echo $page_name; ?></title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" >
	<meta http-equiv="content-style-type" content="text/css">
	<meta http-equiv="content-script-type" content="text/javascript">
	<link href="../css/common.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="../js/common.js"></script>
	<script charset="utf-8" src="../js/jquery.js" type="text/javascript"></script>
	<script src="../js/jquery.blockUI.js" type="text/javascript"></script>
</head>
<body>
<div id="header">
	<div id="header_content">
		<h1><a href="<?php echo $home_page_url; ?>">企業管理画面</a></h1>
	</div>
</div>
<div id="nav">
	<div id="nav_content">
		<a href="<?php echo $home_page_url; ?>"><?php echo $home_page_name.' ＞ '; ?></a>
		<?php echo $page_name; ?>
	</div>
</div>
<div class='content'>
	<div style='float:left;margin-top:120px;margin-bottom:20px'>
		<form action='?action=search' method='post' name='form1'>
			<div style='float:left; text-align:left;width:180px;height:20px;' >
				会社名・部署名:
			</div>
			<div style='float:left; text-align:left; width:296px;height:20px;' >
				<input type='text' name='comp_name' id='comp_name' style='width:296px;height:20px;' value='<?php echo $comp_name;?>'/>
			</div>
			<div style='clear:both;'></div><br/>
			<div style='float:left; text-align:left; width:180px;height:20px;' >
				メールアドレス:
			</div>
			<div style='float:left; text-align:left; width:296px;height:20px;' >
				<input type='text' name='mail' id='mail' style='width:296px;height:20px;' value='<?php echo $mail;?>'/>
			</div>
			<div style='clear:both;'></div><br/>
			<div style='float:left; text-align:left; width:180px;height:20px;' >
				担当者:
			</div>
			<div style='float:left; text-align:left; width:296px;height:25px;' >
				<input type='text' name='man_name' id='man_name' style='width:296px;height:20px;' value='<?php echo $man_name;?>'/>
			</div>
			<div style='clear:both;'></div><br/>
			<div style='float:left; text-align:left; width:180px;height:20px;' >
				Tel:
			</div>
			<div style='float:left; text-align:left; width:296px;height:25px;' >
				<input type='text' name='tel' id='tel' style='width:296px;height:20px;' value='<?php echo $tel;?>'/>
			</div>
			<div style='clear:both;'></div><br/>
			<div style='float:left; text-align:left;' >
				<input type="submit" class="btn_search" value="検索" />
			</div>
			<div style='float:left; text-align:left;margin-left:40px;' >
				<input type="button" class="btn_search" value="新規作成" onclick="addComp()" href="javascript:void(0)""/>
			</div>
			<div style='clear:both; margin-bottom:20px'></div>
			<?php
			if ($rowCnt>0){
				echo "<div>検索件数：$rowCntall 件</div><input type='button' class='btn_search' value='CSV出力' style='width:120px;' onclick='exportCsv()' href='javascript:void(0)'/>";
				echo "
					<table width='100%' cellspacing='1' cellpadding='2'>
						<tr bgcolor='#DBE6F5'>
						  <td><span style='float:left; text-align:center'><font color=#666666>$page_string</font></span></td>
						</tr>
					</table>";
				echo "
					<table id='firm_table' width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
					<thead>
						<tr bgcolor='#DBE6F5'>
							<th width='120px'>操作</th>
							<th width='120px'>公式アカウント</th>
							<th width='100px'>会社ID</th>
							<th width='100px'>ユーザーID</th>
							<th width='255px'>会社名</th>
							<th width='255px'>部署名</th>
							<th width='255px'>メールアドレス</th>
							<th width='255px'>担当者</th>
							<th width='255px'>アドレス</th>
							<th width='120px'>Tel</th>
							<th width='255px'>備考</th>
							<th width='200px'>作成時間</th>
							<th width='200px'>更新時間</th>
						</tr>
					</thead>
						<tbody>
				";
				$i=1;
				while($rs=mysql_fetch_object($result))
				{
					echo "
							<tr align='left' bgcolor='#EEF2F4'>
								<td width='120px'align='center'>
									<input type='button' class='btn2' value='更新' onclick=\"updateChange("."'up_comp_name".$i."',"."'up_dep_name".$i."',"."'up_mail".$i."',"."'up_man_name".$i."',"."'up_addr".$i."',"."'up_tel".$i."',"."'up_memo".$i."',".$rs->comp_id.",".$rs->user_id.",".$page.")\" >
									<input type='button' class='btn3' value='削除' onclick=\"var ret=confirm('該当会社を削除します。よろしいですか？');if(ret)deleteInfo('".$rs->comp_id."','".$rs->user_id."',".$page.")\">
								</td>
					";
					echo "
							<td width='120px'align='center'><input type='button' class='btn3' value='一覧へ' onclick=\"comp_course_list('".$rs->comp_id."')\" ></td>
							<td width='100px'align='center'>".$rs->comp_id."</td>
							<td width='100px'align='center'>".$rs->user_id."</td>
							<td width='255px'><input type='text' name='up_comp_name".$i."' id='up_comp_name".$i."' value=".$rs->comp_name."></td>
							<td width='255px'><input type='text' name='up_dep_name".$i."' id='up_dep_name".$i."' value=".$rs->dep_name."></td>
							<td width='255px'><input type='text' name='up_mail".$i."' id='up_mail".$i."' value=".$rs->mail."></td>
							<td width='255px'><input type='text' name='up_man_name".$i."' id='up_man_name".$i."' value=".$rs->man_name."></td>
							<td width='255px'><input type='text' name='up_addr".$i."' id='up_addr".$i."' value=".$rs->addr."></td>
							<td width='120px'><input type='text' style='width:110px;'name='up_tel".$i."' id='up_tel".$i."' value=".$rs->tel."></td>
							<td width='255px'><textarea name='up_memo".$i."' id='up_memo".$i."' style='width:255px;height:100px;font-family: inherit;' >".$rs->memo."</textarea></td>
				";
					echo "
							<td width='200px'align='center'>".date("Y-m-d H:i:s",$rs->insert_time)."</td>
							<td width='200px'align='center'>".date("Y-m-d H:i:s",$rs->update_time)."</td>
				";

					echo"
							</tr>
					";
					$i++;
				}
				echo "
					</tbody>
					</table>
					<table width='100%' cellspacing='1' cellpadding='2'>
						<tr bgcolor='#DBE6F5'>
						  <td><span style='float:left; text-align:center'><font color=#666666>$page_string</font></span></td>
						</tr>
					</table>";
				mysql_close($link);
			}else{
				if ($action=='search'){
					echo "<div>検索件数：0件</div>";
				}
			}
			?>
		</form>
		<script src="../js/tableExporter.min.js" type="text/javascript"></script>
		<script language="javascript" type="text/javascript">
			function exportTo() {
				$('#firm_table').tableExport({
					filename: 'export_comp_info_%DD%-%MM%-%YY%',
					format: 'csv',
					head_delimiter:',',
					column_delimiter:',',
					cols: '2,3,4,5,6,7,8,9,10,11,12'
				});
			}
			function exportCsv(){
				var s_user_nick=document.getElementById("s_user_nick").value;//a
				var s_user_email=document.getElementById("s_user_email").value;//b
				var s_title=document.getElementById("s_title").value;//c
				var s_content=document.getElementById("s_content").value;//d
				var s_comments=document.getElementById("s_comments").value;//e
				var s_sts=document.getElementById("s_sts").value;//f
				var s_YYYY=document.getElementById("YYYY").value;//g
				var s_MM=document.getElementById("MM").value;//h
				var s_DD=document.getElementById("DD").value;//i
				var s_sort_item=document.getElementById("sort_item").value;//j
				var s_YYYY_TO=document.getElementById("YYYY_TO").value;//k
				var s_MM_TO=document.getElementById("MM_TO").value;//l
				var s_DD_TO=document.getElementById("DD_TO").value;//m
				var s_sort_type=document.getElementById("sort_type").value;//n

				var pageurl="m_export_csv_bbs.php?action=export&a="+s_user_nick+"&b="+s_user_email+"&c="+s_title+"&d="+s_content+"&e="+s_comments+"&f="+s_sts+"&g="+s_YYYY+"&h="+s_MM+"&i="+s_DD+"&j="+s_sort_item+"&k="+s_YYYY_TO+"&l="+s_MM_TO+"&m="+s_DD_TO+"&n="+s_sort_type;

				window.location.href=pageurl;
			}
			function deleteInfo(comp_id,user_id,page) {
				var pageurl="?action=delete&comp_id="+comp_id+"&user_id="+user_id+"&page="+page;
				window.location.href=pageurl;
			}
			function updateChange(up_comp_name,up_dep_name,up_mail,up_man_name,up_addr,up_tel,up_memo,comp_id,user_id,page) {
				var up_comp_name=document.getElementById(up_comp_name).value;
				var up_dep_name=document.getElementById(up_dep_name).value;
				var up_mail=document.getElementById(up_mail).value;
				var up_man_name=document.getElementById(up_man_name).value;
				var up_addr=document.getElementById(up_addr).value;
				var up_tel=document.getElementById(up_tel).value;
				var up_memo=document.getElementById(up_memo).value;
				var pageurl="?action=update&id="+comp_id+"&up_comp_name="+encodeURIComponent(up_comp_name)+"&up_dep_name="+encodeURIComponent(up_dep_name)+"&up_mail="+encodeURIComponent(up_mail)+"&up_man_name="+encodeURIComponent(up_man_name)+"&up_addr="+encodeURIComponent(up_addr)+"&up_tel="+encodeURIComponent(up_tel)+"&up_memo="+encodeURIComponent(up_memo)+"&page="+page+"&user_id="+user_id;
				window.location.href=pageurl;
			}
			function show(msg,id) {
				document.getElementById(id).value=msg;
			}
			function initSearch(){
				document.form1.action='?action=search';
				document.form1.submit();
			}
			function comp_course_list(comp_id){
				var pageurl="m_comp_course.php?action=search&get_comp_id="+comp_id;
				window.location.href=pageurl;
			}
		</script>
	</div>
	<div id="addCompForm" style="text-align: center; display: none;">
		<form onsubmit="return false;" style="margin-bottom:0px;" method="post" action="add_comp.php" name="addCompForm">
			<table border="0" style="font-size:12px; text-align:center; margin:30px auto;">
				<tbody>
				<tr>
					<td colspan="3" class="pirobox_up"><div class="piro_close" style="visibility: visible; display: block;"onclick="jQuery.unblockUI();initSearch();"></div></td>
				</tr>
				<tr>
					<td align="left" colspan="2">
						<div style="font-size:20px; text-align:left; font-weight:bold; color:#900;">
							会社新規作成
						</div>
						<div style='clear:both;'></div><br/>
					</td>
				</tr>
				<tr>
					<td align="left" colspan="2">
						<div style="color:#FC8B05; font-weight:bold;font-size:16px;width:150px;">*会社名：</div>
						<input id="add_comp_name" type="text" class="nomaltext" name="add_comp_name" value=""/>
						<div style='clear:both;'></div><br/>
					</td>
				</tr>
				<tr>
					<td align="left" colspan="2">
						<div style="color:#FC8B05; font-weight:bold;font-size:16px;width:150px;">*部署名：</div>
						<input id="add_dep_name" type="text" class="nomaltext" name="add_dep_name" value=""/>
						<div style="color:#FC8B05; font-weight:bold;font-size:16px;"><input id="add_dep_flg" type="checkbox" name="add_dep_flg" />サブネームを使用する</div>
						<div style='clear:both;'></div><br/>
					</td>
				</tr>
				<tr>
					<td align="left" colspan="2">
						<div style="color:#FC8B05; font-weight:bold;font-size:16px;width:150px;">*メールアドレス：</div>
						<input id="add_mail" type="text" class="nomaltext" name="add_mail" value=""/>
						<div style='clear:both;'></div><br/>
					</td>
				</tr>
				<tr>
					<td align="left" colspan="2">
						<div style="color:#FC8B05; font-weight:bold;font-size:16px;width:150px;">*パスワード：</div>
						<input id="add_pwd" type="password" class="nomaltext" name="add_pwd" value=""/>
						<div style='clear:both;'></div><br/>
					</td>
				</tr>
				<tr>
					<td align="left" colspan="2">
						<div style="color:#FC8B05; font-weight:bold;font-size:16px;width:150px;">担当者：</div>
						<input id="add_man_name" type="text" class="nomaltext" name="add_man_name" value=""/>
						<div style='clear:both;'></div><br/>
					</td>
				</tr>
				<tr>
					<td align="left" colspan="2">
						<div style="color:#FC8B05; font-weight:bold;font-size:16px;width:150px;">Tel：</div>
						<input id="add_tel" type="text" class="nomaltext" name="add_tel" value=""/>
						<div style='clear:both;'></div><br/>
					</td>
				</tr>
				<tr>
					<td align="center" colspan="2">
						<input type="button" style="color:white;font-size:16px; line-height:16px;background-color:orange;width:100px;height:30px;font-weight: bold;"onclick="var ret=confirm('会社情報を追加します。よろしいですか？');if(ret)addCompInfoSubmit()" value="確認"/>
					</td>
				</tr>
				</tbody>
			</table>
		</form>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>