//倒産情報追加
function addInfo(){
  jQuery.blockUI({ message: $('#addInfoForm'),css:{
      padding:        0,
      margin:         0,
      width:          '50%',
      top:            '10%',
      bottom:         'auto',
      left:           '20%',
      textAlign:      'center',
      color:          '#000',
      backgroundColor:'#fff',
      cursor:         'auto'
    } } );
}
//倒産情報追加
function addInfoSubmit(){
  var seili_yyyy = document.getElementById('add_seili_yyyy').value;
  var seili_mm = document.getElementById('add_seili_mm').value;
  var company_name = document.getElementById('add_company_name').value;

  if (company_name!=''&&seili_yyyy!=''&&seili_mm!='') {
    $.post('add_info.php', { company_name: company_name,seili_yyyy: seili_yyyy,seili_mm: seili_mm }, function(data, status){
        alert(data);
      });
  } else {
        alert('整理年月、または倒産企業名を入力してください。');
  }
}
//企業情報追加
function addCompany(){
  jQuery.blockUI({ message: $('#addCompanyForm'),css:{
      padding:        0,
      margin:         0,
      width:          '50%',
      top:            '10%',
      bottom:         'auto',
      left:           '20%',
      textAlign:      'center',
      color:          '#000',
      backgroundColor:'#fff',
      cursor:         'auto'
    } } );
}
//企業情報追加
function addCompanySubmit(){
  var add_city_id = document.getElementById('add_city_id').value;
  var add_company_name = document.getElementById('add_company_name').value;

  if (add_city_id!=''&&add_company_name!='') {
    $.post('add_company_info.php', { add_city_id: add_city_id,add_company_name: add_company_name}, function(data, status){
        alert(data);
      });
  } else {
        alert('地域名、または企業名を入力してください。');
  }
}
//現場情報追加
function addGenInfo(){
  jQuery.blockUI({ message: $('#addGenInfoForm'),css:{
      padding:        0,
      margin:         0,
      width:          '50%',
      top:            '10%',
      bottom:         'auto',
      left:           '20%',
      textAlign:      'center',
      color:          '#000',
      backgroundColor:'#fff',
      cursor:         'auto'
    } } );
}
//現場情報追加
function addGenInfoSubmit(){
  var add_city_id = document.getElementById('add_city_id').value;
  var add_company_name = document.getElementById('add_company_name').value;
  var add_gen_name = document.getElementById('add_gen_name').value;

  if (add_city_id!=''&&add_gen_name!='') {
    $.post('add_gen_info.php', { add_city_id: add_city_id,add_company_name: add_company_name,add_gen_name: add_gen_name }, function(data, status){
        alert(data);
      });
  } else {
        alert('地域名、または現場名を必ず入力してください。');
  }
}

//俺のシリーズ分類情報追加
function addseries(){
  jQuery.blockUI({ message: $('#addseriesForm'),css:{
      padding:        0,
      margin:         0,
      width:          '50%',
      top:            '10%',
      bottom:         'auto',
      left:           '20%',
      textAlign:      'center',
      color:          '#000',
      backgroundColor:'#fff',
      cursor:         'auto'
    } } );
}
//俺のシリーズ分類情報追加
function addseriesSubmit(){
  var add_series_name = document.getElementById('add_series_name').value;

  if (add_series_name!='') {
    $.post('add_series_info.php', { add_series_name: add_series_name}, function(data, status){
        alert(data);
      });
  } else {
        alert('分類名を入力してください。');
  }
}
//カテゴリ情報追加
function addCategory(){
  jQuery.blockUI({ message: $('#addCategoryForm'),css:{
      padding:        0,
      margin:         0,
      width:          '50%',
      top:            '10%',
      bottom:         'auto',
      left:           '20%',
      textAlign:      'center',
      color:          '#000',
      backgroundColor:'#fff',
      cursor:         'auto'
    } } );
}
//カテゴリ情報追加
function addCategorySubmit(){
  var add_category_name = document.getElementById('add_category_name').value;

  if (add_category_name!='') {
    $.post('add_category_info.php', { add_category_name: add_category_name}, function(data, status){
        alert(data);
      });
  } else {
        alert('カテゴリ名を入力してください。');
  }
}
//スポーツカテゴリ情報追加
function addSportsCategory(){
  jQuery.blockUI({ message: $('#addSportsCategoryForm'),css:{
      padding:        0,
      margin:         0,
      width:          '50%',
      top:            '10%',
      bottom:         'auto',
      left:           '20%',
      textAlign:      'center',
      color:          '#000',
      backgroundColor:'#fff',
      cursor:         'auto'
    } } );
}
//スポーツカテゴリ情報追加
function addSportsCategorySubmit(){
  var add_category_name = document.getElementById('add_category_name').value;

  if (add_category_name!='') {
    $.post('add_sports_category_info.php', { add_category_name: add_category_name}, function(data, status){
        alert(data);
      });
  } else {
        alert('スポーツカテゴリ名を入力してください。');
  }
}
//フレンド検索
function searchFollow(u_id){
	document.form1.action='m_mem_user_follow.php?action=search&u='+u_id;
	document.form1.submit();
}
function searchFollower(u_id){
	document.form1.action='m_mem_user_follower.php?action=search&u='+u_id;
	document.form1.submit();
}
function searchBBSComments(u_id){
	document.form1.action='m_app_bbs_comments.php?action=search&u='+u_id;
	document.form1.submit();
}
function searchSystaComments(u_id){
	document.form1.action='m_app_systa_comments.php?action=search&u='+u_id;
	document.form1.submit();
}
//プッシュ情報追加
function addPush(){
    jQuery.blockUI({ message: $('#addPushForm'),css:{
        padding:        0,
        margin:         0,
        width:          '50%',
        top:            '10%',
        bottom:         'auto',
        left:           '20%',
        textAlign:      'center',
        color:          '#000',
        backgroundColor:'#fff',
        cursor:         'auto'
    } } );
}
//プッシュ情報追加
function addPushInfoSubmit(){
    var add_msg = document.getElementById('add_msg').value;
    var add_area_id = document.getElementById('add_area_id').value;

    if (add_msg!='') {
        $.post('add_push.php', {add_msg: add_msg,add_area_id: add_area_id}, function(data, status){
            alert(data);
        });
    } else {
        alert('送信内容を入力してください。');
    }
}
//ユーザー情報追加
function addUser(){
    jQuery.blockUI({ message: $('#addUserForm'),css:{
        padding:        0,
        margin:         0,
        width:          '50%',
        top:            '10%',
        bottom:         'auto',
        left:           '20%',
        textAlign:      'center',
        color:          '#000',
        backgroundColor:'#fff',
        cursor:         'auto'
    } } );
}
//ユーザー情報追加
function addUserInfoSubmit(){
    var add_user_nick = document.getElementById('add_user_nick').value;
    var add_user_email = document.getElementById('add_user_email').value;
    var add_user_pwd = document.getElementById('add_user_pwd').value;

    if (add_user_nick!=''&&add_user_email!=''&&add_user_pwd!='') {
        $.post('add_user.php', {add_user_nick: add_user_nick,add_user_email: add_user_email,add_user_pwd: add_user_pwd}, function(data, status){
            alert(data);
        });
    } else {
        alert('送信内容を入力してください。');
    }
}
//会社情報追加
function addComp(){
    jQuery.blockUI({ message: $('#addCompForm'),css:{
        padding:        0,
        margin:         0,
        width:          '50%',
        top:            '10%',
        bottom:         'auto',
        left:           '20%',
        textAlign:      'center',
        color:          '#000',
        backgroundColor:'#fff',
        cursor:         'auto'
    } } );
}
//会社情報追加
function addCompInfoSubmit(){
    var add_comp_name = document.getElementById('add_comp_name').value;
    var add_dep_name = document.getElementById('add_dep_name').value;
    var add_dep_flg = document.getElementById('add_dep_flg').checked;
    var add_mail = document.getElementById('add_mail').value;
    var add_pwd = document.getElementById('add_pwd').value;
    var add_man_name = document.getElementById('add_man_name').value;
    var add_tel = document.getElementById('add_tel').value;

    if (add_comp_name!=''&&add_dep_name!=''&&add_mail!=''&&add_pwd!='') {
        $.post('add_comp.php', {add_comp_name: add_comp_name,add_dep_name: add_dep_name,add_dep_flg: add_dep_flg,add_mail: add_mail,add_pwd: add_pwd,add_man_name: add_man_name,add_tel: add_tel}, function(data, status){
            alert(data);
        });
    } else {
        alert('「*」マークの情報を入力してください。');
    }
}
//公式情報追加
function addCompCourse(){
    jQuery.blockUI({ message: $('#addCompCourseForm'),css:{
        padding:        0,
        margin:         0,
        width:          '50%',
        top:            '10%',
        bottom:         'auto',
        left:           '20%',
        textAlign:      'center',
        color:          '#000',
        backgroundColor:'#fff',
        cursor:         'auto'
    } } );
}
//公式情報追加
function addCompCourseInfoSubmit(){
    var add_comp_id = document.getElementById('add_comp_id').value;
    var add_course_name = document.getElementById('add_course_name').value;
    var add_from_date = document.getElementById('add_from_date').value;
    var add_to_date = document.getElementById('add_to_date').value;
    var add_price = document.getElementById('add_price').value;

    if (add_course_name!=''&&add_from_date!=''&&add_to_date!=''&&add_price!=''&&add_comp_id!='') {
        $.post('add_comp_course.php', {add_comp_id:add_comp_id,add_course_name: add_course_name,add_from_date: add_from_date,add_to_date: add_to_date,add_price: add_price}, function(data, status){
            alert(data);
        });
    } else {
        alert('「*」マークの情報を入力してください。');
    }
}
//広告コース情報追加
function addAdCourse(){
    jQuery.blockUI({ message: $('#addAdCourseForm'),css:{
        padding:        0,
        margin:         0,
        width:          '50%',
        top:            '10%',
        bottom:         'auto',
        left:           '20%',
        textAlign:      'center',
        color:          '#000',
        backgroundColor:'#fff',
        cursor:         'auto'
    } } );
}
//広告コース情報追加
function addAdCourseInfoSubmit(){
    var add_comp_id = document.getElementById('add_comp_id').value;
    var add_ad_id = document.getElementById('add_ad_id').value;
    var add_course_name = document.getElementById('add_course_name').value;
    var add_from_date = document.getElementById('add_from_date').value;
    var add_to_date = document.getElementById('add_to_date').value;
    var add_price = document.getElementById('add_price').value;

    if (add_course_name!=''&&add_from_date!=''&&add_to_date!=''&&add_price!=''&&add_ad_id!=''&&add_comp_id!='') {
        $.post('add_ad_course.php', {add_comp_id:add_comp_id,add_ad_id:add_ad_id,add_course_name: add_course_name,add_from_date: add_from_date,add_to_date: add_to_date,add_price: add_price}, function(data, status){
            alert(data);
        });
    } else {
        alert('「*」マークの情報を入力してください。');
    }
}
//広告コース情報追加
function addAd(){
    jQuery.blockUI({ message: $('#addAdForm'),css:{
        padding:        0,
        margin:         0,
        width:          '70%',
        top:            '5%',
        bottom:         'auto',
        left:           '20%',
        textAlign:      'center',
        color:          '#000',
        backgroundColor:'#fff',
        cursor:         'auto'
    } } );
}
//広告コース情報追加
function addAdInfoSubmit(){
    var add_ad_name = document.getElementById('add_ad_name').value;
    var add_ad_type = ""
    var oAd_types = document.getElementsByName("add_ad_type");
    for (var i=0;i<oAd_types.length;i++) {
        if (oAd_types[i].checked) {
            add_ad_type = oAd_types[i].value;
            break;
        }
    }
    var add_ad_size = document.getElementById('add_ad_size').value;
    var add_ad_url = document.getElementById('add_ad_url').value;
    var add_ad_img_url = document.getElementById('add_ad_img_url').value;

    if (add_ad_name!=''&&add_ad_type!=''&&add_ad_url!=''&&add_ad_img_url!='') {
        $.post('add_ad.php', {add_ad_name:add_ad_name,add_ad_type:add_ad_type,add_ad_size: add_ad_size,add_ad_url: add_ad_url,add_ad_img_url: add_ad_img_url}, function(data, status){
            alert(data);
        });
    } else {
        alert('「*」マークの情報を入力してください。');
    }
}

//戻るボタン
function back(){
	//window.history.go(-1);
	window.history.back(-1);
//	window.location.href=document.referrer;
}
function back_url(url){
	window.location.href=url;
}