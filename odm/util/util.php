<?php

function redirect($url){
	header("HTTP/1.1 301 Moved Permanently");
	header("Location: " . $url);
// 	http_redirect($url, $arrPam, true, HTTP_REDIRECT_PERM);
	exit();
}

function printErr($errMsgArr){

	$err_cd_list = $_SESSION['err_cd_list'];
	if(!empty($err_cd_list)){
		foreach($err_cd_list as $errCd){
			$errMsg.=$errMsgArr[$errCd] . "<br>";
		}

		print($errMsg);
	}
}

function export_csv($filename,$data) {
    header("Content-type:text/csv");
    header("Content-Disposition:attachment;filename=".$filename);
    header('Cache-Control:must-revalidate,post-check=0,pre-check=0');
    header('Expires:0');
    header('Pragma:public');
    echo $data;
}
function input_csv($handle) {
    $out = array ();
    $n = 0;
    while ($data = fgetcsv($handle, 10000)) {
        $num = count($data);
        for ($i = 0; $i < $num; $i++) {
            $out[$n][$i] = $data[$i];
        }
        $n++;
    }
    return $out;
}


//日本語版を使う　ただし、from_name　に日本語入れると文字化け
function sendMail($to, $subject, $body, $from_email,$from_name)
 {
$headers  = "MIME-Version: 1.0 \n" ;

$headers .= "From: " .
       "".mb_encode_mimeheader (mb_convert_encoding($from_name,"ISO-2022-JP","AUTO")) ."" .
       "<".$from_email."> \n";
$headers .= "Reply-To: " .
       "".mb_encode_mimeheader (mb_convert_encoding($from_name,"ISO-2022-JP","AUTO")) ."" .
       "<".$from_email."> \n";


$headers .= "Content-Type: text/plain;charset=ISO-2022-JP \n";


/* Convert body to same encoding as stated
in Content-Type header above */

$body = mb_convert_encoding($body, "ISO-2022-JP","AUTO");

/* Mail, optional paramiters. */
$sendmail_params  = "-f$from_email";

mb_language("ja");
$subject = mb_convert_encoding($subject, "ISO-2022-JP","AUTO");
$subject = mb_encode_mimeheader($subject);

//$subject =mb_encode_mimeheader( mb_convert_encoding($subject, "JIS", "EUC-JP") );

//$strMBS = mb_convert_encoding($strMailBodySend, "JIS", "EUC-JP");
//$strMS  = mb_encode_mimeheader( mb_convert_encoding($strMailSubj, "JIS", "EUC-JP") );

$result = mail($to, $subject, $body, $headers, $sendmail_params);

return $result;
}


//-----------下記クラスは文字化け発生するため廃止---
class smail {
	var $smtp = ADMIN_MAIL_SMTP;
	var $check = 1;
	var $username = ADMIN_MAIL;
	var $password = ADMIN_MAIL_PASS;
	var $s_from = ADMIN_MAIL;

	function smail ( $from, $password, $smtp, $check = 1 ) {
	if( preg_match("/^[^\d\-_][\w\-]*[^\-_]@[^\-][a-zA-Z\d\-]+[^\-](\.[^\-][a-zA-Z\d\-]*[^\-])*\.[a-zA-Z]{2,3}/", $from ) ) {
	//$this->username = substr( $from, 0, strpos( $from , "@" ) );
	$this->username = ADMIN_MAIL;
	$this->password = $password;
	$this->smtp = $smtp ? $smtp : $this->smtp;
	$this->check = $check;
	$this->s_from = $from;
	}
	}

	function send ( $to, $from, $subject, $message) {
	$fp = fsockopen ( $this->smtp, 25, $errno, $errstr, 60);
	if (!$fp ) return "サーバ接続失敗".__LINE__;
	set_socket_blocking($fp, true );
	$lastmessage=fgets($fp,512);
	if ( substr($lastmessage,0,3) != 220 ) return "Err1:$lastmessage".__LINE__;
	$yourname = "YOURNAME";
	if($this->check == "1") $lastact="EHLO ".$yourname."\r\n";
	else $lastact="HELO ".$yourname."\r\n";
	fputs($fp, $lastact);
	$lastmessage == fgets($fp,512);
	if (substr($lastmessage,0,3) != 220 ) return "Err2:$lastmessage".__LINE__;
	while (true) {
	$lastmessage = fgets($fp,512);
	if ( (substr($lastmessage,3,1) != "-") or (empty($lastmessage)) )
	break;
	}
	if ($this->check=="1") {
	$lastact="AUTH LOGIN"."\r\n";
	fputs( $fp, $lastact);
	$lastmessage = fgets ($fp,512);
	if (substr($lastmessage,0,3) != 334) return "Err3:$lastmessage".__LINE__;
	$lastact=base64_encode($this->username)."\r\n";
	fputs( $fp, $lastact);
	$lastmessage = fgets ($fp,512);
	if (substr($lastmessage,0,3) != 334) return "Err4:$lastmessage".__LINE__;
	$lastact=base64_encode($this->password)."\r\n";
	fputs( $fp, $lastact);
	$lastmessage = fgets ($fp,512);
	if (substr($lastmessage,0,3) != "235") return "Err5:$lastmessage".__LINE__;
	}
	//FROM:
	$lastact="MAIL FROM: <". $this->s_from . ">\r\n";
	fputs( $fp, $lastact);
	$lastmessage = fgets ($fp,512);
	if (substr($lastmessage,0,3) != 250) return "Err6:$lastmessage".__LINE__;
	//TO:
	$lastact="RCPT TO: <". $to ."> \r\n";
	fputs( $fp, $lastact);
	$lastmessage = fgets ($fp,512);
	if (substr($lastmessage,0,3) != 250) return "Err7:$lastmessage".__LINE__;
	//BCC:
//	if ($bcc!=""){
//		die($bcc);
//		$lastact="RCPT TO: <". $bcc ."> \r\n";
//		fputs( $fp, $lastact);
//		$lastmessage = fgets ($fp,512);
//		if (substr($lastmessage,0,3) != 250) return "Err8:bcc:$lastmessage".__LINE__;
//	}
	//DATA
	$lastact="DATA\r\n";
	fputs($fp, $lastact);
	$lastmessage = fgets ($fp,512);
	if (substr($lastmessage,0,3) != 354) return "Err8:$lastmessage".__LINE__;

	//Subject
	$head="Subject: $subject\r\n";
	$message = $head."\r\n".$message;

	//From
	$head="From: $from\r\n";
	$message = $head.$message;
	//To
	$head="To: $to\r\n";
	$message = $head.$message;

	//処理Bcc
//	if ($bcc!=""){
//		$head="Bcc: $bcc\r\n";
//		$message = $head.$message;
//	}

	$message .= "\r\n.\r\n";
	fputs($fp, $message);
	$lastact="QUIT\r\n";
	fputs($fp,$lastact);
	fclose($fp);
	return 0;
	}
}
function get_real_ip(){
$ip=false;
if(!empty($_SERVER["HTTP_CLIENT_IP"])){
$ip = $_SERVER["HTTP_CLIENT_IP"];
}
if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
$ips = explode (", ", $_SERVER['HTTP_X_FORWARDED_FOR']);
if ($ip) { array_unshift($ips, $ip); $ip = FALSE; }
for ($i = 0; $i < count($ips); $i++) {
if (!eregi ("^(10|172\.16|192\.168)\.", $ips[$i])) {
$ip = $ips[$i];
break;
}
}
}
return ($ip ? $ip : $_SERVER['REMOTE_ADDR']);
}
function imgUpload($file_tmp_name,$file,$db_file_name,$upload_folder) {
	$ret=1;
	$systime=date('Y-m-d H:i:s',time());
	$ip=get_real_ip();
	$uptypes=array(
    'image/jpg',
    'image/jpeg',
    'image/png',
    'image/pjpeg',
    'image/gif',
    'image/bmp',
    'image/x-png'
	);

    $logstr = "$systime $ip INFO：▼ファイルアップロード開始 \r\n";
	error_log($logstr,3,'../log/gen.log');


    if (!is_uploaded_file($file_tmp_name))
    //ファイル存在チェック
    {
		$logstr = "$systime ERR：ファイルがありません！\r\n";
		$logstr .= "$systime $ip INFO：▲ファイルアップロード異常終了 \r\n";
		error_log($logstr,3,'../log/gen.log');
		return $ret;
    }

    $file = $_FILES["upfile"];
    $logfilesize=$file["size"];

    if ($logfilesize >= 1073741824) {
          $logfilesize=  sprintf("%0.2fGB",($logfilesize / 1073741824));
    }
    elseif ($logfilesize >= 1048576) {
        $logfilesize=  sprintf("%0.2fMB",($logfilesize / 1048576));
    }else{
        $logfilesize=  sprintf("%0.2fKB",($logfilesize / 1024));
    }

    if(MAX_FILE_SIZE < $file["size"])
    //ファイルサイズチェック
    {
        $logstr = "$systime ERR：ファイルサイズ異常! MaxSize=MAX_FILE_SIZE ファイルサイズ：$logfilesize \r\n";
        $logstr .= "$systime $ip INFO：▲ファイルアップロード異常終了 \r\n";
		error_log($logstr,3,'../log/gen.log');
        return $ret;
    }

    if(!in_array($file["type"], $uptypes))
    //ファイル類型チェック
    {
        $logfiletype=$file["type"];
        $logstr = "$systime ERR：ファイル類型異常：$logfiletype \r\n";
		$logstr .= "$systime $ip INFO：▲ファイルアップロード異常終了 \r\n";
		error_log($logstr,3,'../log/gen.log');
        return $ret;
    }

    if(!file_exists($upload_folder))
    {
        $logstr = "$systime ERR：ファイルアップロードフォルダーが存在しません！：NEWS_UPLOAD_FOLDER_ROOT \r\n";
        $logstr .= "$systime $ip INFO：▲ファイルアップロード異常終了 \r\n";
		error_log($logstr,3,'../log/gen.log');
		return $ret;
    }

    $filename=$file["tmp_name"];
	$filetruename=basename($file["name"]);
    $pinfo=pathinfo($file["name"]);
    $ftype=$pinfo['extension'];

    $destination = $upload_folder.$db_file_name.".".$ftype;

    if (file_exists($destination))
    {
        $logstr = "$systime ERR：ファイルが既に存在しました。：$destination \r\n";
        $logstr .= "$systime $ip INFO：▲ファイルアップロード異常終了 \r\n";
		error_log($logstr,3,'../log/gen.log');
        return $ret;
    }

    if(!move_uploaded_file ($filename, $destination))
    {
        $logstr = "$systime ERR：ファイルアップロード失敗！：$filetruename ファイルアップロードフォルダー：$destination \r\n";
        $logstr .= "$systime $ip INFO：▲ファイルアップロード異常終了 \r\n";
		error_log($logstr,3,'../log/gen.log');
        return $ret;
    }


    $logstr = "$systime $ip INFO：▲ファイルアップロード成功！！ 元ファイル名：$filetruename アップロードファイル名: $destination ファイルサイズ：$logfilesize \r\n";
	error_log($logstr,3,'../log/gen.log');

	$ret=0;
    return $ret;
}
function sendPush_old($message,$deviceToken,$passphrase,$pem_path){
	$fp='';
	$err='';
	$errstr='';
	$r_msg='0';
	// Put your device token here (without spaces):
	$deviceToken = '9bcf827309450c7638759708bf22485948f35d88492971cf33c6450e4bc3f18e';

	// Put your private key's passphrase here:
	$passphrase = '';

	// Put your alert message here:
	$message = 'My first push notification!';
	$pem_path='apns_dev.pem';

	////////////////////////////////////////////////////////////////////////////////

	$ctx = stream_context_create();
	stream_context_set_option($ctx, 'ssl', 'local_cert',$pem_path );
	stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

	// Open a connection to the APNS server
	$fp = stream_socket_client(
		'ssl://gateway.sandbox.push.apple.com:2195', $err,
		$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

	if (!$fp){
		$msg=  "Failed to connect: $err $errstr" . PHP_EOL;
		return $msg;
	}

	$msg=  'Connected to APNS' . PHP_EOL;
	return $msg;

	// Create the payload body
	$body['aps'] = array(
		'alert' => $message,
		'sound' => 'default'
		);

	// Encode the payload as JSON
	$payload = json_encode($body);

	// Build the binary notification
	$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

	// Send it to the server
	$result = fwrite($fp, $msg, strlen($msg));

	if (!$result){
		$msg= 'Message not delivered' . PHP_EOL;
		return $msg;
	}
	else{
		$msg=  'Message successfully delivered' . PHP_EOL;
		return $msg;
	}
	// Close the connection to the server
	fclose($fp);
}

function send_noti_mail($user_email,$title,$content){

	ini_set("mbstring.internal_encoding","UTF-8");
	$from = "master@syszo.com";
	$subject = "【シス蔵】 ".$title;
	$headers = "From: $from";

	mb_language("uni"); 
	mb_send_mail($user_email,$subject,$content,$headers);
}