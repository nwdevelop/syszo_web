<?php
require 'include.php';
$title="SYSZO - 情シス特化型メディア";

$url = API_PATH.API_KNOW_INFO_LIST;
//新着の投稿
$post_data_new['p_size'] = 1000000;//表示件数
$post_data_new['type'] = 1;//新着順

$o = "";
foreach ( $post_data_new as $k => $v ){$o.= "$k=" . urlencode( $v ). "&" ;}
$post_data_new = substr($o,0,-1);
$res = request_post($url, $post_data_new);
$new_json = json_decode($res,TRUE);
//echo var_dump($new_json);

$result = $new_json['result'];
$msg = $new_json['msg'];
if($result!="0"){
	$count_new = count($new_json["data"]);
}
?>
<?php include "head.php"; ?>
</head>
<body>
<?php include "header.php"; ?>
<div id="wrapper">
  <div id="contents">
		<?php include "nav.php"; ?>
    <section id="newPosts">
      <h2>新着順</h2>
      <ul>
				<?php for ($i = 0; $i < $count_new; $i++){ ?>
					<li <?php if($new_json["data"][$i]['urgent']=="2") {echo "class=''";}?>>
						<a href="detail.php?id=<?php echo $new_json["data"][$i]['know_id'];?>">
							<span class="postTitle"><em><?php echo $new_json["data"][$i]["title"];?></em></span>
							<span class="fbIine"><?php echo $new_json["data"][$i]["good_sum"];?></span>
							<span class="comeCount"><?php echo $new_json["data"][$i]["comment_sum"];?></span>
							<span class="date">update - <?php echo date('Y.m.d H:i',strtotime($new_json["data"][$i]['time']));?></span>
						</a>
					</li>
				<?php } ?>
      </ul>
    </section>
    <!--/#newPosts--> 
    
  </div>
  <!--/#contents-->
	<?php include "side.php"; ?>
</div>
<!--/#wrapper-->
<?php include "footer.php"; ?>
<script>
$(function() {
    $('#button').click(function(){
    $(this).next('#questionArea').slideToggle();
	$("#button").toggleClass("active");
    });
});
</script>
</body>
</html>