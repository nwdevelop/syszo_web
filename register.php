<?php
require 'include.php';
$title="SYSZO - 情シス特化型メディア";

	$action=$_GET['action'];

	if($action=="reg"){

		$url = API_PATH.API_LOGIN_REG;

		$user_nick=$_POST['user_nick'];
		$user_email=$_POST['user_email'];

		$post_data['user_nick'] = $_POST['user_nick'];
		$post_data['user_email'] = $_POST['user_email'];
		$post_data['user_pwd'] = $_POST['user_pwd'];
		$post_data['device'] = "0";

		$o = "";
		foreach ( $post_data as $k => $v ){ $o.= "$k=" . urlencode( $v ). "&" ;}
		$post_data = substr($o,0,-1);
		$res = request_post($url, $post_data);
		$obj = json_decode($res);

		$result = $obj->{'result'};
		$msg = $obj->{'msg'};
		if($result!="0"){

			$_SESSION['user_id']='';
			$_SESSION['user_nick']='';
			setcookie('user_id','',time()-3600);
			setcookie('user_nick','',time()-3600);
			$_SESSION['user_id']=$obj->{'data'}->{'user_id'};
			$_SESSION['user_nick']=$_POST['user_nick'];
			header("Location:thanks.php");
		}
		
	}
?>
<?php include "head.php"; ?>
</head>
<body>
<?php include "header.php"; ?>
<div id="wrapper">
  <section id="mypage">
    <h2>会員登録</h2>
    <div id="myPageInner">
    <form action="?action=reg" method="post">
      <dl>
			<?php if($result=="0"){echo "<dt  style='color:red;'>$msg</dt>";}?>
      <dt>ニックネーム</dt>
        <dd>
          <input type="text" name="user_nick" maxlength="15" placeholder="※１５文字以内" size="40" value="<?php echo $user_nick;?>" />
        </dd>
        <dt>メールアドレス</dt>
        <dd>
          <input type="email" name="user_email" maxlength="100" size="40" value="<?php echo $user_email;?>" />
        </dd>
        <dt>パスワード</dt>
        <dd>
          <input type="password" name="user_pwd" size="25" value="" maxlength="100" />
          <p class="note">※6〜16文字</p>
        </dd>
      </dl>
      <div id="submit">
        <input type="submit" value="送信する" onclick="ga('send', 'event', 'button', 'click', '通常の新規登録');" />
      </div>
    </form>
    </div>
  </section>
</div>
<!--/#wrapper-->
<?php include "footer.php"; ?>
<script>
$(function() {
    $('#button').click(function(){
    $(this).next('#questionArea').slideToggle();
	$("#button").toggleClass("active");
    });
});
</script>
</body>
</html>