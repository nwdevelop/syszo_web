<?php
	require 'include.php';
	$title="SYSZO - 情シス特化型メディア";
	$action=$_GET['action'];

	if($action=="login"){
		$url = API_PATH.API_LOGIN;
		$user_email=$_POST['user_email'];
		$user_pwd=$_POST['user_pwd'];
		$contact=$_POST['contact'];

		$post_data['user_email'] = $user_email;
		$post_data['user_pwd'] = $user_pwd;

		$o = "";
		foreach ( $post_data as $k => $v ){ $o.= "$k=" . urlencode( $v ). "&" ;}
		$post_data = substr($o,0,-1);
		$res = request_post($url, $post_data);
		$obj = json_decode($res);

		$result = $obj->{'result'};
		$msg = $obj->{'msg'};
		if($result!="0"){
			if($contact=="1"){
				$expire = time() + 30*86400;
				setcookie ("user_id",$obj->{'data'}->{'user_id'}, $expire);
				setcookie ("user_nick",$obj->{'data'}->{'user_nick'}, $expire);
			}else{
				$_SESSION['user_id']=$obj->{'data'}->{'user_id'};
				$_SESSION['user_nick']=$obj->{'data'}->{'user_nick'};
			}
			header("Location:".HOME_PAGE);
		}
		
	}

?>
<?php include "head.php"; ?>
</head>
<body>
<?php include "header.php"; ?>
<div id="wrapper">
  <section id="mypage">
    <h2>ログイン</h2>
    <div id="myPageInner">
    <form action="?action=login" method="post">
      <dl>
				<?php if($result=="0"){echo "<dt  style='color:red;'>$msg</dt>";}?>
        <dt>メールアドレス</dt>
        <dd>
          <input type="text" name="user_email" maxlength="100" size="40" value="<?php echo $user_email;?>" />
        </dd>
        <dt>パスワード</dt>
        <dd>
          <input type="password" name="user_pwd" size="25" value="<?php echo $user_pwd;?>" />
          <p class="note">※6〜16文字</p>
        </dd>
      </dl>
      <p id="check">
              <input name="contact" type="checkbox" value="1" id="check-1" />
              <label for="check-1">次回から自動ログイン</label>
     </p>
      <div id="submit">
        <input name="commit" type="submit" value="ログイン"  onclick="ga('send', 'event', 'button', 'click', '通常のログイン');"/>
      </div>
    </form>
    <p id="forget">パスワードを忘れた方は<a href="password.php">パスワードの再設定</a>を行ってください。</p>
    </div>
  </section>
</div>
<!--/#wrapper-->
<?php include "footer.php"; ?>
<script>
$(function() {
    $('#button').click(function(){
    $(this).next('#questionArea').slideToggle();
	$("#button").toggleClass("active");
    });
});
</script>
</body>
</html>