<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67612275-1', 'auto');
  ga('send', 'pageview');

</script>
<section id="supporter">
  <div class="inner">
    <h3>SYSZO応援企業</h3>
    <ul>
      <li><a href="http://www.nec.co.jp/" target="_blank"><img alt="NEC" src="images/NEC.gif"></a></li>
      <li><a href="http://welcome.hp.com/country/jp/ja/welcome.html" target="_blank"><img alt="日本HP" src="images/hp.gif"></a></li>
      <li><a href="http://www.otsuka-shokai.co.jp/" target="_blank"><img alt="大塚商会" src="images/otsuka.gif"></a></li>
      <li><a href="http://www.cisco.com/japanese/warp/public/3/jp/index.shtml" target="_blank"><img alt="シスコシステムズ" src="images/cisco.gif"></a></li>
      <li><a href="http://jp.fujitsu.com/" target="_blank"><img alt="FIJITSU" src="images/fujitsu.gif"></a></li>
      <li><a href="http://www.ibm.com/jp/" target="_blank"><img alt="IBM" src="images/ibm.gif"></a></li>
      <li><a href="http://www.hitachi.co.jp/" target="_blank"><img alt="HITACHI" src="images/hitachi.gif"></a></li>
      <li><a href="http://www.mind.co.jp/" target="_blank"><img alt="三菱インフォメーションテクノロジー" src="images/mdit.gif"></a></li>
      <li><a href="http://www.industry.ricoh.co.jp/" target="_blank"><img alt="リコープリンティングシステムズ" src="images/ricoh.gif"></a></li>
      <li><a href="http://www.orix.co.jp/grp/" target="_blank"><img alt="ORIX" src="images/orix.gif"></a></li>
      <li><a href="http://www.datalive.co.jp/" target="_blank"><img alt="データライブ" src="images/datalive.gif"></a></li>
      <li><a href="http://www.neo.co.jp/" target="_blank"><img alt="ネオジャパン" src="images/NEOJAPAN.gif"></a></li>
      <li><a href="http://www.creo.co.jp/" target="_blank"><img alt="クレオ" src="images/ecreo01.gif"></a></li>
      <li><a href="http://www.oro.co.jp/" target="_blank"><img alt="オロ" src="images/oRo.jpg"></a></li>
      <li><a href="http://www.himico.net/" target="_blank"><img alt="ヒミコ" src="images/himico.jpg"></a></li>
      <li><a href="http://www.chamberweb.jp/" target="_blank"><img alt="CHAMBER WEB" src="images/chamber_web.gif"></a></li>
      <li><a href="http://www.itouentai.jp/" target="_blank"><img alt="IT経営応援隊" src="images/it_oen.gif"></a></li>
      <li><a href="http://www.norkresearch.co.jp/" target="_blank"><img alt="NORK RESEARCH" src="images/norkresearch.gif"></a></li>
      <li><a href="http://okwave.jp/" target="_blank"><img alt="OKWAVE" src="images/okwave.gif"></a></li>
    </ul>
  </div>
</section>
<footer>
  <div class="inner">
    <nav>
      <ul>
        <li><a href="policy.php">利用規約</a>&nbsp;&nbsp;｜&nbsp;&nbsp;</li>
        <li><a href="privacy.php">個人情報に関して</a>&nbsp;&nbsp;｜&nbsp;&nbsp;</li>
        <!--<li><a href="manor.php">マナー</a>&nbsp;&nbsp;｜&nbsp;&nbsp;</li>-->
        <li><a href="ban.php">禁止事項</a></li>
      </ul>
    </nav>
    <h2><a href="http://www.ug-inc.net/" target="_blank"><img src="images/logo_uniteandgrow.png" alt="Unite and Grow Inc."/></a></h2>
    <p id="gSign"><a href="http://jp.globalsign.com/" target="_blank"><img src="images/globalsign.png" alt="GlobalSign"/></a></p>
    <p><small>Copyright © Unite and Grow Inc. <span>All Rights Reserved.</span></small></p>
  </div>
</footer>
