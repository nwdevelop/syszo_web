<?php
require 'include.php';
$title="SYSZO - 情シス特化型メディア";

$login_user_id=$_SESSION['user_id'];
if($login_user_id==""){$login_user_id=$_COOKIE['user_id'];}
$login_user_name=$_SESSION['user_nick'];
if($login_user_name==""){$login_user_name=$_COOKIE['user_nick'];}

$id=$_GET['id'];
$action=$_GET['action'];

if($action=="good"){
	if($login_user_id==""){
		header("Location:https://syszo.com/login.php");
	}

	$url_good = API_PATH.API_KNOW_GOOD;
	$post_data_good['user_id'] = $login_user_id;
	$post_data_good['know_id'] = $id;
	$post_data_good['comments_id'] = $_GET['c_id'];
	$a = "";
	foreach ( $post_data_good as $key => $val ){ $a.= "$key=" . urlencode( $val ). "&" ;}
	$post_data_good = substr($a,0,-1);
	$res_good = request_post($url_good, $post_data_good);
	$obj_good = json_decode($res_good);

	$result = $obj_good->{'result'};
	$msg = $obj_good->{'msg'};
}
if($action=="info_good"){
	if($login_user_id==""){
		header("Location:https://syszo.com/login.php");
	}
	$url_info_good = API_PATH.API_KNOW_GOOD;
	$post_data_info_good['user_id'] = $login_user_id;
	$post_data_info_good['know_id'] = $id;
	$post_data_info_good['comments_id'] = '0';
	$a = "";
	foreach ( $post_data_info_good as $key => $val ){ $a.= "$key=" . urlencode( $val ). "&" ;}
	$post_data_info_good = substr($a,0,-1);
	$res_info_good = request_post($url_info_good, $post_data_info_good);
	$obj_info_good = json_decode($res_info_good);

	$result = $obj_info_good->{'result'};
	$msg = $obj_info_good->{'msg'};
}

if($action=="new_comment"){
	if($login_user_id==""){
		header("Location:https://syszo.com/login.php");
	}

	$url_comment = API_PATH.API_KNOW_COMMENTS;
	$post_data_comment['user_id'] = $login_user_id;
	$post_data_comment['know_id'] = $id;
	$post_data_comment['reply_content'] = $_POST['reply_comments'];
	$a = "";
	foreach ( $post_data_comment as $key => $val ){ $a.= "$key=" . urlencode( $val ). "&" ;}
	$post_data_comment = substr($a,0,-1);
	$res_comment = request_post($url_comment, $post_data_comment);
	$obj_comment = json_decode($res_comment);

	$result = $obj_comment->{'result'};
	$msg = $obj_comment->{'msg'};
}

if($id!=""){
	//知恵袋详情
	$url = API_PATH.API_KNOW_INFO;
	$post_data['know_id'] = $id;
	$post_data['user_id'] = $login_user_id;
	$o = "";
	foreach ( $post_data as $k => $v ){ $o.= "$k=" . urlencode( $v ). "&" ;}
	$post_data = substr($o,0,-1);
	$res = request_post($url, $post_data);
//	$res = str_replace("	"," ",$res);
	$obj = json_decode($res);
	$result = $obj->{'result'};
	$msg = $obj->{'msg'};
	if($result=="0"){
		header("Location:".HOME_PAGE);
	}
	//知恵袋いいね数量
	$url_get_good = API_PATH.API_KNOW_GET_GOOD_COUNT;
	$post_data_get_good['know_id'] = $id;
	$o = "";
	foreach ( $post_data_get_good as $k => $v ){ $o.= "$k=" . urlencode( $v ). "&" ;}
	$post_data_get_good = substr($o,0,-1);
	$res_get_good = request_post($url_get_good, $post_data_get_good);
	$obj_get_good = json_decode($res_get_good);
	$result = $obj_get_good->{'result'};
	$msg = $obj_get_good->{'msg'};
	if($result=="0"){
		header("Location:".HOME_PAGE);
	}
	//知恵袋评论列表接口
	$url = API_PATH.API_KNOW_COMMENTS_LIST;
	$post_data_comments['know_id'] = $id;
	$post_data_comments['p_size'] = 10000;
	$post_data_comments['user_id'] = $login_user_id;
	$o = "";
	foreach ( $post_data_comments as $k => $v ){ $o.= "$k=" . urlencode( $v ). "&" ;}
	$post_data_comments = substr($o,0,-1);
	$res = request_post($url, $post_data_comments);
//	$res = str_replace("	"," ",$res);
	$json = json_decode($res,TRUE);
	$result = $json['result'];
	$msg = $json['msg'];
	if($result=="0"){
		header("Location:".HOME_PAGE);
	}else{
		$count_comments = count($json["data"]);
	}
}
function url_henkan($mojiretu){
	$mojiretu = htmlspecialchars($mojiretu,ENT_QUOTES);
	
	//文字列にURLが混じっている場合のみ下のスクリプト発動
	if(preg_match("(https?://[-_.!~*\'()a-zA-Z0-9;/?:@&=+$,%#]+)",$mojiretu)){
		preg_match_all("(https?://[-_.!~*\'()a-zA-Z0-9;/?:@&=+$,%#]+)",$mojiretu,$pattarn);
			foreach ($pattarn[0] as $key=>$val){
				$replace[] = '<a href="'.$val.'" target="_blank">'.$val.'</a>';
			}
		$mojiretu = str_replace($pattarn[0],$replace,$mojiretu);
		}
		$mojiretu = nl2br($mojiretu);
		return $mojiretu;
	}
?>
<?php include "head.php"; ?>
</head>
<body>
<?php include "header.php"; ?>
<div id="wrapper">
  <div id="contents">
		<?php include "nav.php"; ?>
    <section id="detail">
      <div class="detailArea">
        <p class="date">update - <?php echo date('Y.m.d H:i:s',strtotime($obj->{'data'}->{'time'}));?></p>
         
		  <?php 
		  if($obj->{'data'}->{'status'}=='0'){
			  if($obj->{'data'}->{'vip_flg'}=='1'){$vip_style="color: #6e9bd4;padding: 6px 0 6px 20px;background: url(images/vip.png) no-repeat left top;";}else{$vip_style="";}
			  $str_url="mypage.php?v=1&u=".$obj->{'data'}->{'user_id'};
			  $str_user_name=$obj->{'data'}->{'user_name'};
			  echo "<p class='user'><a style='$vip_style' href='$str_url'><span class='userName'>$str_user_name</span>さん</a></p>";
		  }else{
			  if($obj->{'data'}->{'vip_flg'}=='1'){$vip_style="color: #6e9bd4;padding: 6px 0 6px 20px;background: url(images/vip.png) no-repeat left top;";}else{$vip_style="";}
			  $str_user_name=$obj->{'data'}->{'user_name'};
			  if ($str_user_name == "") {
			  	echo "<p class='user'><a style='$vip_style'><span class='userName'>ユーザー</span>さん</a></p>";
			  }else {
			  echo "<p class='user'><a style='$vip_style'><span class='userName'>$str_user_name</span>さん</a></p>";
			  }
		  }
		
		  
		  ?>
		  
        <h2><?php echo $obj->{'data'}->{'title'};?></h2>
        <p>
					<?php echo url_henkan($obj->{'data'}->{'content'});?>
				</p>
				<?php if($obj->{'data'}->{'info_img'}!="") { ?>
					<p class="img"><img src="<?php echo $obj->{'data'}->{'info_img'};?>" alt=""></p>
				<? } ?>
		 		 <div class="commentList">
		  		<dl style="border-bottom: none;">
					  <span style="display: block;float: right;font-weight: bold;margin-left: 10px;margin-top: 5px; color: #6e9bd4;"><?php echo $obj_get_good->{'data'}->{'good_count'};?></span>
					<?php if ($obj->{'data'}->{'user_id'}!=$login_user_id){;?>
					  <span style="display: block;float: right;font-weight: bold;margin-left: 10px;margin-top: 13px; color: #6e9bd4;font-size: 15px;"><?php echo $json["data"][$i]['comment_goods'];?></span>

					  <?php if ($obj->{'data'}->{'if_good'}=="1"){;?>
						  <dd class="iine"><img style="display: block;" src="images/btn_iine.png" alt="いいね" onclick="infoisGood(<?php echo $id;?>)"/></dd>
					  <?php } else {?>
						  <dd class="iine"><img style="display: block;" src="images/btn_iine_none.png" alt="いいね" onclick="infoisGood(<?php echo $id;?>)"/></dd>
					  <?php } ?>
					<?php } else { ?>
						<dd class="iine"><img style="display: block;" src="images/btn_iine_none.png" alt="いいね" /></dd>
					<?php } ?>
			  	</dl>
					 </div>
      </div>
			<?php if ($count_comments>0) { ?>
      <div class="commentList">
			<?php } ?>
				<?php for ($i = 0; $i < $count_comments; $i++){ ?>
				<dl>
          <dt><?php echo date('Y.m.d H:i',strtotime($json["data"][$i]['comments_time']));?></dt>
          <dd class="user"><a style="color: #6e9bd4;<?php if($json["data"][$i]['vip_flg']=='1'){echo "padding: 6px 0 6px 20px;background: url(images/vip.png) no-repeat left top;";}?>" href="mypage.php?v=1&u=<?php echo $json["data"][$i]['user_id'];?>"><span class="userName"><?php if($json["data"][$i]['user_name'] == ""){echo "ユーザー";}else{ echo $json["data"][$i]['user_name'];}?></span>さん</a></dd>
          <dd class="commentText"><?php echo url_henkan($json["data"][$i]['comments']);?></dd>
					<span style="display: block;float: right;font-weight: bold;margin-left: 10px;margin-top: 13px; color: #6e9bd4;font-size: 15px;"><?php echo $json["data"][$i]['comment_goods'];?></span>
					<?php if ($json["data"][$i]['user_id']!=$login_user_id){;?>
						<?php if ($json["data"][$i]['if_good']=="1"){;?>
							<dd class="iine"><img src="images/btn_iine.png" alt="いいね" onclick="itsGood(<?php echo $id;?>,<?php echo $json["data"][$i]['comments_id'];?>)"/></dd>
						<?php } else {?>
							<dd class="iine"><img src="images/btn_iine_none.png" alt="いいね" onclick="itsGood(<?php echo $id;?>,<?php echo $json["data"][$i]['comments_id'];?>)"/></dd>
						<?php } ?>
					<?php } else { ?>
						<dd class="iine"><img style="display: block;" src="images/btn_iine_none.png" alt="いいね" /></dd>
					<?php } ?>
        </dl>
				<?php } ?>
			<?php if ($count_comments>0) { ?>
      </div>
			<?php } ?>
    </section>
    <!--/#detail-->
    <section id="commentArea">
      <form action="?action=new_comment&id=<?php echo $id;?>" method="post">
      <dl>
      <dt>コメントする</dt>
			<?php if($login_user_name!="") { ?>
				<dd class="myName"><?php echo $login_user_name;?></dd>
      <dd><textarea rows="5" name="reply_comments" maxlength="1000"></textarea></dd>
      </dl>
      <p class="commentSend"><input type="submit" value="コメント送信" /></p>
			<?php }else{ ?>
				<p class="commentSend"><a href="https://syszo.com/login.php" style="font-size: 15px;color: #fff;background: #444;padding: 8px 10px;text-align:center;border:none;border-radius:4px;cursor:pointer;">ログインしてからコメントできます</a></p>
			<?php } ?>
			
      </form>
    </section>
  </div>
  <!--/#contents-->
	<?php include "side.php"; ?>
</div>
<!--/#wrapper-->
<?php include "footer.php"; ?>
<script>
$(function() {
    $('#button').click(function(){
    $(this).next('#questionArea').slideToggle();
	$("#button").toggleClass("active");
    });
});
</script>
<script>
	function itsGood(know_id,comments_id) {
		var pageurl="?action=good&id="+know_id+"&c_id="+comments_id;
		window.location.href=pageurl;
	}
	function infoisGood(know_id) {
		var pageurl="?action=info_good&id="+know_id;
		window.location.href=pageurl;
	}
</script>
</body>
</html>
