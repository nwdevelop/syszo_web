<?php
require 'include.php';
$title="SYSZO - 情シス特化型メディア";
?>
<?php include "head.php"; ?>
</head>
<body>
<?php include "header.php"; ?>
<div id="wrapper">
  <section id="mypage">
    <h2>会員登録完了</h2>
    <div id="myPageInner">
      <p id="thanksText">ご登録いただきありがとうございました。<br>
        こちらのアカウントでアプリもご利用いただけます。<br>
        <br>
        <strong>1分でわかるSYSZOアプリ</strong><br>
        <img src="images/koma.jpg" alt="3分でわかるSYSZOアプリ" /></p>
        <h3 id="goSYSZO"><strong>Go</strong> SYSZOアプリ</h3>
      <ul id="appLink">
        <li><a href="https://itunes.apple.com/jp/app/syszo/id991513528?l=ja&ls=1&mt=8" target="_blank"><img src="images/btn_appstore.png" alt="App Store" /></a></li>
        <li><a href="https://play.google.com/store/apps/details?id=com.app.ug_inc&hl=ja" target="_blank"><img src="images/btn_googleplay.png" alt="Google play"/></a></li>
      </ul>
    </div>
  </section>
</div>
<!--/#wrapper-->
<?php include "footer.php"; ?>
<script>
  $(function() {
    $('#button').click(function(){
      $(this).next('#questionArea').slideToggle();
      $("#button").toggleClass("active");
    });
  });
</script>
</body>
</html>