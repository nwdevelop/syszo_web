<?php
require 'include.php';
header("Expires:Mon,26 Jul 1970 05:00:00 GMT");
header("Last-Modified:".gmdate("D, d M Y H:i:s")."GMT");
header("Cache-Control:no-cache,must-revalidate");
header("Pragma:no-cache");
$title="SYSZO - 情シス特化型メディア";

$url = API_PATH.API_KNOW_SEARCH;

//検索
$search_key=$_POST['search_key'];
if($search_key==""){
	$search_key=$_GET['key'];
}

$post_data_new['p_size'] = 10000000;//表示件数
$post_data_new['keyword'] = $search_key;//検索キーワード

$o = "";
foreach ( $post_data_new as $k => $v ){$o.= "$k=" . urlencode( $v ). "&" ;}
$post_data_new = substr($o,0,-1);
$res = request_post($url, $post_data_new);
$new_json = json_decode($res,TRUE);
//echo var_dump($new_json);

$result = $new_json['result'];
$msg = $new_json['msg'];
if($result!="0"){
	$count_new = count($new_json["data"]["list"]);
}
?>
<?php include "head.php"; ?>
</head>
<body>
<?php include "header.php"; ?>
<div id="wrapper">
  <div id="contents">
		<?php include "nav.php"; ?>
    <section id="newPosts">
      <h2><?php echo "「".$search_key."」の検索結果";?></h2>
      <ul>
				<?php for ($i = 0; $i < $count_new; $i++){ ?>
					<li <?php if($new_json["data"]["list"][$i]['urgent']=="2") {echo "class='emergency'";}?>>
						<a href="detail.php?id=<?php echo $new_json["data"]["list"][$i]['know_id'];?>">
						<span class="postTitle"><?php echo $new_json["data"]["list"][$i]["title"];?></span>
						<span class="date">update - <?php echo date('Y.m.d H:i',strtotime($new_json["data"]["list"][$i]['time']));?></span>
						</a>
					</li>
				<?php } ?>
      </ul>
    </section>
    <!--/#newPosts--> 
    
  </div>
  <!--/#contents-->
	<?php include "side.php"; ?>
</div>
<!--/#wrapper-->
<?php include "footer.php"; ?>
<script>
$(function() {
    $('#button').click(function(){
    $(this).next('#questionArea').slideToggle();
	$("#button").toggleClass("active");
    });
});
</script>
</body>
</html>