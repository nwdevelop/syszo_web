<?php
require 'include.php';
if(isset($_GET['action'])){
	if($_GET['action']=="login"){
		$url = API_PATH.API_LOGIN;
		$user_email=stripslashes(trim($_POST['user_email']));
		$user_pwd=stripslashes(trim($_POST['user_pwd']));
		$contact=stripslashes(trim($_POST['contact']));

		if(empty($user_email)){
			echo "メールアドレスを入力してください。";die;
		}
		if(empty($user_pwd)){
			echo "パスワードを入力してください。";die;
		}

		$post_data['user_email'] = $user_email;
		$post_data['user_pwd'] = $user_pwd;

		$o = "";
		foreach ( $post_data as $k => $v ){ $o.= "$k=" . urlencode( $v ). "&" ;}
		$post_data = substr($o,0,-1);
		$res = request_post($url, $post_data);
		$obj = json_decode($res);

		$result = $obj->{'result'};
		$msg = $obj->{'msg'};
		if($result!="0"){
			if($contact=="1"){
				$expire = time() + 30*86400;
				setcookie ("user_id",$obj->{'data'}->{'user_id'}, $expire);
				setcookie ("user_nick",$obj->{'data'}->{'user_nick'}, $expire);
			}else{
				$_SESSION['user_id']=$obj->{'data'}->{'user_id'};
				$_SESSION['user_nick']=$obj->{'data'}->{'user_nick'};
				session_regenerate_id();
			}
			echo 'success';
			die;
		}else{
			echo $msg;
			die;
		}
	}
}
?>