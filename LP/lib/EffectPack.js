// Hover ///////////////////////////////////////////////////////////////

var fdck01 = jQuery.noConflict();
fdck01(function () {
fdck01(".grpC1").hover(function(){
fdck01(".X_Phone").fadeOut(350);
fdck01(".B_Phone").fadeOut(350);
fdck01(".C_Phone").fadeOut(350);
fdck01(".D_Phone").fadeOut(350);
fdck01(".A_Phone").fadeIn(350);
}); });

var fdck02 = jQuery.noConflict();
fdck02(function () {
fdck02(".grpC2").hover(function(){
fdck02(".X_Phone").fadeOut(350);
fdck02(".A_Phone").fadeOut(350);
fdck02(".C_Phone").fadeOut(350);
fdck02(".D_Phone").fadeOut(350);
fdck02(".B_Phone").fadeIn(350);
}); });

var fdck03 = jQuery.noConflict();
fdck03(function () {
fdck03(".grpC3").hover(function(){
fdck03(".X_Phone").fadeOut(350);
fdck03(".A_Phone").fadeOut(350);
fdck03(".B_Phone").fadeOut(350);
fdck03(".D_Phone").fadeOut(350);
fdck03(".C_Phone").fadeIn(350);
}); });

var fdck04 = jQuery.noConflict();
fdck04(function () {
fdck04(".grpC4").hover(function(){
fdck04(".X_Phone").fadeOut(350);
fdck04(".A_Phone").fadeOut(350);
fdck04(".B_Phone").fadeOut(350);
fdck04(".C_Phone").fadeOut(350);
fdck04(".D_Phone").fadeIn(350);
}); });

// zoomBtn /////////////////////////////////////////////////////////////

var zoomBtn = jQuery.noConflict();

zoomBtn(document).ready(function() {

	//move the image in pixel
	var move = -33;
	
	//zoom percentage, 1.2 =120%
	var zoom = 1.2;

	//On mouse over those thumbnail
	zoomBtn('.t_TmZoomOne').hover(function() {
		
		//Set the width and height according to the zoom percentage
		width = zoomBtn('.t_TmZoomOne').width() * zoom;
		height = zoomBtn('.t_TmZoomOne').height() * zoom;
		
		//Move and zoom the image
		zoomBtn(this).find('img.Zoom').stop(false,true).animate({'width':width, 'height':height, 'top':move, 'left':move}, {duration:200});
		
		//Display the caption
		zoomBtn(this).find('div.caption').stop(false,true).fadeIn(200);
	},
	function() {
		//Reset the image
		zoomBtn(this).find('img.Effect').stop(false,true).animate({'width':zoomBtn('.t_TmZoomOne').width(), 'height':zoomBtn('.t_TmZoomOne').height(), 'top':'0', 'left':'0'}, {duration:100});	

		//Hide the caption
		zoomBtn(this).find('div.caption').stop(false,true).fadeOut(200);
	});

});

// Mouseover Fade /////////////////////////////////////////////////////////////

var movfad = jQuery.noConflict();

movfad(function(){
    movfad('#t_FtBox a').css({
        opacity: 1.0,
        filter: "alpha(opacity=100)"
        }).hover(function(){
            movfad(this).fadeTo(200,0.6);
        },function(){
            movfad(this).fadeTo(200,1.0);
    })
});

// Fixed Menu /////////////////////////////////////////////////////////////////

var fixdA = jQuery.noConflict();

	// ファーストビューの全画面＋サムネイル＋ワークスの高さを取得して、変数whFAに代入
    var whFA = fixdA('.fullSlideShow').height() + fixdA('#Stage_Thumbnail').height() + fixdA('#Stage_Works').height() + fixdA('#Stage_Web').height();

fixdA(function() {
	var topBtn = fixdA('#p_HdMenu');
	topBtn.hide();
	fixdA(window).scroll(function () {
		if (fixdA(this).scrollTop() > whFA) {
			topBtn.fadeIn();
		} else {
			topBtn.fadeOut();
		}
	});
});

// Fixed Back /////////////////////////////////////////////////////////////////

var fixdB = jQuery.noConflict();

fixdB(function() {
	var topBtn = fixdB('#p_FtBack');
	topBtn.hide();
	fixdB(window).scroll(function () {
		if (fixdB(this).scrollTop() > 300) {
			topBtn.fadeIn();
		} else {
			topBtn.fadeOut();
		}
	});
});

// Joe-Ge Button ///////////////////////////////////////////////////////

var joge = jQuery.noConflict();

joge(function () { setTimeout('rect()'); });
 
function rect() {
    joge('#rect').animate({
        marginTop: '-=10px'
    }, 800).animate({
        marginTop: '+=10px'
    }, 800);
    setTimeout('rect()', 1600);
}

// School Escaper /////////////////////////////////////////////////////////////

var sce = jQuery.noConflict();

sce(function(){
   // #で始まるアンカーをクリックした場合に処理
   sce('a[href^=#]').click(function() {
      // スクロールの速度
      var speed = 400; // ミリ秒
      // アンカーの値取得
      var href= sce(this).attr("href");
      // 移動先を取得
      var target = sce(href == "#" || href == "" ? 'html' : href);
      // 移動先を数値で取得
      var position = target.offset().top;
      // スムーススクロール
      sce('body,html').animate({scrollTop:position}, speed, 'swing');
      return false;
   });
});

// Firstview FullScreen ///////////////////////////////////////////////////////

var fss = jQuery.noConflict();

fss(document).ready(function () {
    hsize = fss(window).height();
    fss("#Firstview").css("height", hsize + "px");
});
fss(window).resize(function () {
    hsize = fss(window).height();
    fss("#Firstview").css("height", hsize + "px");
});