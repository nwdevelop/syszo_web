// mvgrpA ///////////////////////////////////////////////////////////////

var mvgrpA = jQuery.noConflict();

mvgrpA(function () {

	// 変数定義
	var grpA1 = mvgrpA('#grpASpace').find('.grpA1');
	var grpAAnime = false;

	// 画像表示
	var grpABako = function (output_obj) { if (undefined || null) { return; } console.log(output_obj); };

	// ＩＥ対応
	if (typeof window.console === "undefined") { window.console = {} }
	if (typeof window.console.log !== "function") { window.console.log = function () { } }

	// スライドイン
	mvgrpA(window).scroll(function () {
		grpABako(mvgrpA(window).scrollTop());
		if (!grpAAnime) {

			if (mvgrpA(window).scrollTop() >= 0) {

				TweenMax.to(grpA1, 3, {
					opacity: 1,
					ease : Power3.easeOut,
					top: 230,
					delay: 0,
					right: 50,
					onComplete: function () { grpAAnime = true; } });

			}

		}

	})

});

// mvgrpB ///////////////////////////////////////////////////////////////

var mvgrpB = jQuery.noConflict();

mvgrpB(function () {

	// 上の高さを取得して、変数whBに代入
    var whB = mvgrpB('#TypeGreen').height() - 300;

	// 変数定義
	var grpB1 = mvgrpB('#grpBSpace').find('.grpB1');
	var grpB2 = mvgrpB('#grpBSpace').find('.grpB2');
	var grpB3 = mvgrpB('#grpBSpace').find('.grpB3');
	var grpBAnime = false;

	// 画像表示
	var grpBBako = function (output_obj) { if (undefined || null) { return; } console.log(output_obj); };

	// ＩＥ対応
	if (typeof window.console === "undefined") { window.console = {} }
	if (typeof window.console.log !== "function") { window.console.log = function () { } }

	// スライドイン
	mvgrpB(window).scroll(function () {
		grpBBako(mvgrpB(window).scrollTop());
		if (!grpBAnime) {

			if (mvgrpB(window).scrollTop() >= whB) {

				TweenMax.to(grpB1, 1, {
					top: 40,
					left: 20,
					delay: 0.5,
					opacity: 1,
					ease      : Power1.easeOut,
					onComplete: function () { grpBAnime = true; } });

				TweenMax.to(grpB2, 1, {
					top: 190,
					left: 100,
					delay: 0.5,
					opacity: 1,
					ease : Power1.easeOut })

				TweenMax.to(grpB3, 1, {
					top: 0,
					left: 0,
					delay: 0,
					opacity: 1,
					ease : Power1.easeOut })

			}

		}

	})

});

// mvgrpC ///////////////////////////////////////////////////////////////

var mvgrpC = jQuery.noConflict();

mvgrpC(function () {

	// 上の高さを取得して、変数whCに代入
    var whC = mvgrpC('#TypeGreen').height() + mvgrpC('#TypeBlue').height() - 300;

	// 試験用・全画面の高さを取得して、#outputに表示
	// mvgrpC(document).ready(function(){
    // mvgrpC("footer").html(whC); });

	// 変数定義
	var grpC1 = mvgrpC('#grpCSpace').find('.grpC1');
	var grpC2 = mvgrpC('#grpCSpace').find('.grpC2');
	var grpC3 = mvgrpC('#grpCSpace').find('.grpC3');
	var grpC4 = mvgrpC('#grpCSpace').find('.grpC4');
	
	var grpCAnime = false;

	// 画像表示
	var grpCBako = function (output_obj) { if (undefined || null) { return; } console.log(output_obj); };

	// ＩＥ対応
	if (typeof window.console === "undefined") { window.console = {} }
	if (typeof window.console.log !== "function") { window.console.log = function () { } }

	// スライドイン
	mvgrpC(window).scroll(function () {
		grpCBako(mvgrpC(window).scrollTop());
		if (!grpCAnime) {

			if (mvgrpC(window).scrollTop() >= whC) {

				TweenMax.to(grpC1, 1, {
					top: 180,
					left: 30,
					delay: 0.5,
					opacity: 1,
					onComplete: function () { grpCAnime = true; } });

				TweenMax.to(grpC2, 1, {
					top: 360,
					left: 330,
					delay: 0.75,
					opacity: 1,
					ease : Power1.easeOut })

				TweenMax.to(grpC3, 1, {
					top: 690,
					left: 30,
					delay: 1.50,
					opacity: 1,
					ease : Power1.easeOut })

				TweenMax.to(grpC4, 1, {
					top: 890,
					left: 330,
					delay: 1.75,
					opacity: 1,
					ease : Power1.easeOut })

			}

		}

	})

});

// mvgrpD ///////////////////////////////////////////////////////////////

var mvgrpD = jQuery.noConflict();

mvgrpD(function () {

	// 上の高さを取得して、変数whDに代入
    var whD = mvgrpD('#TypeGreen').height() + mvgrpD('#TypeBlue').height() + mvgrpD('#TypeYellow').height();

	// 試験用・全画面の高さを取得して、#outputに表示
	// mvgrpD(document).ready(function(){
    // mvgrpD("footer").html(whD); });

	// 変数定義
	var grpD1 = mvgrpD('#grpDSpace').find('.grpD1');
	var grpD2 = mvgrpD('#grpDSpace').find('.grpD2');
	var grpD3 = mvgrpD('#grpDSpace').find('.grpD3');
	var grpD4 = mvgrpD('#grpDSpace').find('.grpD4');
	var grpD5 = mvgrpD('#grpDSpace').find('.grpD5');
	var grpD6 = mvgrpD('#grpDSpace').find('.grpD6');
	var grpD7 = mvgrpD('#grpDSpace').find('.grpD7');
	var grpD8 = mvgrpD('#grpDSpace').find('.grpD8');
	
	var grpDAnime = false;

	// 画像表示
	var grpDBako = function (output_obj) { if (undefined || null) { return; } console.log(output_obj); };

	// ＩＥ対応
	if (typeof window.console === "undefined") { window.console = {} }
	if (typeof window.console.log !== "function") { window.console.log = function () { } }

	// スライドイン
	mvgrpD(window).scroll(function () {
		grpDBako(mvgrpD(window).scrollTop());
		if (!grpDAnime) {

			if (mvgrpD(window).scrollTop() >= whD) {

				TweenMax.to(grpD1, 1, {
					top: -16,
					left: 460,
					delay: 0.25,
					opacity: 1,
					onComplete: function () { grpDAnime = true; } });

				TweenMax.to(grpD2, 1, {
					top: -16,
					left: 710,
					delay: 0.75,
					opacity: 1,
					ease : Power1.easeOut })

				TweenMax.to(grpD3, 1, {
					top: 184,
					left: 460,
					delay: 1,
					opacity: 1,
					ease : Power1.easeOut })

				TweenMax.to(grpD4, 1, {
					top: 184,
					left: 710,
					delay: 0.5,
					opacity: 1,
					ease : Power1.easeOut })

				TweenMax.to(grpD5, 1, {
					top: 384,
					left: 460,
					delay: 0.75,
					opacity: 1,
					ease : Power1.easeOut })

				TweenMax.to(grpD6, 1, {
					top: 384,
					left: 710,
					delay: 1.25,
					opacity: 1,
					ease : Power1.easeOut })

				TweenMax.to(grpD7, 1, {
					top: 584,
					left: 460,
					delay: 1.5,
					opacity: 1,
					ease : Power1.easeOut })

				TweenMax.to(grpD8, 1, {
					top: 584,
					left: 710,
					delay: 1,
					opacity: 1,
					ease : Power1.easeOut })

			}

		}

	})

});