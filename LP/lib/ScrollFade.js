/**
 * jquery.scrollFade.js
 * Description: スクロールに合わせて要素をフェードさせるjQueryプラグイン（要 jQuery Easing Plugin）
 * Version: 1.0.2
 * Author: Takashi Kitajima
 * Autho URI: http://2inc.org
 * created : July 30, 2013
 * modified: January 14, 2014
 * License: GPL2
 *
 * Copyright 2014 Takashi Kitajima (email : inc@2inc.org)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 2, as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

var fdd = jQuery.noConflict();

( function( fdd ) {
	fdd.fn.scrollFade = function( config ) {
		var defaults = {
			out     : 0,	// フェードアウト前の透明度
			duration: 250	// フェードにかける時間（ms）
		};
		var config = fdd.extend( defaults, config );
		var target = this;

		function fade() {
			target.each( function( i, e ) {
				var in_position = fdd( e ).offset().top + fdd( window ).height() / 5;
				var window_bottom_position = fdd( window ).scrollTop() + fdd( window ).height();
				if ( in_position < window_bottom_position ) {
					fdd( e ).animate( {
						opacity: 1
					}, {
						queue   : false,
						duration: config.duration,
						easing  : 'easeOutQuad'
					} );
				} else {
					if( fdd( e ).css( 'opacity' ) > config.out ) {
						fdd( e ).animate( {
							opacity: config.out
						}, {
							queue   : false,
							duration: config.duration,
							easing  : 'easeOutQuad'
						} );
					}
				}
			} );
		}
		fade();

		fdd( window ).scroll( function() {
			fade();
		} );
		return target;
	};
} )( jQuery );