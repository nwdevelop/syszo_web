var chng = jQuery.noConflict();

chng(document).ready(function(chng) {

	if (window.matchMedia( '(min-width: 769px)' ).matches) {
		chng.ajax({
			url: 'lib/Anime_PC.js',
			dataType: 'script',
			cache: false
	   });

	} else if (window.matchMedia( '(min-device-width:  721px) and (max-device-width: 1024px) and (orientation:portrait)' ).matches) {
	    chng.ajax({
			url: 'lib/Anime_iPad.js',
			dataType: 'script',
			cache: false
		});

	} else {
	    chng.ajax({
			url: 'lib/Anime_SP.js',
			dataType: 'script',
			cache: false
		});

	};
});
