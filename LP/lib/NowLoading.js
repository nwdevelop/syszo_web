var nlg = jQuery.noConflict();

nlg(function() {
  var h = nlg(window).height();
 
  nlg('#ld_Wrap').css('display','none');
  nlg('#ld_Screen ,#ld_Effect').height(h).css('display','block');
});
 
nlg(window).load(function () { //全ての読み込みが完了したら実行
  nlg('#ld_Screen').delay(900).fadeOut(800);
  nlg('#ld_Effect').delay(600).fadeOut(300);
  nlg('#ld_Wrap').css('display', 'block');
});
 
//10秒たったら強制的にロード画面を非表示
nlg(function(){
  setTimeout('stopload()',10000);
});
 
function stopload(){
  nlg('#ld_Wrap').css('display','block');
  nlg('#ld_Screen').delay(900).fadeOut(800);
  nlg('#ld_Effect').delay(600).fadeOut(300);
}