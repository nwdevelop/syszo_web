<?php
	require 'include.php';
	$title="SYSZO - 情シス特化型メディア";

	$login_user_id=$_SESSION['user_id'];
	if($login_user_id==""){$login_user_id=$_COOKIE['user_id'];}
	$login_user_name=$_SESSION['user_nick'];
	if($login_user_name==""){$login_user_name=$_COOKIE['user_nick'];}

	if($login_user_id==""){header("Location:https://syszo.com/login.php");}

	$view_flg=$_GET['v'];
	if($view_flg=='1'){
		$login_user_id=$_GET['u'];
	}

	$url = API_PATH.API_MYSELF_MYDATA;
	$post_data['user_id'] = $login_user_id;

	$o = "";
	foreach ( $post_data as $k => $v ){ $o.= "$k=" . urlencode( $v ). "&" ;}
	$post_data = substr($o,0,-1);
	$res = request_post($url, $post_data);
	$obj = json_decode($res);

	$result = $obj->{'result'};
	$msg = $obj->{'msg'};
	if($result!="0"){
		$user_email=$obj->{'data'}->{'user_email'};
		$user_nick=$obj->{'data'}->{'user_nick'};
		$user_sex=$obj->{'data'}->{'user_sex'};
		$address=$obj->{'data'}->{'address'};
		$industry=$obj->{'data'}->{'industry'};
		$size_company=$obj->{'data'}->{'size_company'};
		$job=$obj->{'data'}->{'job'};
		$reg_money=$obj->{'data'}->{'reg_money'};
		$calendar=$obj->{'data'}->{'calendar'};
		$software=$obj->{'data'}->{'software'};
		$arr_software=explode(",",$software);
		$hardware=$obj->{'data'}->{'hardware'};
		$arr_hardware=explode(",",$hardware);
		$qualified=$obj->{'data'}->{'qualified'};
		$arr_qualified=explode(",",$qualified);
		$allyear=$obj->{'data'}->{'allyear'};
		$number=$obj->{'data'}->{'number'};
		$price=$obj->{'data'}->{'price'};
		$time=$obj->{'data'}->{'time'};
		$introduction=$obj->{'data'}->{'introduction'};
		$scale=$obj->{'data'}->{'scale'};
		$mail_flg=$obj->{'data'}->{'mail_flg'};
		if($mail_flg=="1"){$mail_flg="全ての新着";}
		elseif($mail_flg=="2"){$mail_flg="緊急の投稿のみ";}
		elseif($mail_flg=="3"){$mail_flg="受け取らない";}
		else{$mail_flg="全ての新着";}
	}
		

?>
<?php include "head.php"; ?>
</head>
<body>
<?php include "header.php"; ?>
<div id="wrapper">
  <section id="mypage">
<?php if($view_flg=='1'){ ?>
    <h2><span class="userName"><?php echo $user_nick;?></span><span class="mini">さんのプロフィール</span></h2>
<?php }else{ ?>
    <h2><span class="userName"><?php echo $user_nick;?></span><span class="mini">さんのマイページ</span><span id="account"><a href="password_editing.php">パスワード変更</a></span><span id="editing"><a href="editing.php">編集</a></span></h2>
<?php } ?>
    <div id="yourPage">
      <div id="perfection">
        <p class="upDate">最終更新：<?php echo date('Y.m.d',strtotime($time));?></p>
        <p id="percent">プロフィール完成度<strong><?php echo $scale;?></strong><span>%</span></p>
      </div>
<?php if($view_flg=='1'){ ?>
<?php }else{ ?>
      <dl>
        <dt>メールアドレス</dt>
        <dd><?php echo $user_email;?></dd>
      </dl>
<?php } ?>
      <dl>
        <dt>ニックネーム</dt>
        <dd><?php echo $user_nick;?></dd>
      </dl>
      <dl>
        <dt>性別</dt>
        <dd><?php if($user_sex=="1"){echo "男性";}elseif($user_sex=="2"){echo "女性";}elseif($user_sex=="3"){echo "その他";}else{echo "";}?></dd>
      </dl>
      <dl>
        <dt>現在地</dt>
        <dd><?php echo $address;?></dd>
      </dl>
      <dl>
        <dt>業種</dt>
        <dd><?php echo $industry;?></dd>
      </dl>
      <dl>
        <dt>会社規模</dt>
        <dd><?php echo $size_company;?></dd>
      </dl>
      <dl>
        <dt>役職・役割</dt>
        <dd><?php echo $job;?></dd>
      </dl>
      <dl>
        <dt>年間予算</dt>
        <dd><?php echo $reg_money;?></dd>
      </dl>
      <dl>
        <dt>情シス歴</dt>
        <dd><?php echo $calendar;?></dd>
      </dl>
<!--
      <dl>
        <dt>ソフトウェア</dt>
        <dd>
					<?php echo implode("<br>",$arr_software);?><br>
				</dd>

      </dl>
      <dl>
        <dt>ハードウェア</dt>
        <dd><?php echo implode("<br>",$arr_hardware);?></dd>
      </dl>
      <dl>
        <dt>保有資格</dt>
        <dd><?php echo implode("<br>",$arr_qualified);?></dd>
      </dl>
-->
<!--
      <dl>
        <dt>年間予算</dt>
        <dd><?php echo $allyear;?></dd>
      </dl>
-->
      <dl>
        <dt>シストモ</dt>
        <dd><?php echo $number;?>人</dd>
      </dl>
      <dl>
        <dt>いいね！数</dt>
        <dd><?php echo $price;?>いいね</dd>
      </dl>
<?php if($view_flg=='1'){ ?>
<?php }else{ ?>
      <dl>
        <dt>メール通知</dt>
        <dd><?php echo $mail_flg;?></dd>
      </dl>
<?php } ?>
<!--
      <dl>
        <dt>自己紹介</dt>
        <dd><?php echo $introduction;?></dd>
      </dl>
-->
    </div>
  </section>
</div>
<!--/#wrapper-->
<?php include "footer.php"; ?>
<script>
//$(function() {
//    $('#button').click(function(){
//    $(this).next('#questionArea').slideToggle();
//	$("#button").toggleClass("active");
//    });
//});
</script>
</body>
</html>