<?php
require 'include.php';
$title="SYSZO - 情シス特化型メディア";
//
//function is_mobile(){
//	$useragents = array(
//			'iPhone',          // iPhone
//			'iPod',            // iPod touch
//			'Android',         // 1.5+ Android
//			'dream',           // Pre 1.5 Android
//			'CUPCAKE',         // 1.5+ Android
//			'blackberry9500',  // Storm
//			'blackberry9530',  // Storm
//			'blackberry9520',  // Storm v2
//			'blackberry9550',  // Storm v2
//			'blackberry9800',  // Torch
//			'webOS',           // Palm Pre Experimental
//			'incognito',       // Other iPhone browser
//			'iPad',
//			'webmate'          // Other iPhone browser
//	);
//	$pattern = '/'.implode('|', $useragents).'/i';
//	return preg_match($pattern, $_SERVER['HTTP_USER_AGENT']);
//}

$url = API_PATH.API_KNOW_INFO_LIST;

//緊急トラブル発生中！
$post_data['p_size'] = 5;//表示件数
$post_data['type'] = 4;//トラブル発生中

$o = "";
foreach ( $post_data as $k => $v ){$o.= "$k=" . urlencode( $v ). "&" ;}
$post_data = substr($o,0,-1);
$res = request_post($url, $post_data);
$urgent_json = json_decode($res,TRUE);
//echo var_dump($urgent_json);

$result = $urgent_json['result'];
$msg = $urgent_json['msg'];
if($result!="0"){
	$count_urgent = count($urgent_json["data"]);
}

//新着の投稿
$post_data_new['p_size'] = 12;//表示件数
$post_data_new['type'] = 1;//新着順

$o = "";
foreach ( $post_data_new as $k => $v ){$o.= "$k=" . urlencode( $v ). "&" ;}
$post_data_new = substr($o,0,-1);
$res = request_post($url, $post_data_new);
$new_json = json_decode($res,TRUE);


$result = $new_json['result'];
$msg = $new_json['msg'];
if($result!="0"){
	$count_new = count($new_json["data"]);
}

//最新のコメント順
$post_data_replay['p_size'] = 12;//表示件数
$post_data_replay['type'] = 2;//最新のコメント順

$o = "";
foreach ( $post_data_replay as $k => $v ){$o.= "$k=" . urlencode( $v ). "&" ;}
$post_data_replay = substr($o,0,-1);
$res = request_post($url, $post_data_replay);
$replay_json = json_decode($res,TRUE);
//echo var_dump($replay_json);

$result = $replay_json['result'];
$msg = $replay_json['msg'];
if($result!="0"){
	$count_replay = count($replay_json["data"]);
}
$action=$_GET['action'];
if($action=="reg"){

	$url_reg = API_PATH.API_LOGIN_REG;

	$user_nick=$_POST['user_nick'];
	$user_email=$_POST['user_email'];

	$post_data_reg['user_nick'] = $_POST['user_nick'];
	$post_data_reg['user_email'] = $_POST['user_email'];
	$post_data_reg['user_pwd'] = $_POST['user_pwd'];
	$post_data_reg['device'] = "0";

	$o = "";
	foreach ( $post_data_reg as $k => $v ){ $o.= "$k=" . urlencode( $v ). "&" ;}
	$post_data_reg = substr($o,0,-1);
	$res_reg = request_post($url_reg, $post_data_reg);
	$obj_reg = json_decode($res_reg);

	$result_reg = $obj_reg->{'result'};
	$msg_reg = $obj_reg->{'msg'};
	if($result_reg!="0"){

		$_SESSION['user_id']='';
		$_SESSION['user_nick']='';
		setcookie('user_id','',time()-3600);
		setcookie('user_nick','',time()-3600);
		$_SESSION['user_id']=$obj_reg->{'data'}->{'user_id'};
		$_SESSION['user_nick']=$_POST['user_nick'];
		header("Location:thanks.php");
	}

}
?>
<?php include "head.php"; ?>
<?php
if($_SESSION['user_id']==""&&$_COOKIE['user_id']==""){
	?>
	<!--151217追加-->
	<script src="js/jquery.cookie.js"></script>
	<script src="js/jquery.layerBoard.js"></script>
	<script>
		$(function(){
			$('#layer_board_area').layerBoard({alpha:0.5});
		})
	</script>
	<!--151217追加ここまで-->
	<?php
}
?>
</head>
<body>
<?php include "header.php"; ?>
<?php if(!is_mobile()){ ?>
	<?php if($_SESSION['user_id']!=""||$_COOKIE['user_id']!=""){ ?>
	<?php }else{ ?>
		<div id="main_visual">

			<div id="mv_contents">
				<h2>SYSZOは、情シスに関わるノウハウを記録・共有するサービスです。</h2>
				<div id="button_area">
					<div id="chose_area">
						<div class="button_left"><a class="tab1" onClick="signup()">新規登録</a></div>
						<div class="button_right"><a class="tab2" onClick="login()">ログイン</a></div>
					</div>

					<div id="top_signup">
						<form id="form2" action="?action=reg" method="post">
							<a id="info" style='color:red;'><?php if($result_reg=="0"){echo "$msg_reg";}?></a>
							<input type="text" class="form_first" id="user_nick" name="user_nick" maxlength="15" size="40" value="" placeholder="ニックネーム ※１５文字以内">
							<input type="email" id="user_email" name="user_email" maxlength="100" size="40" value="" placeholder = "メールアドレス">
							<input type="password" id="user_pwd" name="user_pwd" size="40"  maxlength="100" value="" placeholder = "パスワード">
							<input type="submit" class="submit is_signup" value="登録する" onclick="ga('send', 'event', 'button', 'click', '通常の新規登録');">
						</form>
					</div>

					<div id="top_login">
						<form id="form3">
							<a id="info_login"></a>
							<input type="text" class="form_first" name="user_email" id="user_email_login" maxlength="100" size="40" value="" placeholder="メールアドレス" />
							<input type="password" name="user_pwd" id="user_pwd_login" size="40" value="" placeholder="パスワード" />

							<p id="check">
								<input name="contact" type="checkbox" value="1" id="check-1" />
								<label for="check-1">次回から自動ログイン</label>
							</p>
							<div id="submit"><input class="submit is_login" type="button" onclick="validation_login();" value="ログイン" /></div>
						</form>

						<p id="forget">パスワードを忘れた方は<a href="password.php">パスワードの再設定</a>を行ってください。</p>
					</div>


				</div>

			</div>
		</div>

		<div id="native_apps">
			<div id="native_contents">
				<div id="native_contents_left">
					<p>アプリ版では更に多彩な機能をご利用いただけます。</p>
					<img src="/images/sp_img.png">
				</div>
				<div id="native_contents_right">
					<ul>
						<li class="n_c_r"><a href="https://itunes.apple.com/jp/app/syszo/id991513528?l=ja&amp;ls=1&amp;mt=8" target="_blank"><img src="images/btn_appstore.png" alt="App Store"></a></li>
						<li><a href="https://play.google.com/store/apps/details?id=com.app.ug_inc&amp;hl=ja" target="_blank"><img src="images/btn_googleplay.png" alt="Google play"></a></li>
					</ul>
				</div>
			</div>
		</div>

	<?php }?>
<?php } ?>


<div id="wrapper">
	<div id="contents">
		<?php include "nav.php"; ?>
		<?php if($count_urgent != null) {?>
			<section id="emergencyArea">
				<h2>緊急の投稿</h2>
				<p class="btnPosts"><a href="emergency.php">緊急一覧</a></p>
				<ul>
					<?php for ($i = 0; $i < $count_urgent; $i++){ ?>
						<li <?php if($urgent_json["data"][$i]['urgent']=="2") {echo "class='emergency'";}?>>
							<a href="detail.php?id=<?php echo $urgent_json["data"][$i]['know_id'];?>">
								<span class="postTitle"><em><?php echo $urgent_json["data"][$i]["title"];?></em></span>
								<span class="fbIine"><?php echo $urgent_json["data"][$i]["good_sum"];?></span>
								<span class="comeCount"><?php echo $urgent_json["data"][$i]["comment_sum"];?></span>
								<span class="date">update - <?php echo date('Y.m.d H:i',strtotime($urgent_json["data"][$i]['time']));?></span>
							</a>
						</li>
					<?php } ?>
				</ul>
			</section>
		<?php } ?>
		<!--/#emergencyArea-->
		<section id="newPosts">
			<h2>新着の投稿</h2>
			<p class="btnPosts"><a href="new.php">投稿一覧</a></p>
			<ul>
				<?php for ($i = 0; $i < $count_new; $i++){ ?>
					<li <?php if($new_json["data"][$i]['urgent']=="2") {echo "class=''";}?>>
						<a href="detail.php?id=<?php echo $new_json["data"][$i]['know_id'];?>">
							<span class="postTitle"><em><?php echo $new_json["data"][$i]["title"];?></em></span>
							<span class="fbIine"><?php echo $new_json["data"][$i]["good_sum"];?></span>
							<span class="comeCount"><?php echo $new_json["data"][$i]["comment_sum"];?></span>
							<span class="date">update - <?php echo date('Y.m.d H:i',strtotime($new_json["data"][$i]['time']));?></span>
						</a>
					</li>
				<?php } ?>
			</ul>
		</section>
		<!--/#newPosts-->
		<section id="answer">
			<h2>最新のコメント順</h2>
			<p class="btnPosts"><a href="answer.php">回答一覧</a></p>
			<ul>
				<?php for ($i = 0; $i < $count_replay; $i++){ ?>
					<li <?php if($replay_json["data"][$i]['urgent']=="2") {echo "class=''";}?>>
						<a href="detail.php?id=<?php echo $replay_json["data"][$i]['know_id'];?>">
							<span class="postTitle"><em><?php echo $replay_json["data"][$i]["title"];?></em></span>
							<span class="fbIine"><?php echo $replay_json["data"][$i]["good_sum"];?></span>
							<span class="comeCount"><?php echo $replay_json["data"][$i]["comment_sum"];?></span>
							<span class="date">update - <?php echo date('Y.m.d H:i',strtotime($replay_json["data"][$i]['time']));?></span>
						</a>
					</li>
				<?php } ?>
			</ul>
		</section>
		<!--/#answer-->

	</div>
	<!--/#contents-->
	<?php include "side.php"; ?>
</div>
<!--/#wrapper-->
<?php include "footer.php"; ?>
<script>
	$(function() {
		$('#button').click(function(){
			$(this).next('#questionArea').slideToggle();
			$("#button").toggleClass("active");
		});
	});
</script>
<script type="text/javascript">
	function validation(){
		var user_nick = document.getElementById("user_nick").value;
		var user_email = document.getElementById("user_email").value;
		var user_pwd = document.getElementById("user_pwd").value;
		var postStr = "user_nick="+user_nick+"&user_email="+user_email+"&user_pwd="+user_pwd;
		ajax("pop_reg.php?action=reg",postStr,function(result){
			if(result=="success"){
				alert('yes');
				$("#popup").css({ display: "none" });
				$("#mypage").css({ display: "block" });
				//document.getElementById("tks").innerHTML="ご登録いただきありがとうございました!<br>こちらのアカウントでアプリもご利用いただけます。<br><img src='images/appicon.png' alt='syszoアプリ'/>";
				//google
				ga('send', 'event', 'button', 'click', 'ポップアップ新規登録');
				//$("#layer_board_area").fadeOut(5000);
				//window.location.href="/";
				$('.btn_close').click(function() {
					window.location.href="/";
				});
			}else{
				alert('no');
				$("#info").css({ color: "red", display: "inline" });
				document.getElementById("info").innerHTML=result;
				$("#info").fadeOut(5000);
			}
		});
	}
	function validation_login(){
		var user_email = document.getElementById("user_email_login").value;
		var user_pwd = document.getElementById("user_pwd_login").value;
		var contact = document.getElementById("check-1").value;
		var postStr = "user_email="+user_email+"&user_pwd="+user_pwd+"&contact="+contact;
		ajax("pop_login.php?action=login",postStr,function(result){
			if(result=="success"){
				//google
				ga('send', 'event', 'button', 'click', 'ポップアップログイン');
				//$("#layer_board_area").fadeOut(5000);
				window.location.href="/";
			}else{
				$("#info_login").css({ color: "red", display: "inline" });
				document.getElementById("info_login").innerHTML=result;
				$("#info_login").fadeOut(5000);
			}
		});
	}


	function ajax(url,postStr,onsuccess){
		var xmlhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
		xmlhttp.open("POST", url, true);
		xmlhttp.onreadystatechange = function ()
		{
			if (xmlhttp.readyState == 4)
			{
				if (xmlhttp.status == 200)
				{
					onsuccess(xmlhttp.responseText);
				}
				else
				{
					alert("ERROR!");
				}
			}
		}
		xmlhttp.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
		xmlhttp.send(postStr);
	}
</script>
<script type="text/javascript">
	function signup() {
		var signup = document.getElementById("top_signup");
		var login =document.getElementById("top_login");
		signup.style.display = "inline";
		login.style.display = "none";
	}

	function login() {
		var signup = document.getElementById("top_signup");
		var login =document.getElementById("top_login");
		signup.style.display = "none";
		login.style.display = "inline";
	}

</script>

</body>
</html>